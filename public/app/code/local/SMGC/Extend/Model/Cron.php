<?php
/**
 * Cron processor class
 *
 */
class SMGC_Extend_Model_Cron
{
    public function smgc_cron()
    {
        $productcollection = Mage::getModel('catalog/product')->getCollection();
        foreach($productcollection as $prod):
        $productId=$prod->getId();
        //load the product
        $product = Mage::getModel('catalog/product')->load($productId);
        //get all images
        $mediaGallery = $product->getMediaGallery();
        //if there are images
        if (isset($mediaGallery['images'])){
            //loop through the images
            foreach ($mediaGallery['images'] as $image){
                //set the first image as the base image
                Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('thumbnail'=>$image['file']), 0);
                echo $productId.'<br>';
                //stop
                break;
            }
        }
        endforeach;
        die;
        #### Assign to Brands ####
        // set brand array: key = attribute Code and value = category ID
        $arrBrands = array(
                        79 => 76, //Foscam
                        81 => 73, //Gamut
                        306 => 74, //Green Feathers
                        84 => 72, //Hikvision
                        89 => 75 //Netgear
                    );

        // process all brands
        foreach($arrBrands as $k => $v){
            // get all products from same brand (as per $k)
            $products = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('brand', $k)->getAllIds();

            // load products from category (as per $v)
            $category = Mage::getModel('catalog/category')->load($v);
            $collection = $category->getProductCollection()->getItems();

            // empty products from category (as per $v)
            if(!empty($collection)) {
                foreach($collection as $x) {
                    $product = Mage::getModel('catalog/product')->load($x->getId());
                    $productcat = $product->getCategoryIds();
                    if(($key = array_search($category_id, $productcat)) !== false) unset($productcat[$key]);
                    $product->setCategoryIds($productcat);
                    $product->save();
                }
            }

            // load products into category (as per $v)
            foreach($products as $p) {
                $product = Mage::getModel('catalog/product')->load($p);
                $product->setCategoryIds(array_merge($product->getCategoryIds(), array($v)));
                $product->save();
            }
            sleep(15);
        }
        #### Assign to Brands ####

        echo "Brands Updated - ";

        #### Assign to New In! ####
        // category ID of New In! (ID: 33) category
        $category_id = "33";

        // get products of New In! (ID: 33) category
        $category = Mage::getModel('catalog/category')->load($category_id);
        $collection = $category->getProductCollection()->getItems();

        // empty products of New In! (ID: 33) category
        if(!empty($collection)) {
            foreach($collection as $x) {
                $product = Mage::getModel('catalog/product')->load($x->getId());
                $productcat = $product->getCategoryIds();
                if(($key = array_search($category_id, $productcat)) !== false) unset($productcat[$key]);
                $product->setCategoryIds($productcat);
                $product->save();
                // Mage::getSingleton('catalog/category_api')->removeProduct($category_id, $x->getId());
            }
        }

        // reload products in New In! (ID: 33) category
        $collection = Mage::getModel('catalog/product')->getCollection()->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.is_in_stock>0', 'left')->addAttributeToFilter('qty', array("gt" => 0))->addAttributeToFilter('status', array('in' => array(1) ))->addAttributeToFilter('visibility', array('in' => array(2,4) ))->addAttributeToSort('created_at', 'DESC')->setPageSize(10);
        if(!empty($collection)) {
            foreach($collection as $x) {
                $product = Mage::getModel('catalog/product')->load($x->getId());
                $product->setCategoryIds(array_merge($product->getCategoryIds(), array("$category_id")));
                $product->save();
                // Mage::getSingleton('catalog/category_api')->assignProduct($category->getId(),$p‌​roduct->getId());
            }
        }
        #### Assign to New In! ####
        echo "New In! Updated";
    }
}
