<?php
class SMGC_Extend_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        echo "index"; die;
    }
    
    public function loginAction()
    {
        $email=$_POST['username'];
        $password=$_POST['password'];
        $websiteId = Mage::app()->getWebsite()->getId();
        $customer = Mage::getModel('customer/customer');
        if ($websiteId) {
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($email);
        if ($customer->getId()) {
            $cust_exist=$customer->getId();
        }
         
        if($cust_exist){
            try {
                $session = Mage::getSingleton('customer/session');
                echo $session->login($email, $password);
            }catch(Exception $e){
                echo "failed";
            }
        } else {
            echo "notexist";
        }
    }
    public function get_register_user_addressAction()
    {
        $bill_address_id=$_POST['bill_address_id'];

        Mage::app()->setCurrentStore(1); //desired store id
        $address=Mage::getModel('customer/address')->load($bill_address_id);

        $address = array(
            'firstname' => $address->getFirstname(),
            'lastname' => $address->getLastname(),
            'email' => $address->getEmail(),
            'tel' => $address->getTelephone(),
            'company'=> $address->getCompany(),
            'postcode' => $address->getPostcode(),
            'city' => $address->getCity(),
            'street' => $address->getStreet(),
            'region' => $address['region'],
            'region_id' => $address->getRegionId(),
            'country' => $address->getCountry()
        );

        echo json_encode($address);
    }
    
    public function reviewsubmitAction()
    {
        $name2=$_POST['name1'];
        $title2=$_POST['title'];
        $comment2=$_POST['comment'];
        $rate2=$_POST['ratings'];
        if($rate2=="undefined")
        {
            $rate2=5;
        }
        $pro_id2=$_POST['pro_id'];
        $cust_id2=$_POST['cust_id'];

        if($cust_id2=="")
        {
            Mage::app()->setCurrentStore(1); //desired store id
            $review = Mage::getModel('review/review');
            $review->setEntityPkValue($pro_id2);//product id
            $review->setStatusId(2);
            $review->setTitle($title2);
            $review->setDetail($comment2);
            $review->setEntityId(1);                                      
            $review->setStoreId(0);      //storeview              
            $review->setStatusId(2); //approved
            $review->setNickname($name2);
            $review->setCreatedAt(date("Y/m/d"));
            $review->setReviewId($review->getId());
            $review->setStores(array(Mage::app()->getStore()->getId()));
            $review->save();
        }
        else
        {
            Mage::app()->setCurrentStore(1); //desired store id
            $review = Mage::getModel('review/review');
            $review->setEntityPkValue($pro_id2);//product id
            $review->setStatusId(2);
            $review->setTitle($title2);
            $review->setDetail($comment2);
            $review->setEntityId(1);                                      
            $review->setStoreId(0);      //storeview              
            $review->setStatusId(2); //approved
            $review->setCustomerId($cust_id2);
            $review->setNickname($name2);
            $review->setCreatedAt(date("Y/m/d"));
            $review->setReviewId($review->getId());
            $review->setStores(array(Mage::app()->getStore()->getId()));
            $review->save();
        }
        $_rating = Mage::getModel('rating/rating')
                ->setRatingId(1)
                ->setReviewId($review->getId())
                ->addOptionVote($rate2,$pro_id2);
                
        $review->aggregate();

        echo "Review submited successfully";
    }
    
    public function addaddressAction()
    {
        $customer_id=$_POST['customer_id'];
        $customer_firstname=$_POST['customer_firstname'];
        $customer_lastname=$_POST['customer_lastname'];
        $customer_tel=$_POST['customer_tel'];
        $country=$_POST['country'];
        $postcode=$_POST['postcode'];
        $county=$_POST['county'];
        $company=$_POST['company'];
        $street=$_POST['street'];
        $region_id=$_POST['region_id'];
        $region=$_POST['region'];

        Mage::app()->setCurrentStore(1); //desired store id
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $address = Mage::getModel("customer/address");

        $address->setCustomerId($customer_id)
        ->setFirstname($customer_firstname)
        ->setLastname($customer_lastname)
        ->setRegion($region)
        ->setCountryId($country)
        ->setRegionId($region_id) //state/province, only needed if the country is USA
        ->setPostcode($postcode)
        ->setCity($county)
        ->setTelephone($customer_tel)
        ->setFax('')
        ->setCompany($company)
        ->setStreet($street)
        ->setIsDefaultBilling('0')
        ->setIsDefaultShipping('0');

        try{
            $address->save();
            echo $address->getData()['entity_id'];
        }
        catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
    }
    
    public function updateaddressAction()
    {
        $data = (array) json_decode($_POST['data']);
        $order_id=$data['order_id'];
        $password=$data['password'];
        Mage::app()->setCurrentStore(1); //desired store id
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
        $billingAddress = Mage::getModel('sales/order_address')->load($order->getBillingAddress()->getId());

        $aid=$data['aid'];
        if($aid=="")
        {
            $customer_firstname=$data['customer_firstname'];
            $customer_lastname=$data['customer_lastname'];
            $customer_tel=$data['customer_tel'];
            $country=$data['country'];
            $postcode=$data['postcode'];
            $county=$data['county'];
            $company=$data['company'];
            $street=$data['street'];
            $region_id=$data['region_id'];
            $region=$data['region'];
            
            $billingAddress
            ->setFirstname($customer_firstname)
            ->setMiddlename("")
            ->setLastname($customer_lastname)
            ->setSuffix("")
            ->setCompany($company)
            ->setStreet($street)
            ->setCity($county)
            ->setCountry_id($country)
            ->setRegion($region)
            ->setRegion_id($region_id)
            ->setPostcode($postcode)
            ->setTelephone($customer_tel)
            ->setFax("");
        }
        else
        {
            $addr = Mage::getModel('customer/address')->load($aid);
            $addressdata = $addr ->getData();
            $customer_firstname=$addressdata['firstname'];
            $customer_lastname=$addressdata['lastname'];
            $customer_tel=$addressdata['telephone'];
            $country=$addressdata['country_id'];
            $postcode=$addressdata['postcode'];
            $county=$addressdata['city'];
            $company=$addressdata['company'];
            $street=$addressdata['street'];
            $region_id=$addressdata['region_id'];
            $region=$addressdata['region'];
            
            $billingAddress
            ->setCustomer_address_id($aid)
            ->setFirstname($customer_firstname)
            ->setMiddlename("")
            ->setLastname($customer_lastname)
            ->setSuffix("")
            ->setCompany($company)
            ->setStreet($street)
            ->setCity($county)
            ->setCountry_id($country)
            ->setRegion($region)
            ->setRegion_id($region_id)
            ->setPostcode($postcode)
            ->setTelephone($customer_tel)
            ->setFax("");
        }

        try{
            $billingAddress->save();
        }
        catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }


        if($aid=="")
        {
            if($password!=""){
                $customer = Mage::getModel("customer/customer");
                $websiteId = Mage::app()->getWebsite()->getId();
                if ($websiteId) {
                    $customer->setWebsiteId($websiteId);
                }
                $billingAddress = Mage::getModel('sales/order_address')->load($order->getBillingAddress()->getId());
                $shippingAddress = Mage::getModel('sales/order_address')->load($order->getShippingAddress()->getId());
                $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
                $email=$order->getCustomerEmail();
                $customer->loadByEmail($email);
                if (!$customer->getId()) {
                    $customer->website_id = $websiteId; 
                    $customer->store = 1;
                    $customer->entity_type_id = 1;
                    $customer->group_id = 1;
                    $customer->email = $email; 
                    $customer->password_hash = md5($password);
                    if($order->getCustomerId() === NULL){
                       $customer->firstname = $order->getBillingAddress()->getFirstname();
                       $customer->lastname = $order->getBillingAddress()->getLastname();
                    }
                    
                    $customer->created_in = "English Store View";
                    $customer->created_at =date('Y-m-d H:i:s', time());
                    $customer->updated_at =date('Y-m-d H:i:s', time()); 
                    $customer->save();
                    
                    $excludeKeys = array('entity_id', 'customer_address_id', 'quote_address_id', 'region_id', 'customer_id', 'address_type');
                    $oBillingAddress = $order->getBillingAddress()->getData();
                    $oShippingAddress = $order->getShippingAddress()->getData();
                    $oBillingAddressFiltered = array_diff_key($oBillingAddress, array_flip($excludeKeys));
                    $oShippingAddressFiltered = array_diff_key($oShippingAddress, array_flip($excludeKeys));

                    $addressDiff = array_diff($oBillingAddressFiltered, $oShippingAddressFiltered);

                    
                    
                    if($billingAddress){
                        $billaddress = Mage::getModel('customer/address');
                        $billstreet = $order->getBillingAddress()->getStreet();
                        $billaddress->setCustomerId($customer->getId())
                                ->setFirstname($order->getBillingAddress()->getFirstname())
                                ->setMiddleName('')
                                ->setLastname($order->getBillingAddress()->getLastname())
                                ->setCountryId($order->getBillingAddress()->getCountryId())
                                ->setRegion($order->getBillingAddress()->getRegion())
                                ->setRegionId($order->getBillingAddress()->getRegionId())
                                ->setPostcode($order->getBillingAddress()->getPostcode())
                                ->setCity($order->getBillingAddress()->getCity())
                                ->setTelephone($order->getBillingAddress()->getTelephone())
                                ->setFax('')
                                ->setCompany($order->getBillingAddress()->getCompany())
                                ->setStreet($billstreet[0])
                                ->setVatId($order->getBillingAddress()->getVatId())
                                ->setIsDefaultBilling('1')
                                ->setIsDefaultShipping('0')
                                ->setSaveInAddressBook('1');
                        try{
                            $billaddress->save();
                        }
                        catch (Exception $e) {
                            Zend_Debug::dump($e->getMessage());
                        }
                    }
                    if($shippingAddress){
                        if( $addressDiff ) {
                            $shipaddress = Mage::getModel('customer/address');
                            
                            $shipstreet = $order->getShippingAddress()->getStreet();
                            $shipaddress->setCustomerId($customer->getId())
                                    ->setFirstname($order->getShippingAddress()->getFirstname())
                                    ->setMiddleName('')
                                    ->setLastname($order->getShippingAddress()->getLastname())
                                    ->setCountryId($order->getShippingAddress()->getCountryId())
                                    ->setRegion($order->getShippingAddress()->getRegion())
                                    ->setRegionId($order->getShippingAddress()->getRegionId())
                                    ->setPostcode($order->getShippingAddress()->getPostcode())
                                    ->setCity($order->getShippingAddress()->getCity())
                                    ->setTelephone($order->getShippingAddress()->getTelephone())
                                    ->setFax('')
                                    ->setCompany($order->getShippingAddress()->getCompany())
                                    ->setStreet($shipstreet[0])
                                    ->setVatId($order->getShippingAddress()->getVatId())
                                    ->setIsDefaultBilling('0')
                                    ->setIsDefaultShipping('1')
                                    ->setSaveInAddressBook('1');
                            try{
                                $shipaddress->save();
                            }
                            catch (Exception $e) {
                                Zend_Debug::dump($e->getMessage());
                            }
                        }
                    }
                    $order->setCustomerId($customer->getId()); 
                    $order->setCustomerFirstname($customer->getFirstname()); 
                    $order->setCustomerLastname($customer->getLastname()); 
                    $order->setCustomerEmail($customer->getEmail());
                    $order->save(); 
                    if ($customer->getWebsiteId())
                    {
                        $storeId = $customer->getSendemailStoreId();
                        $customer->sendNewAccountEmail('registered', '', $storeId,$password);
                    }
                }
                else
                {
                    $order->setCustomerId($customer->getId()); 
                    $order->setCustomerFirstname($customer->getFirstname()); 
                    $order->setCustomerLastname($customer->getLastname()); 
                    $order->setCustomerEmail($customer->getEmail());
                    $order->save();
                    $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                }
            }
            else
            {
                $customer = Mage::getModel("customer/customer");
                $websiteId = Mage::app()->getWebsite()->getId();
                if ($websiteId) {
                    $customer->setWebsiteId($websiteId);
                }
                $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
                $email=$order->getCustomerEmail();
                $customer->loadByEmail($email);
                if ($customer->getId()) {
                    $order->setCustomerId($customer->getId()); 
                    $order->setCustomerFirstname($customer->getFirstname()); 
                    $order->setCustomerLastname($customer->getLastname()); 
                    $order->setCustomerEmail($customer->getEmail());
                    $order->save();
                }
            }
        }
    }
}
