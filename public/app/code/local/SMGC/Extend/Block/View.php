<?php
class SMGC_Extend_Block_View extends Mage_Core_Block_Template
{
    public function get_save_address()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if(count($customer->getAddresses()) > 0){
            $options = array();
            foreach ($customer->getAddresses() as $address) {
                $options[] = array(
                                'value' => $address->getId(),
                                'label' => $address->format('oneline'),
                                'firstname' => $address->getFirstname(),
                                'lastname' => $address->getLastname(),
                                'email' => $address->getEmail(),
                                'tel' => $address->getTelephone(),
                                'company' => $address->getCompany(),
                                'postcode' => $address->getPostcode(),
                                'city' => $address->getCity(),
                                'street' => $address->getStreet(),
                                'region' => $address['region'],
                                'country' => $address->getCountry()
                            );
            }
            return $options;
        }
    }

    public function head_meta()
    {
        $product = Mage::registry('current_product');
        $title = $product->getMetaTitle();
        $custom_title= $product['name']." ".$product['sub_name'];
        if ($title) {
            $ret_val['headBlock'] = $title;
        } else {
            $ret_val['headBlock'] = $custom_title;
        }
        $description = $product->getMetaDescription();
        $id = $product->getId();
        $mod = $id%10;
        if($mod==0 || $mod==1){
            if($mod==0){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Free Delivery with SpyCameraCCTV';
            } else if($mod==1){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Discreet packaging with SpyCameraCCTV';
            }
        } else if($mod==2 || $mod==3){
            if($mod==2){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Easy returns with SpyCameraCCTV';
            } else if($mod==3){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Free UK support with SpyCameraCCTV';
            }
        } else if($mod==4 || $mod==5){
            if($mod==4){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Fast delivery with SpyCameraCCTV';
            } else if($mod==5){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' 2 year guarantee with SpyCameraCCTV';
            }
        } else if($mod==6 || $mod==7){
            if($mod==6){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Real customer reviews with SpyCameraCCTV';
            } else if($mod==7){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Price match promise with SpyCameraCCTV';
            }
        } else if($mod==8 || $mod==9){
            if($mod==8){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Easy returns with SpyCameraCCTV';
            } else if($mod==9){
                $metaDescription = 'Find out more about '.$product['brand_value']." ".$product['name'].', '.$product['sub_name'].' Fast delivery with SpyCameraCCTV';
            }
        }
         if ($description) {
            $ret_val['desc'] = $description;
        } else {
            $ret_val['desc'] = $metaDescription;
        }
        return $ret_val;
    }

    public function sitemap_navigation(){
        $statsbs=Mage::getModel('core/variable')->loadByCode('shop-by-solution-category-order')->getName();
        $temp=explode(":",$statsbs);
        $static_cat_id_sbs=explode(",",$temp[1]);

        $statofr=Mage::getModel('core/variable')->loadByCode('our-full-range-category-order')->getName();
        $temp1=explode(":",$statofr);
        $static_cat_id_ofr=explode(",",$temp1[1]);

        $navigation = array();
        $_croot = Mage::app()->getStore()->getRootCategoryId();
        $_c = Mage::getModel('catalog/category')->load($_croot);
        $_categoriesone = $_c->getCollection()->addAttributeToFilter('is_active', 1)->addAttributeToFilter('include_in_menu', 1)->addAttributeToSelect(array('name'))->addIdFilter($_c->getChildren());
        foreach ($_categoriesone as $_cone):
            $navigation['desktop'] .= '<div class="sitemap-holder sitemap-four-col"><h2>'.$_cone->getName().'</h2><div class="four-col-site-link">';
            if($_cone->getID()==$temp[0]){
                $cat_tu=$static_cat_id_sbs;
            }
            else if($_cone->getID()==$temp1[0]){
                $cat_tu=$static_cat_id_ofr;
            }
            foreach ($cat_tu as $_ctwo):
                $_ctwo = Mage::getModel('catalog/category')->load($_ctwo);
                if(!empty($_ctwo->getImageUrl())) $navigation['desktop'] .= '<div class="site-link-holder"><img src="'.$_ctwo->getImageUrl().'" alt="'.$_ctwo->getName().'" border="0"><h3>'.$_ctwo->getName().'</h3>';
                else $navigation['desktop'] .= '<div class="site-link-holder"><h3>'.$_ctwo->getName().'</h3>';
                if(!empty($_ctwo->getChildren())) $_ctwoex = explode(",", $_ctwo->getChildren());
                else $_ctwoex = array();
                foreach ($_ctwoex as $_cthree):
                    $_cthree = Mage::getModel('catalog/category')->load($_cthree);
                    $navigation['desktop'] .= '<a href="'.$_cthree->getUrl().'">'.$_cthree->getName().'</a>';
                endforeach;
                $navigation['desktop'] .= '</div>';
            endforeach;
            $navigation['desktop'] .= '</div></div>';
        endforeach;
        $category = Mage::getModel('catalog/category')->load(78);
        if(!empty($category->getImageUrl())) $navigation['offer_desktop'].='<div class="sitemap-holder sitemap-four-col"><div class="four-col-site-link"><div class="site-link-holder"><img src="'.$category->getImageUrl().'" alt="'.$category->getName().'" border="0"><h3>'.$category->getName().'</h3>';
        else $navigation['offer_desktop'].='<div class="sitemap-holder sitemap-four-col"><div class="four-col-site-link"><div class="site-link-holder"><h3>'.$category->getName().'</h3>';
        $_categories = $category->getChildrenCategories();
        foreach ($_categories as $_cone):
            $navigation['offer_desktop'].='<a href="'.$_cone->getUrl().'">'.$_cone->getName().'</a>';
        endforeach;
        $navigation['offer_desktop'].='</div></div></div>';

        return $navigation;
    }

    public function getPopularCatgeotirs()
    {
        $data="";
        if (Mage::registry('current_category')) {
            $currentCategory = Mage::registry('current_category');
            $level = $currentCategory->getLevel();
            if($level==2 || $level==3) {
                $_c = Mage::getModel('catalog/category')->load($currentCategory->getId());
                $_categoriesone = $_c->getCollection()->addAttributeToFilter('is_active', 1)->addAttributeToFilter('include_in_menu', 1)->addAttributeToSelect(array('name'))->addIdFilter($_c->getChildren());
                $i=0;
                if(count($_categoriesone)!=0){
                    foreach ($_categoriesone as $_cone){
                        $i++;
                        if($i<10)
                        {
                            $_cone = Mage::getModel('catalog/category')->load($_cone->getId());
                            $data .= '<a href="'.$_cone->getUrl().'">'.$_cone->getName().'</a>';
                        }
                    }
                } else {
                    $parentId = $currentCategory->getParentCategory()->getId();
                    $_c = Mage::getModel('catalog/category')->load($parentId);
                    $_categoriesone = $_c->getCollection()->addAttributeToFilter('is_active', 1)->addAttributeToFilter('include_in_menu', 1)->addAttributeToSelect(array('name'))->addIdFilter($_c->getChildren());
                    $i=0;
                    foreach ($_categoriesone as $_cone){
                        if ($_cone->getId() != Mage::registry('current_category')->getId()){
                            $i++;
                            if($i<10){
                                $_cone = Mage::getModel('catalog/category')->load($_cone->getId());
                                $data .= '<a href="'.$_cone->getUrl().'">'.$_cone->getName().'</a>';
                            }
                        }
                    }
                }
            }
            if($level==4) {
                $parentId = $currentCategory->getParentCategory()->getId();
                $_c = Mage::getModel('catalog/category')->load($parentId);
                $_categoriesone = $_c->getCollection()->addAttributeToFilter('is_active', 1)->addAttributeToFilter('include_in_menu', 1)->addAttributeToSelect(array('name'))->addIdFilter($_c->getChildren());
                $i=0;
                foreach ($_categoriesone as $_cone){
                    if ($_cone->getId() != Mage::registry('current_category')->getId()){
                        $i++;
                        if($i<10){
                            $_cone = Mage::getModel('catalog/category')->load($_cone->getId());
                            $data .= '<a href="'.$_cone->getUrl().'">'.$_cone->getName().'</a>';
                        }
                    }
                }
            }
        } else {
            $_croot = Mage::app()->getStore()->getRootCategoryId();
            $_c = Mage::getModel('catalog/category')->load($_croot);
            $_categoriesone = $_c->getCollection()->addAttributeToFilter('is_active', 1)->addAttributeToFilter('include_in_menu', 1)->addAttributeToSelect(array('name'))->addIdFilter($_c->getChildren());
            $i=0;
            foreach ($_categoriesone as $_cone){
                if(!empty($_cone->getChildren())) $_coneex = explode(",", $_cone->getChildren());
                else $_ctwoex = array();
                foreach ($_coneex as $_ctwo){
                    $i++;
                    if($i<10){
                        $_ctwo = Mage::getModel('catalog/category')->load($_ctwo);
                        $data .= '<a href="'.$_ctwo->getUrl().'">'.$_ctwo->getName().'</a>';
                    }
                }
            }
        }
        return $data;
    }

    public function offer_category_navigation()
    {
        $offers=array();
        $category = Mage::getModel('catalog/category')->load(78);
        $_categories = $category->getChildrenCategories();
        foreach ($_categories as $_cone):
            $offers['desktop'].='<a href="'.$_cone->getUrl().'">'.$_cone->getName().'</a>';
            $offers['mobile'].='<a href="'.$_cone->getUrl().'" class="hide">'.$_cone->getName().'</a>';
        endforeach;
        return $offers;
    }

    public function cart_price_header()
    {
        $cartsummary = new Mage_Checkout_Block_Cart_Minicart();
        $data['cartQty'] = $cartsummary->getSummaryCount();
        if(empty($data['cartQty'])) $data['cartQty'] = 0;
        $data['subtotal'] = Mage::helper('checkout/cart')->getQuote()->getSubtotal();
        return $data;
    }

    public function google_translate_plugin()
    {
        $data['enabled'] = Mage::getStoreConfig('solwintransec/solwintransgroup/enable');
        $data['selected'] = strtolower(Mage::getStoreConfig('solwintransec/solwintransgroup/selectlanguage'));
        $layout = Mage::getStoreConfig('solwintransec/solwintransgroup/selectlayout');
        $snippetenabel = Mage::getStoreConfig('solwintransec/solwintransgroup/enable_custom');
        $snippet = Mage::getStoreConfig('solwintransec/solwintransgroup/snippet');
        $lanmodel = Mage::getModel('googlelangtranslator/system_config_source_language')->toOptionArray();
        $exselect = explode(',', $selected);
        if($layout){
            $data['layouthtml'] = 'layout: google.translate.TranslateElement.InlineLayout.'.$layout;
        }else{
            $data['layouthtml'] = 'layout: google.translate.TranslateElement.InlineLayout.SIMPLE';
        }
        return $data;
    }

    public function getReviewRatings($productId)
    {
        $data['reviews'] = Mage::getModel('review/review')
                    ->getResourceCollection()
                    ->addStoreFilter(Mage::app()->getStore()->getId())
                    ->addEntityFilter('product', $productId)
                    ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                    ->setDateOrder()
                    ->addRateVotes();
        if (count($data['reviews']) > 0) {
            $data['votesCollection'] = Mage::getModel('rating/rating_option_vote')
                ->getResourceCollection()
                ->setEntityPkFilter($productId)
                ->setStoreFilter(Mage::app()->getStore()->getId())
                ->load();
            $rate=array();
            foreach($data['votesCollection'] as $vo){
                foreach ($data['reviews']->getItems() as $review) {
                    if($review['review_id']==$vo['review_id']){
                        $rate[]=$vo['value'];
                    }
                }
            }
            $data['rate'] = $rate;
            $avgf = array_sum($rate)/count($rate);
            $data['avg'] = floor($avgf);
            $fraction = $avgf - $data['avg'];
            $data['fraction'] = round($fraction);
            $remaining_star = 5-$avgf;
            $data['remaining_star'] = round($remaining_star, 0, PHP_ROUND_HALF_DOWN);
            $perc = ($avgf/5)*100;
            $round_perc = floor($perc);
            $round_perc = $round_perc*0.05;
            $data['round_perc'] = number_format($round_perc,1);
        }
        $data['ratingCollection'] = Mage::getModel('rating/rating')
                ->getResourceCollection()
                ->addEntityFilter('product')
                ->setPositionOrder()
                ->addRatingPerStoreName(Mage::app()->getStore()->getId())
                ->setStoreFilter(Mage::app()->getStore()->getId())
                ->load()
                ->addOptionToItems();
        return $data;
    }

    public function getIndividualVotes($reviewId)
    {
        $votesByIndividual = Mage::getModel('rating/rating_option_vote')
                ->getResourceCollection()
                ->setReviewFilter($reviewId)
                ->setStoreFilter(Mage::app()->getStore()->getId())
                ->load();
        foreach($votesByIndividual as $vo){
            $ratebyindiviual=$vo['value'];
        }
        return $ratebyindiviual;
    }

    public function getBreadcrumbs($crumbs)
    {
        $data="";
        if(array_key_exists("product", $crumbs)){
            unset($crumbs['product']);
            unset($crumbs['category3']);
            unset($crumbs['category4']);
            $count = 0;
            foreach($crumbs as $_crumbName=>$_crumbInfo){
                $data .= '<a href="'.$_crumbInfo['link'].'">'.$this->escapeHtml($_crumbInfo['label']).'</a>';
                if($count != sizeof($crumbs)-1) $data .= " > ";
                $count++;
            }
        } else {
            unset($crumbs['category3']);
            unset($crumbs['category4']);
            $count = 0;
            foreach($crumbs as $_crumbName=>$_crumbInfo){
                if($count != sizeof($crumbs)-1) {
                    $data .= '<a href="'.$_crumbInfo['link'].'">'.$this->escapeHtml($_crumbInfo['label']).'</a>';
                    $data .= " > ";
                } else {
                    $data .= $this->escapeHtml($_crumbInfo['label']);
                }
                $count++;
            }
        }
        return $data;
    }

    public function getLinkBlock_html($idPath)
    {
        $_product = Mage::getModel('catalog/product')->load($idPath[1]);
        $productId=$idPath[1];
        $reviews = Mage::getModel('review/review')
                        ->getResourceCollection()
                        ->addStoreFilter(Mage::app()->getStore()->getId())
                        ->addEntityFilter('product', $productId)
                        ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                        ->setDateOrder()
                        ->addRateVotes();
        if (count($reviews) > 0) {
            $votesCollection = Mage::getModel('rating/rating_option_vote')
                ->getResourceCollection()
                ->setEntityPkFilter($productId)
                ->setStoreFilter(Mage::app()->getStore()->getId())
                ->load();
            $rate=array();
            foreach($votesCollection as $vo){
                foreach ($reviews->getItems() as $review) {
                    if($review['review_id']==$vo['review_id']){
                        $rate[]=$vo['value'];
                    }
                }
            }
            $avg = array_sum($rate)/count($rate);
            $avgf = array_sum($rate)/count($rate);
            $avg = floor($avgf);
            $fraction = $avgf - $avg;
            $remaining_star=5-$avgf;
        }
        $data .= '<li><div class="col-table full-width"><ul>';
        foreach($_product as $pro) {
            $data .= '<li><a '.$this->getLinkAttributes().'><h3>'.$_product->getName().'<span>'.$pro['sub_name'].'</span></h3></a></li><li>';
            $i=1;
            for($i=1;$i<=$avg;$i++) {
                $data .= '<img src="'.$this->getSkinUrl('images/black-star.png').'" alt="" border="0">';
            }
            for($j=$i;$j<=5;$j++) {
                $data .= '<img src="'.$this->getSkinUrl('images/grey-star.png').'" alt="" border="0">';
            }
            $data .= '</li>';
            if($_product->getTypeId()=="simple"){
                $sim_price = $_product->getPrice();
                $baseCurrencyCode = Mage::app()->getBaseCurrencyCode();
                $allowedCurr = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
                $currencyRa = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurr));
                foreach($currencyRa as $me => $rates){
                    if(Mage::app()->getStore()->getCurrentCurrencyCode()==$me){
                        $priceWitCurrency= number_format((float)$sim_price*$rates, 2, '.', '');
                    } else {
                        $priceWitCurrency=number_format((float)$sim_price, 2, '.', '');
                    }
                }
                $data .= '<li><span class="trust-prod-price"><span class="price" data-currency="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'" data-incprice="'.number_format((float)($priceWitCurrency)+($priceWitCurrency*0.2), 2, '.', '').'" data-exprice="'.number_format((float)$priceWitCurrency, 2, '.', '').'"></span></span></li>';
            }
            if($_product->getTypeId()=="bundle"){
                $optionCol= $_product->getTypeInstance(true)
                                    ->getOptionsCollection($_product);
                $selectionCol= $_product->getTypeInstance(true)
                                       ->getSelectionsCollection(
                $_product->getTypeInstance(true)->getOptionsIds($_product),$_product
                );
                $optionCol->appendSelections($selectionCol);
                $price = $_product->getPrice();

                foreach ($optionCol as $option) {
                    if($option->required) {
                        $selections = $option->getSelections();
                        $minPrice = min(array_map(function ($s) {
                                        return $s->price;
                                    }, $selections));
                        if($_product->getSpecialPrice() > 0) {
                            $minPrice *= $_product->getSpecialPrice()/100;
                        }
                        $price += round($minPrice,2);
                    }
                }
                $baseCurrencyCode = Mage::app()->getBaseCurrencyCode();
                $allowedCurr = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
                $currencyRa = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurr));
                foreach($currencyRa as $me => $rates){
                    if(Mage::app()->getStore()->getCurrentCurrencyCode()==$me){
                        $price= number_format((float)$price*$rates, 2, '.', '');
                    } else {
                        $price=number_format((float)$price, 2, '.', '');
                    }
                }
                $data .= '<li><span class="trust-prod-price"><span class="trust-from">From</span><span class="price" data-currency="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'" data-incprice="'.number_format((float)($price)+($price*0.2), 2, '.', '').'" data-exprice="'.number_format((float)$price, 2, '.', '').'"></span></span></li>';
            }
            if($_product->getTypeId()=='configurable'){
                $minn = array();
                $confi_product = Mage::getModel('catalog/product')->load($_product->getId());
                $block = Mage::app()->getLayout()->createBlock('catalog/product_view_type_configurable');
                $block->setProduct($confi_product);
                $config = json_decode($block->getJsonConfig(), true);
                foreach ($config['childProducts'] as $aId=>$aValues){
                    $minn[]=$aValues['price'];
                }
                $min=min($minn);
                $data .= '<li><span class="trust-prod-price"><span class="trust-from">From</span><span class="price" data-currency="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'" data-incprice="'.number_format((float)($min)+($min*0.2), 2, '.', '').'" data-exprice="'.number_format((float)$min, 2, '.', '').'"></span></span></li>';
            }
            break;
            $data .= '</ul></div></li>';
        }
    }

    public function bundleSelect_html($_selection)
    {
        $selecty = new Mage_Bundle_Block_Catalog_Product_View_Type_Bundle_Option_Select();
        $priceTitle = $this->escapeHtml($_selection->getName());
        $price = $selecty->getProduct()->getPriceModel()->getSelectionPreFinalPrice($selecty->getProduct(), $_selection, 1);
        $price = $price*$_selection->getSelectionQty();
        $inc_price = $price+($price*0.2);
        $data['inc_finalTilte'] = $priceTitle.':'.' ' . $selecty->formatPriceString($inc_price, $includeContainer);
        $data['finalTilte'] = $priceTitle.':'.' ' . $selecty->formatPriceString($price, $includeContainer);
        return $data;
    }

    public function getBestSellingProduct_html()
    {
        $storeId    = Mage::app()->getStore()->getId();
        $today = time();$data="";
        $last = $today - (60*60*24*30);
        $from = date("Y-m-d H:i:s", $last);
        $to = date("Y-m-d H:i:s", $today);

        $products = Mage::getResourceModel('reports/product_collection')
        ->addAttributeToSelect('*')
        ->setStoreId($storeId)
        ->addStoreFilter($storeId)
        ->addOrderedQty($from, $to)
        ->setOrder('ordered_qty', 'desc');

        Mage::getSingleton('catalog/product_status')
                ->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')
                ->addVisibleInCatalogFilterToCollection($products);
        $temp=$products->getData();

        $product=array();
        foreach($temp as $val){
            $product[] = Mage::getModel('catalog/product')->loadByAttribute('sku',$val['sku']);
        }
        $i=0;
        $data .= '<section class="section top-btm-gap related-products">';
        if(count($product)>0){
            $data .= '<h2>Our Best-Selling Products</h2><hr><div class="col-table full-width wrapper"><ul>';
            foreach($product as $prodi){
                if($prodi['is_salable']==1){
                    $i++;
                    if($i<5){
                        $formattedPrice = Mage::helper('core')->currency($prodi->getPrice(), true, false);
                        $gallery_images = Mage::getModel('catalog/product')->load($prodi->getId())->getMediaGalleryImages();
                        $la = array();
                        foreach($gallery_images as $g_image) {
                            $la[] = $g_image['label'];
                        }
                    $data .= '<li><a href="'.Mage::getBaseUrl().$prodi['url_key'].'"><img src="'.$this->helper('catalog/image')->init($prodi, 'thumbnail')->resize(100, 100).'" width="100" height="100" border="0" alt="'.$la[0].'"/><h3>'.$this->escapeHtml($prodi->getName()).'<span>'.$prodi['sub_name'].'</span></h3><span class="fill-btn black-btn">View Product</span></a></li>';
                    }
                }
            }
            $data .= '</ul></div>';
        }
        $data .= '</section>';
        return $data;
    }

    public function getCrossellProduct_html($_product)
    {
        $upsell_product = $_product->getCrossSellProducts();
        $object = Mage::getModel('catalog/product');
        $count = count($upsell_product);
        $data = '<section class="section top-btm-gap related-products">';
        if($count){
            $data .= '<h2>Other Customers Bought</h2><hr><div class="col-table full-width wrapper"><ul>';
            foreach($upsell_product as $_upsell){
                $upsp = $object->load($_upsell->getId());
                $gallery_images = Mage::getModel('catalog/product')->load($_upsell->getId())->getMediaGalleryImages();
                $la = array();
                foreach($gallery_images as $g_image) {
                    $la[] = $g_image['label'];
                }
                if($upsp['is_salable']==1){
                    $data .= '<li><a href="'.Mage::getBaseUrl().$upsp['url_key'].'"><img src="'.$this->helper('catalog/image')->init($upsp, 'thumbnail')->resize(100, 100).'" width="100" height="100" border="0" alt="'.$la[0].'"/><h3>'.$this->escapeHtml($upsp->getName()).'<span>'.$upsp['sub_name'].'</span></h3><span class="fill-btn black-btn">View Product</span></a></li>';
                }
            }
            $data .= '</ul></div>';
        }
        $data .= '<section>';
        return $data;
    }

    public function getCrossellProductForPopup_html($_product)
    {
        $upsell_product = $_product->getCrossSellProducts();
        $object = Mage::getModel('catalog/product');
        $count = count($upsell_product);
        if($count){
            $data = '<hr><h2>'.$this->__('RELATED PRODUCTS').'</h2><div class="col-table full-width popup-crosssell"><hr><ul>';
            foreach($upsell_product as $_upsell){
                $upsp = $object->load($_upsell->getId());
                $formattedPrice = Mage::helper('core')->currency($upsp->getPrice(), true, false);
                $gallery_images = Mage::getModel('catalog/product')->load($_upsell->getId())->getMediaGalleryImages();
                $la = array();
                foreach($gallery_images as $g_image) {
                    $la[] = $g_image['label'];
                }
                if($upsp['is_salable']==1){
                    $data .= '<li><a href="'.Mage::getBaseUrl().$upsp['url_key'].'"><img src="'.$this->helper('catalog/image')->init($upsp, 'thumbnail')->resize(160, 160).'" width="160" height="160" border="0" alt="'.$la[0].'"/><h3>'.$this->escapeHtml($upsp->getName()).'<span>'.$upsp['sub_name'].'</span></h3></a></li>';
                }
            }
            $data .= '</ul></div>';
        }
        return $data;
    }

    public function checkBundleOutOfStock($product){
        $flag=0;
        $optionCol= $product->getTypeInstance(true)
                            ->getOptionsCollection($product);
        $selectionCol= $product->getTypeInstance(true)
                               ->getSelectionsCollection(
        $product->getTypeInstance(true)->getOptionsIds($product),$product);
        $optionCol->appendSelections($selectionCol);
        $price = $product->getPrice();
        foreach ($optionCol as $key => $option) {
             $default_title = $option->getData('default_title');
             if($default_title=="hidethis") {
                foreach ($option['selections'] as $selKey => $selection) {
                    if($option->required) {
                        $selections = $option->getSelections();
                        $minPrice = min(array_map(function ($s) {
                                        return $s->price;
                                    }, $selections));
                        if($product->getSpecialPrice() > 0) {
                            $minPrice *= $product->getSpecialPrice()/100;
                        }
                        $minPrice *= $selection['selection_qty'];
                        $price += round($minPrice,2);
                    }
                    foreach ($selection['stock_item'] as $sKey => $select) {
                        if($select['qty']<$selection['selection_qty']){
                            $flag=1;
                        }
                        break;
                    }
                }
            }
        }
        $data['price'] = $price;
        $data['flag'] = $flag;
        return $data;
    }

    public function getRelatedUpsell_html($_product)
    {
        $_helper = $this->helper('catalog/output');
        $prosku=$_helper->productAttribute($_product, $_product->getSku(), 'sku');
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$prosku);
        $data = '<div class="select-config">';
        $prodrelated=explode("|",$product['related_upsell']);
        foreach($prodrelated as $related){
            $rp=explode(":",$related);
            $relatedproduct = Mage::getModel('catalog/product')->loadByAttribute('sku',$rp[0]);
            if($relatedproduct){
                if($rp[0]==$prosku){
                $data .= '<a class="selected-config">'.$rp[1].'</a>';
                } else {
                $data .= '<a href="'.$relatedproduct->getProductUrl().'">'.$rp[1].'</a>';
                }
            }
        }
        $data .= '</div>';
        return $data;
    }

    public function getFeaturedAttribute_html($_product)
    {
        $count=0;$data="";
        $prodfeat=explode("|",$_product->getData('featured_attributes'));
        foreach($_product->_data as $key => $val){
            if (in_array($key, $prodfeat)) {
                if(!empty($val)){
                    $count++;
                    $attribute_value = $_product->getResource()->getAttribute($key)->getFrontend()->getValue($_product);
                    $label = $_product->getResource()->getAttribute($key)->getFrontend()->getLabel($_product);
                    if($count==6){
                        break;
                    } else {
                        $data .= '<li><div class="feature"><img src="'.$this->getSkinUrl('images/'.$key.'.jpg').'" alt="'.$label;
                        if($attribute_value!='YES') {
                            $data .= ': '.$attribute_value.' border="0">';
                        }
                        if($attribute_value!='YES'){
                            $data .= '<div>'.$attribute_value.'</div>';
                        }
                        $data .= '<span>'.$label.'</span></div></li>';
                    }
                }
            }
        }
        return $data;
    }

    public function state_view()
    {
        $load_state = new Mage_Catalog_Block_Layer_State();
        $_filters = $load_state->getActiveFilters();
        $cur=Mage::registry('current_category');
        if($cur){
            $finminPrice=Mage::getSingleton('catalog/layer')
            ->setCurrentCategory(Mage::registry('current_category'))
            ->getProductCollection()
            ->getMinPrice();

            $minPrice=round($finminPrice);
            $mod=$minPrice%5;
            if($mod!=0){
                $minPrice=$minPrice-$mod;
            }

            $inc_minprice=round($finminPrice+($finminPrice*0.2));
            $mod1=$inc_minprice%5;
            if($mod1!=0){
                $inc_minprice=$inc_minprice-$mod1;
            }

            $finmaxPrice=Mage::getSingleton('catalog/layer')
            ->setCurrentCategory(Mage::registry('current_category'))
            ->getProductCollection()
            ->getMaxPrice();

            $maxPrice=round($finmaxPrice);
            $mod=$maxPrice%5;
            if($mod!=0){
                $temp=5-$mod;
                $maxPrice=$maxPrice+$temp;
            }

            $inc_maxPrice=round($finmaxPrice+($finmaxPrice*0.2));
            $mod1=$inc_maxPrice%5;
            if($mod1!=0){
                $temp1=5-$mod1;
                $inc_maxPrice=$inc_maxPrice+$temp1;
            }

            $_productCollection= clone Mage::getSingleton('catalog/layer')
            ->setCurrentCategory(Mage::registry('current_category'))
            ->getProductCollection();
            $nvr_value=array();
            $nvr_label=array();
            $fov_value=array();
            $fov_label=array();
            $temp=array();
            $temp1=array();
            foreach($_productCollection as $_product) {
               if($_product['night_vision_range_value']!=""){
                  $temp[]=$_product['night_vision_range_value'];
                  $nvr_value[]=$_product['night_vision_range']."_".$_product['night_vision_range_value'];
               }
               if($_product['field_of_view']!=""){
                  $temp1[]=$_product['field_of_view_value'];
                  $fov_value[]=$_product['field_of_view']."_".$_product['field_of_view_value'];
               }
            }
            $nvr_value=array_unique($nvr_value);
            $fov_value=array_unique($fov_value);
        } else {
            $category_id = '';
            $nvr_value=0;
            $fov_value=0;
            $minPrice=0;
            $maxPrice=0;
            $temp=0;
            $temp1=0;
        }
        foreach ($_filters as $_filter){
            if(($_filter->getName()=='Price')){
                $fil2=$_filter->getRemoveUrl();
                $ft2=explode("?",$fil2);
                $findme2   = '&';
                $pos2 = strpos($ft2[1], $findme2);
                if ($pos2 === false){
                    $fin_url2=$ft2[0];
                } else {
                    $gp2=explode("&",$ft2[1]);
                    $fing2="";
                    foreach($gp2 as $g2){
                        $findme2   = 'price';
                        $pos2 = strpos($g2, $findme2);
                        if ($pos2 === false){
                            $fing2=$g2."&".$fing2;
                        }
                    }
                    $fin_url2=$ft2[0]."?".$fing2;
                    $fin_url2=rtrim($fin_url2, "&");
                }
                $data['price'] = '<a id="state-filter" href="'.$fin_url2.'">'.$this->__($_filter->getName()).': <span class="state-filter" data-exprice="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().number_format((float)$minPrice, 2, '.', '')."-".Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().number_format((float)$maxPrice, 2, '.', '').'" data-incprice="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().number_format($inc_minprice, 2, '.', '')."-".Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().number_format($inc_maxPrice, 2, '.', '').'"></span></a>';
                break;
            }
        }
        foreach ($_filters as $_filter){
            if(($_filter->getName()=='Night Vision Range')){
                $fil=$_filter->getRemoveUrl();
                $ft=explode("?",$fil);
                $findme   = '&';
                $pos = strpos($ft[1], $findme);
                if ($pos === false){
                    $fin_url=$ft[0];
                } else {
                    $gp=explode("&",$ft[1]);
                    $fing="";
                    foreach($gp as $g){
                        $findme   = 'night_vision_range';
                        $pos = strpos($g, $findme);
                        if ($pos === false){
                            $fing=$g."&".$fing;
                        }
                    }
                    $fin_url=$ft[0]."?".$fing;
                    $fin_url=rtrim($fin_url, "&");
                }
                $data['ngvira'] = '<a href="'.$fin_url.'">'.$this->__($_filter->getName()).': <span>'.min($temp)."-".max($temp).'</span></a>';
                break;
            }
        }
        foreach ($_filters as $_filter){
            if(($_filter->getName()=='Field of View')){
                $fil1=$_filter->getRemoveUrl();
                $ft1=explode("?",$fil1);
                $findme1   = '&';
                $pos1 = strpos($ft1[1], $findme1);
                if ($pos1 === false){
                    $fin_url1=$ft1[0];
                } else {
                    $gp1=explode("&",$ft1[1]);
                    $fing1="";
                    foreach($gp1 as $g1){
                        $findme1   = 'field_of_view';
                        $pos1 = strpos($g1, $findme1);
                        if ($pos1 === false){
                            $fing1=$g1."&".$fing1;
                        }
                    }
                    $fin_url1=$ft1[0]."?".$fing1;
                    $fin_url1=rtrim($fin_url1, "&");
                }
                $data['fiofv'] = '<a href="'.$fin_url1.'">'.$this->__($_filter->getName()).': <span>'.min($temp1)."-".max($temp1).'</span></a>';
                break;
            }
        }
        return $data;
    }

    public function filter_view()
    {
        $cur=Mage::registry('current_category');
        if($cur){
            $category_id = Mage::registry('current_category')->getId();
            $finminPrice=Mage::getSingleton('catalog/layer')
            ->setCurrentCategory(Mage::registry('current_category'))
            ->getProductCollection()
            ->getMinPrice();

            $minPrice=round($finminPrice);
            $mod=$minPrice%5;
            if($mod!=0){
                $minPrice=$minPrice-$mod;
            }

            $inc_minprice=round($finminPrice+($finminPrice*0.2));
            $mod1=$inc_minprice%5;
            if($mod1!=0){
                $inc_minprice=$inc_minprice-$mod1;
            }

            $finmaxPrice=Mage::getSingleton('catalog/layer')
            ->setCurrentCategory(Mage::registry('current_category'))
            ->getProductCollection()
            ->getMaxPrice();

            $maxPrice=round($finmaxPrice);
            $mod=$maxPrice%5;
            if($mod!=0){
                $temp=5-$mod;
                $maxPrice=$maxPrice+$temp;
            }

            $inc_maxPrice=round($finmaxPrice+($finmaxPrice*0.2));
            $mod1=$inc_maxPrice%5;
            if($mod1!=0){
                $temp1=5-$mod1;
                $inc_maxPrice=$inc_maxPrice+$temp1;
            }

            $_productCollection= clone Mage::getSingleton('catalog/layer')
            ->setCurrentCategory(Mage::registry('current_category'))
            ->getProductCollection();

            $nvr_value=array();
            $nvr_label=array();
            $fov_value=array();
            $fov_label=array();
            $temp=array();
            $temp1=array();
            foreach($_productCollection as $_product) {
               if($_product['night_vision_range_value']!=""){
                  $temp[]=$_product['night_vision_range_value'];
                  $nvr_value[]=$_product['night_vision_range']."_".$_product['night_vision_range_value'];
               }
               if($_product['field_of_view']!=""){
                  $temp1[]=$_product['field_of_view_value'];
                  $fov_value[]=$_product['field_of_view']."_".$_product['field_of_view_value'];
               }
                if($_product->getTypeId()=="bundle"){
                   $optionCol= $_product->getTypeInstance(true)
                                        ->getOptionsCollection($_product);
                    $selectionCol= $_product->getTypeInstance(true)
                                           ->getSelectionsCollection(
                    $_product->getTypeInstance(true)->getOptionsIds($_product),$_product
                    );
                    $optionCol->appendSelections($selectionCol);
                    $price = $_product->getPrice();

                    foreach ($optionCol as $option) {
                        if($option->required) {
                            $selections = $option->getSelections();
                            $minPricey = min(array_map(function ($s) {
                                            return $s->price;
                                        }, $selections));
                            if($_product->getSpecialPrice() > 0) {
                                $minPricey *= $_product->getSpecialPrice()/100;
                            }
                            $price += round($minPricey,2);
                        }
                    }
                }
                if($_product->getTypeId()=="simple" || $_product->getTypeId()=="configurable"){
                    $price = $_product->getPrice();
                }
                $pricet[] = number_format((float)$price, 2, '.', '');

            }
            $nvr_value=array_unique($nvr_value);
            $fov_value=array_unique($fov_value);
            $products_count = Mage::getModel('catalog/category')->load($category_id)->getProductCount();
        } else {
            $category_id = '';
            $nvr_value=0;
            $fov_value=0;
            $minPrice=0;
            $maxPrice=0;
            $temp=0;
            $temp1=0;
            $products_count=0;
        }

        $data['input'] = '<input type="hidden" id="price-range-baseline" data-value="'.implode(",",$pricet).'" data-rangemin="'.$minPrice.'" data-rangemax="'.$maxPrice.'" data-rangemin-inc="'.$inc_minprice.'" data-rangemax-inc="'.$inc_maxPrice.'"/><input type="hidden" id="night-vision-range-baseline" data-value="'.implode(",",$nvr_value).'" data-rangemin="'.min($temp).'" data-rangemax="'.max($temp).'"/><input type="hidden" id="field-of-view-range-baseline" data-value="'.implode(",",$fov_value).'" data-rangemin="'.min($temp1).'" data-rangemax="'.max($temp1).'"/>';

        $data['price'] = '<div ';
        if($minPrice==$maxPrice || $inc_minprice==$inc_maxPrice || count($_productCollection)==1){
            $data['price'] .= 'style="display:none"';
        }
        $data['price'] .= '><div class="round-checkbox">Price</div><input type="text" id="price-range" data-value="'.implode(",",$pricet).'" name="priceSlider" data-rangemin="'.$minPrice.'" data-rangemax="'.$maxPrice.'" data-rangemin-inc="'.$inc_minprice.'" data-rangemax-inc="'.$inc_maxPrice.'" symbol="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'"/></div>';

        $data['ngvira'] = '<div ';
        if(min($temp)==max($temp)){
            $data['ngvira'] .= 'style="display:none"';
        }
        $data['ngvira'] .= '><div class="round-checkbox">Night Vision Range</div><input type="text" id="night-vision-range" name="nightvisionSlider" data-rangemin="'.min($temp).'" data-rangemax="'.max($temp).'"/></div>';

        $data['fiofv'] = '<div ';
        if(min($temp1)==max($temp1)){
            $data['fiofv'] .= 'style="display:none"';
        }
        $data['fiofv'] .= '><div class="round-checkbox">Field of View</div><input type="text" id="field-of-view-range" name="fieldofviewSlider" data-rangemin="'.min($temp1).'" data-rangemax="'.max($temp1).'"/></div>';

        $data['product_count'] = $products_count;

        return $data;
    }

    public function cart_header_items($item)
    {
        if($item->getQty()>0){
            $data = "";
            $type=$item->getProduct()->getTypeId();
            $productName = $item->getName();
            $productPrice = $item->getPrice();

            $baseCurrencyCode = Mage::app()->getBaseCurrencyCode();
            $allowedCurr = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
            $currencyRa = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurr));

            foreach($currencyRa as $me => $rates){
                if(Mage::app()->getStore()->getCurrentCurrencyCode()==$me){
                    $pric= number_format((float)$productPrice*$rates, 2, '.', '');
                }
            }
            $gallery_images = Mage::getModel('catalog/product')->load($item->getProduct()->getId())->getMediaGalleryImages();
            $labe = array();
            foreach($gallery_images as $g_image) {
                $labe[] = $g_image['label'];
            }
            if($type!='configurable' && $type!='bundle'){
                if(!$item->getParentItemId()){
                    $data .= '<a href="'.$item->getProduct()->getProductUrl().'" class="basket-item"><div class="item-img"><img src="'.$this->helper('catalog/image')->init($item->getProduct(), 'thumbnail')->resize(70, 70).'" width="70" height="70" border="0" alt="'.$labe[0].'"/></div><div class="item-detail"><div class="item-name">'.$item->getQty()."x ".$productName.'</div><div class="item-price"><span class="price" data-currency="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'" data-incprice="'.number_format((float)($pric)+($pric*0.2), 2, '.', '').'" data-exprice="'.number_format((float)$pric, 2, '.', '').'"></span></div></div></a>';
                }
            }
            if($type==='bundle'){
                $options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
                $data .= '<a href="'.$item->getProduct()->getProductUrl().'" class="basket-item"><div class="item-img"><img src="'.$this->helper('catalog/image')->init($item->getProduct(), 'thumbnail')->resize(70, 70).'" width="70" height="70" border="0" alt="'.$labe[0].'"/></div><div class="item-detail"><div class="item-name">'.$item->getQty()."x ".$productName.'</div><div class="item-price"><span class="price" data-currency="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'" data-incprice="'.number_format((float)($pric)+($pric*0.2), 2, '.', '').'" data-exprice="'.number_format((float)$pric, 2, '.', '').'"></span></div></div><div class="item-options">';
                foreach ($options['bundle_options'] as $option){
                    if($option['label']!="hidethis"){
                        foreach ($option['value'] as $sub){
                            $data .= '<div>'.$sub['title'].'</div>';
                        }
                    }
                }
                $data .= '</div></a>';
            }
            if($type==='configurable'){
                $options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
                $data .= '<a href="'.$item->getProduct()->getProductUrl().'" class="basket-item"><div class="item-img"><img src="'.$this->helper('catalog/image')->init($item->getProduct(), 'thumbnail')->resize(70, 70).'" width="70" height="70" border="0" alt="'.$labe[0].'"/></div><div class="item-detail"><div class="item-name">'.$item->getQty()."x ".$options['simple_name'].'</div><div class="item-price"><span class="price" data-currency="'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'" data-incprice="'.number_format((float)($productPrice)+($productPrice*0.2), 2, '.', '').'" data-exprice="'.number_format((float)$productPrice, 2, '.', '').'"></span></div></div><div class="item-options"></div></a>';
            }
            return $data;
        }
    }

    public function category_navigation()
    {
        $statsbs=Mage::getModel('core/variable')->loadByCode('shop-by-solution-category-order')->getName();
        $temp=explode(":",$statsbs);
        $static_cat_id_sbs=explode(",",$temp[1]);

        $statofr=Mage::getModel('core/variable')->loadByCode('our-full-range-category-order')->getName();
        $temp1=explode(":",$statofr);
        $static_cat_id_ofr=explode(",",$temp1[1]);

        $navigation = array();
        $_croot = Mage::app()->getStore()->getRootCategoryId();
        $_c = Mage::getModel('catalog/category')->load($_croot);
        $_categoriesone = $_c->getCollection()->addAttributeToFilter('is_active', 1)->addAttributeToFilter('include_in_menu', 1)->addAttributeToSelect(array('name'))->addIdFilter($_c->getChildren());
        foreach ($_categoriesone as $_cone):
            $navigation['mobile'] .= '<li class="burger-accordian"><div class="burger-sub-menu use toggle-main-menu">'.$_cone->getName().'</div>';
            $navigation['desktop'] .= '<li><div class="dropdown"><a>'.$_cone->getName().'</a><div class="drop-list"><div class="wrapper">';
            if($_cone->getID()==$temp[0]){
                $cat_tu=$static_cat_id_sbs;
            }
            else if($_cone->getID()==$temp1[0]){
                $cat_tu=$static_cat_id_ofr;
            }
            foreach ($cat_tu as $_ctwo):
                $_ctwo = Mage::getModel('catalog/category')->load($_ctwo);
                $navigation['mobile'] .= '<span>'.$_ctwo->getName().'</span><div>';
                    if(!empty($_ctwo->getImageUrl())) $navigation['desktop'] .= '<div><div class="col-table full-width"><ul><li><img src="'.$_ctwo->getImageUrl().'" alt="'.$_ctwo->getName().'" border="0"></li><li><h5>'.'<a href="'.$_ctwo->getUrl().'">'.$_ctwo->getName().'</a>'.'</h5>';
                    else $navigation['desktop'] .= '<div><div class="col-table full-width"><ul><li></li><li><h5>'.'<a href="'.$_ctwo->getUrl().'">'.$_ctwo->getName().'</a>'.'</h5>';
                    if(!empty($_ctwo->getChildren())) $_ctwoex = explode(",", $_ctwo->getChildren());
                    else $_ctwoex = array();
                foreach ($_ctwoex as $_cthree):
                    $_cthree = Mage::getModel('catalog/category')->load($_cthree);
                    $navigation['mobile'] .= '<a href="'.$_cthree->getUrl().'">'.$_cthree->getName().'</a>';
                    $navigation['desktop'] .= '<a href="'.$_cthree->getUrl().'">'.$_cthree->getName().'</a>';
                endforeach;
                $navigation['mobile'] .= '</div>';
                $navigation['desktop'] .= '</li></ul></div></div>';
            endforeach;
            $navigation['mobile'] .= '</li>';
            $navigation['desktop'] .= '</div></div></div></li>';
        endforeach;
        return $navigation;
    }

    public function shipping_methods_checkout()
    {
        $ship_block = new Mage_Checkout_Block_Onepage_Shipping_Method_Available();
        $_shippingRateGroups = $ship_block->getShippingRates();
        foreach ($_shippingRateGroups as $code => $_rates){
            $_sole = $_sole && count($_rates) == 1;
            $tempsortship = array();
            foreach ($_rates as $_rate){
                $tempship = $tempsortkey = "";
                $str_method_title = str_replace(' ', '', $this->escapeHtml($_rate->getMethodTitle()));
                $shippingCodePrice[] = "'".$_rate->getCode()."':".(float)$_rate->getPrice();
                $tempship .= "<li>";
                if ($_rate->getErrorMessage()) {
                    $tempship .= '<ul class="messages"><li class="error-msg"><ul><li>'.$this->escapeHtml($_rate->getErrorMessage()).'</li></ul></li></ul>';
                } else {
                    $tempship .= '<span class="no-display"><input name="shipping_method" type="radio" value="'.$this->escapeHtml($_rate->getCode()).'" id="s_method_'.$_rate->getCode().'"';
                    if($_rate->getCode()===$ship_block->getAddressShippingMethod()){
                        $tempship .= ' checked="checked"';
                    }
                    $tempship .= 'class="radio" /></span>';
                    if($_rate->getCode()===$ship_block->getAddressShippingMethod()){
                        echo '<script type="text/javascript">lastPrice = '.(float)$_rate->getPrice().'</script>';
                    }
                    $tempship .= ' <label for="s_method_'.$_rate->getCode().'">';
                    $fin='#\((.*?)\)#';
                    $tempship .= '<span id="'.strtolower($str_method_title).'">';
                    if(strtolower($str_method_title)=='uknextworkingday'):
                        $fin_date=$this->mainCalculationOfWeekends(date('Y-m-d', strtotime("next day")));
                        $tempship .= $fin_date;
                        $tempsortkey = strtotime($fin_date);
                    elseif(strtolower($str_method_title)=='uksaturdaydelivery'):
                        $fin_date= date("l j F",strtotime("next saturday"));
                        $tempship .= $fin_date;
                        $tempsortkey = strtotime($fin_date);
                    elseif(strtolower($str_method_title)=='uksundaydelivery'):
                        $fin_date = date("l j F",strtotime("next sunday"));
                        $tempship .= $fin_date;
                        $tempsortkey = strtotime($fin_date);
                    elseif(strtolower($str_method_title)=='ukpre-10:30amnextworkingday'):
                        $fin_date=$this->mainCalculationOfWeekends(date('Y-m-d', strtotime("next day")));
                        $tempship .= $fin_date." (before 10.30am)";
                        $tempsortkey = strtotime($fin_date)+1;
                    else:
                        preg_match($fin, $_rate->getMethodTitle(), $matches);
                        $days_replace=str_replace(" Days","",str_replace(" Working Days","",$matches[1]));
                        $date_of_delivery = explode("-", $days_replace);
                        if(is_array($date_of_delivery)) {
                            $final_date_val=$date_of_delivery[1];
                        } else {
                            $final_date_val=$days_replace;
                        }
                        $final_date_val="+".$final_date_val." days";
                        $cal_date=date('Y-m-d', strtotime($final_date_val));
                        $fin_date=$this->mainCalculationOfWeekends($cal_date);
                        $tempship .= "by ".$fin_date;
                        $tempsortkey = strtotime($fin_date);
                    endif;
                    $tempship .= '</span>';
                    $_excl = $ship_block->getShippingPrice($_rate->getPrice(), $this->helper('tax')->displayShippingPriceIncludingTax());
                    $_incl = $ship_block->getShippingPrice($_rate->getPrice(), true);
                    $tempship .= strip_tags($_excl);
                    if ($this->helper('tax')->displayShippingBothPrices() && $_incl != $_excl):
                        $tempship .= '('.$this->__('Incl. Tax').''.$_incl.')';
                    endif;
                    $tempship .= '</label>';
                    $tempship .= '</li>';
                }
                $tempsortship[$tempsortkey] = $tempship;
            }
            return $tempsortship;
        }
    }

    public function shipping_methods_cart()
    {
        $ship_block = new Mage_Checkout_Block_Cart_Shipping();
        $_shippingRateGroups = $ship_block->getEstimateRates();
        foreach ($_shippingRateGroups as $code => $_rates){
            $tempsortship = array();
            foreach ($_rates as $_rate){
                $tempship = $tempsortkey = "";
                $str_method_title = str_replace(' ', '', $this->escapeHtml($_rate->getMethodTitle()));
                $tempship .= "<li ";
                if ($_rate->getErrorMessage()) $tempship .= ' class="error-msg"';
                $tempship .= ">";
                if ($_rate->getErrorMessage()):
                    $tempship .= $this->escapeHtml($_rate->getErrorMessage());
                else:
                    $tempship .= '<input name="estimate_method" type="radio" value="'.$this->escapeHtml($_rate->getCode()).'" id="s_method_'.$_rate->getCode().'"';
                    if($_rate->getCode()===$ship_block->getAddressShippingMethod()) $tempship .= ' checked="checked"';
                    $tempship .= 'class="radio" />';
                    $_excl = $ship_block->getShippingPrice($_rate->getPrice(), $this->helper('tax')->displayShippingPriceIncludingTax());
                    $_incl = $ship_block->getShippingPrice($_rate->getPrice(), true);
                    $tempship .= '<label for="s_method_'.$_rate->getCode().'">'.strip_tags($_excl).'</label>';
                    if ($this->helper('tax')->displayShippingBothPrices() && $_incl != $_excl):
                        $tempship .= '('.$this->__('Incl. Tax').''.$_incl.')';
                    endif;
                    $tempship .= '</label>';
                endif;
                $tempship .= '</li>';
                $tempship .= '<li>';
                $fin='#\((.*?)\)#';
                $tempship .= '<label id="'.strtolower($str_method_title).'" for="s_method_'.$_rate->getCode().'">';
                if(strtolower($str_method_title)=='uknextworkingday'):
                    $fin_date=$this->mainCalculationOfWeekends(date('Y-m-d', strtotime("next day")));
                    $tempship .= $fin_date;
                    $tempsortkey = strtotime($fin_date);
                elseif(strtolower($str_method_title)=='uksaturdaydelivery'):
                    $fin_date= date("l j F",strtotime("next saturday"));
                    $tempship .= $fin_date;
                    $tempsortkey = strtotime($fin_date);
                elseif(strtolower($str_method_title)=='uksundaydelivery'):
                    $fin_date = date("l j F",strtotime("next sunday"));
                    $tempship .= $fin_date;
                    $tempsortkey = strtotime($fin_date);
                elseif(strtolower($str_method_title)=='ukpre-10:30amnextworkingday'):
                    $fin_date=$this->mainCalculationOfWeekends(date('Y-m-d', strtotime("next day")));
                    $tempship .= $fin_date." (before 10.30am)";
                    $tempsortkey = strtotime($fin_date)+1;
                else:
                    preg_match($fin, $_rate->getMethodTitle(), $matches);
                    $days_replace=str_replace(" Days","",str_replace(" Working Days","",$matches[1]));
                    $date_of_delivery = explode("-", $days_replace);
                    if(is_array($date_of_delivery)) {
                        $final_date_val=$date_of_delivery[1];
                    } else {
                        $final_date_val=$days_replace;
                    }
                    $final_date_val="+".$final_date_val." days";
                    $cal_date=date('Y-m-d', strtotime($final_date_val));
                    $fin_date=$this->mainCalculationOfWeekends($cal_date);
                    $tempship .= "by ".$fin_date;
                    $tempsortkey = strtotime($fin_date);
                endif;
                $tempship .= '</label>';
                $tempship .= '</li>';
                $tempsortship[$tempsortkey] = $tempship;
            }
            return $tempsortship;
        }
    }

    function mainCalculationOfWeekends($date_to) {
        $days = array("Sat", "Sun");
        $cal_date=$date_to;
        $count=0;
        $count1=0;
        for($i=0;$i<count($days);$i++) {
            $datesArray[$i] = $this->getDateForSpecificDayBetweenDates(date("Y-m-d"),$cal_date, $days[$i]);
            if (!empty($datesArray[$i])) {
                $count++;
            }
        }
        $final_date_val1="+".$count." days";
        $cal_date1=date('Y-m-d', strtotime($cal_date.$final_date_val1));
        for($i=0;$i<count($days);$i++) {
            $datesArray1[$i] = $this->getDateForSpecificDayBetweenDates(date("Y-m-d"), date("Y-m-d",strtotime($cal_date1)), $days[$i]);
            if (!empty($datesArray1[$i])) {
                $count1++;
            }
        }
        if($count==$count1) {
            return date("l j F",strtotime($cal_date1));
        } else {
            $diff_bet_dates=$count1-$count;
            $final_date_val2="+".$diff_bet_dates." days";
            $cal_date2=date('Y-m-d', strtotime($cal_date1.$final_date_val2));
            return date("l j F",strtotime($cal_date2));
        }
    }

    function getDateForSpecificDayBetweenDates($startDate, $endDate, $weekday) {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);
        $dateArr = array();
        do {
            if(date("D", $startDate) != $weekday) {
                $startDate += (24 * 3600); // add 1 day
            }
        } while(date("D", $startDate) != $weekday);
        while($startDate <= $endDate) {
            $dateArr[] = date('Y-m-d', $startDate);
            $startDate += (7 * 24 * 3600); // add 7 days
        }
        return $dateArr;
    }
}
