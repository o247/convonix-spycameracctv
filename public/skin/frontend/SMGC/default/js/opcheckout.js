/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
 
setInterval(function(){
    
    // Shipping & Billing Section
    if($j("#opc-shipping").hasClass('section allow active') || $j("#opc-billing").hasClass('section allow active')) {
        $j('#checkout-step-billing').show();
    }
    
    // Delivery & Payment Section
    if($j("#opc-shipping_method").hasClass('section allow active') && $j(".select-date-holder ul li").length == 1) {
        if(!$j(".select-date-holder ul li").hasClass('selected-ship-opt')) {
            $j(".select-date-holder ul li").addClass('selected-ship-opt');
            $j(".select-date-holder ul li span input").prop('checked', true);        
        }
        $j("button#shipping_method_continue").click();
    } else if($j("#opc-shipping_method").hasClass('section allow active') && $j("#dt_method_authorizenet").hasClass('selected-pay-method')) {
        $j('#opc-shipping_method').removeClass('active');
        $j('#opc-payment').addClass('allow active');
    } else if($j("#opc-shipping_method").hasClass('section allow active')){
        $j("#opc-payment").addClass('allow');
        $j("#checkout-step-payment").show();
    }
    if($j('#opc-payment').hasClass('section allow'))
    {
        $j('#checkout-step-payment').show();
    }
    else if(!$j('#opc-payment').hasClass('active') && $j('#opc-payment').hasClass('section')) 
    {
        $j('#checkout-step-payment').hide();
    }
    if($j('input[name="payment\\[method\\]"]:checked').length == 1) {
        $j('input[name="payment\\[method\\]"]:checked').parent().parent().parent().addClass('selected-pay-method');
    }
    if($j("#opc-shipping_method").hasClass('section allow active') || $j("#opc-payment").hasClass('section allow active')) {
        $j('#opc-billing').removeClass('active');
        $j('#opc-shipping').removeClass('active');
        $j('#checkout-step-billing').hide();
        $j('#checkout-step-shipping').hide();
    }
    
    // Review Section
    if($j("#opc-review").hasClass('section allow active')) {
        $j('#opc-billing').removeClass('active');
        $j('#opc-shipping').removeClass('active');
        $j('#opc-shipping_method').removeClass('active');
        $j('#opc-payment').removeClass('active');
        $j('#checkout-step-billing').hide();
        $j('#checkout-step-shipping').hide();
        $j('#checkout-step-shipping_method').hide();
        $j('#checkout-step-payment').hide();
    }
    
    
    $j(".c2a_results li[data-count='{\"data\"\\:\"1\"}']").click(function(){
        var section = $j(".c2a_active").attr("id");
        if(section == "billing_cc_search_input") {

        if($j("#billing-new-address-form .print-add").length==1){
            var str=$j(this).attr('title');
            var res = str.split(",");
            var addres="";
            if(res[1]!="" && res[1] != null && res[1] != undefined) addres = addres+res[1]+", ";
            if(res[2]!="" && res[2] != null && res[2] != undefined) addres = addres+res[2]+", ";
            if(res[3]!="" && res[3] != null && res[3] != undefined) addres = addres+res[3]+", ";
            if(res[0]!="") addres = addres+res[0];
            document.getElementById('print-bill-add-text').innerHTML = '<div>'+addres+'</div>';
            $j("#billing-new-address-form .print-add").show();
        }
        else
        {
            var str=$j(this).attr('title');
            var res = str.split(",");
            var addres="";
            if(res[1]!="" && res[1] != null && res[1] != undefined) addres = addres+res[1]+", ";
            if(res[2]!="" && res[2] != null && res[2] != undefined) addres = addres+res[2]+", ";
            if(res[3]!="" && res[3] != null && res[3] != undefined) addres = addres+res[3]+", ";
            if(res[0]!="") addres = addres+res[0];
            document.getElementById('sh-address').innerHTML = '<div>'+addres+'</div>';
            $j(".print-add").show();
            $j("#sh-address").show();
            $j("#edit-address").show();
            $j("#billing-new-address-form .wide").hide();
        }
        } else { 
        // update shipping address to LS and in #print-add
            var str1=$j(this).attr('title');
            var res1 = str1.split(",");
            var addres1="";
            if(res1[1]!="" && res[1] != null && res[1] != undefined) addres1 = addres1+res1[1]+", ";
            if(res1[2]!="" && res[2] != null && res[2] != undefined) addres1 = addres1+res1[2]+", ";
            if(res1[3]!="" && res[3] != null && res[3] != undefined) addres1 = addres1+res1[3]+", ";
            if(res1[0]!="") addres1 = addres1+res1[0];
            document.getElementById('print-ship-add-text').innerHTML = '<div>'+addres1+'</div>';
            $j("#shipping-new-address-form .print-add").show();
        }
    });
    if($j("#dt_method_paypal_express").hasClass("selected-pay-method") && $j("#payment_form_paypal_express").is(":visible")) {
    $j("#payment-buttons-container > div > button > span > span").text("Continue to PayPal");
    } else {
    $j("#payment-buttons-container > div > button > span > span").text("Continue");
    }
    
}, 300);
 
var Checkout = Class.create();
Checkout.prototype = {
    initialize: function(accordion, urls){
        this.accordion = accordion;
        this.progressUrl = urls.progress;
        this.reviewUrl = urls.review;
        this.saveMethodUrl = urls.saveMethod;
        this.failureUrl = urls.failure;
        this.billingForm = false;
        this.shippingForm= false;
        this.syncBillingShipping = false;
        this.method = '';
        this.payment = '';
        this.loadWaiting = false;
        this.steps = ['login', 'billing', 'shipping', 'shipping_method', 'payment', 'review'];
        //We use billing as beginning step since progress bar tracks from billing
        this.currentStep = 'billing';

        this.accordion.sections.each(function(section) {
            Event.observe($(section).down('.step-title'), 'click', this._onSectionClick.bindAsEventListener(this));
        }.bind(this));

        this.accordion.disallowAccessToNextSections = true;
    },

    /**
     * Section header click handler
     *
     * @param event
     */
    _onSectionClick: function(event) {
        var section = $(Event.element(event).up().up());
        if (section.hasClassName('allow')) {
            Event.stop(event);
            this.gotoSection(section.readAttribute('id').replace('opc-', ''), false);
            return false;
        }
    },

    ajaxFailure: function(){
        location.href = this.failureUrl;
    },

    reloadProgressBlock: function(toStep) {
        this.reloadStep(toStep);
        if (this.syncBillingShipping) {
            this.syncBillingShipping = false;
            this.reloadStep('shipping');
        }
    },

    reloadStep: function(prevStep) {
        var updater = new Ajax.Updater(prevStep + '-progress-opcheckout', this.progressUrl, {
            method:'get',
            onFailure:this.ajaxFailure.bind(this),
            onComplete: function(){
                this.checkout.resetPreviousSteps();
            },
            parameters:prevStep ? { prevStep:prevStep } : null
        });
    },

    reloadReviewBlock: function(){
        var updater = new Ajax.Updater('checkout-review-load', this.reviewUrl, {method: 'get', onFailure: this.ajaxFailure.bind(this)});
    },

    _disableEnableAll: function(element, isDisabled) {
        var descendants = element.descendants();
        for (var k in descendants) {
            descendants[k].disabled = isDisabled;
        }
        element.disabled = isDisabled;
    },

    setLoadWaiting: function(step, keepDisabled) {
        if (step) {
            if (this.loadWaiting) {
                this.setLoadWaiting(false);
            }
            var container = $(step+'-buttons-container');
            container.addClassName('disabled');
            container.setStyle({opacity:.5});
            this._disableEnableAll(container, true);
            Element.show(step+'-please-wait');
        } else {
            if (this.loadWaiting) {
                var container = $(this.loadWaiting+'-buttons-container');
                var isDisabled = (keepDisabled ? true : false);
                if (!isDisabled) {
                    container.removeClassName('disabled');
                    container.setStyle({opacity:1});
                }
                this._disableEnableAll(container, isDisabled);
                Element.hide(this.loadWaiting+'-please-wait');
            }
        }
        this.loadWaiting = step;
    },

    gotoSection: function (section, reloadProgressBlock) {

        if (reloadProgressBlock) {
            this.reloadProgressBlock(this.currentStep);
        }
        this.currentStep = section;
        var sectionElement = $('opc-' + section);
        sectionElement.addClassName('allow');
        this.accordion.openSection('opc-' + section);
        if(!reloadProgressBlock) {
            this.resetPreviousSteps();
        }
    },

    resetPreviousSteps: function () {
        var stepIndex = this.steps.indexOf(this.currentStep);

        //Clear other steps if already populated through javascript
        for (var i = stepIndex; i < this.steps.length; i++) {
            var nextStep = this.steps[i];
            var progressDiv = nextStep + '-progress-opcheckout';
            if ($(progressDiv)) {
                //Remove the link
                $(progressDiv).select('.changelink').each(function (item) {
                    item.remove();
                });
                $(progressDiv).select('dt').each(function (item) {
                    item.removeClassName('complete');
                });
                //Remove the content
                $(progressDiv).select('dd.complete').each(function (item) {
                    item.remove();
                });
            }
        }
    },

    changeSection: function (section) {
        var changeStep = section.replace('opc-', '');
        this.gotoSection(changeStep, false);
    },

    setMethod: function(){
        if ($('login:guest') && $('login:guest').checked) {
            this.method = 'guest';
            var request = new Ajax.Request(
                this.saveMethodUrl,
                {method: 'post', onFailure: this.ajaxFailure.bind(this), parameters: {method:'guest'}}
            );
            Element.hide('register-customer-password');
            this.gotoSection('billing', true);
            document.getElementById('step-check-billing').className = document.getElementById("step-check-billing").className.replace(/(?:^|\s)disable-accord(?!\S)/g, '');
            if(!document.getElementById('step-check-billing').classList.contains('active-accord')) document.getElementById('step-check-billing').className += " active-accord";
        }
        else if($('login:register') && ($('login:register').checked || $('login:register').type == 'hidden')) {
            this.method = 'register';
            var request = new Ajax.Request(
                this.saveMethodUrl,
                {method: 'post', onFailure: this.ajaxFailure.bind(this), parameters: {method:'register'}}
            );
            Element.show('register-customer-password');
            this.gotoSection('billing', true);
            document.getElementById('step-check-billing').className = document.getElementById("step-check-billing").className.replace(/(?:^|\s)disable-accord(?!\S)/g, '');
            if(!document.getElementById('step-check-billing').classList.contains('active-accord')) document.getElementById('step-check-billing').className += " active-accord";
        }
        else{
            alert(Translator.translate('Please choose to register or to checkout as a guest').stripTags());
            return false;
        }
        document.body.fire('login:setMethod', {method : this.method});
    },

    setBilling: function() {
        if (($('billing:use_for_shipping_yes')) && ($('billing:use_for_shipping_yes').checked)) {
            shipping.syncWithBilling();
            $('opc-shipping').addClassName('allow');
            this.gotoSection('shipping_method', true);
        } else if (($('billing:use_for_shipping_no')) && ($('billing:use_for_shipping_no').checked)) {
            $('shipping:same_as_billing').checked = false;
            this.gotoSection('shipping', true);
        } else {
            $('shipping:same_as_billing').checked = true;
            this.gotoSection('shipping', true);
        }

    },

    setShipping: function() {
        this.gotoSection('shipping_method', true);
    },

    setShippingMethod: function() {
        this.gotoSection('payment', true);
    },

    setPayment: function() {
        this.gotoSection('review', true);
    },

    setReview: function() {
        this.reloadProgressBlock();
    },

    back: function(){
        if (this.loadWaiting) return;
        //Navigate back to the previous available step
        var stepIndex = this.steps.indexOf(this.currentStep);
        var section = this.steps[--stepIndex];
        var sectionElement = $('opc-' + section);

        //Traverse back to find the available section. Ex Virtual product does not have shipping section
        while (sectionElement === null && stepIndex > 0) {
            --stepIndex;
            section = this.steps[stepIndex];
            sectionElement = $('opc-' + section);
        }
        this.changeSection('opc-' + section);
    },

    setStepResponse: function(response){
        if (response.update_section) {
            $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
        }
        if (response.allow_sections) {
            response.allow_sections.each(function(e){
                $('opc-'+e).addClassName('allow');
            });
        }

        if(response.duplicateBillingInfo)
        {
            this.syncBillingShipping = true;
            shipping.setSameAsBilling(true);
        }

        if (response.goto_section) {
            this.gotoSection(response.goto_section, true);
            return true;
        }
        if (response.redirect) {
            location.href = response.redirect;
            return true;
        }
        return false;
    }
}

// billing
var Billing = Class.create();
Billing.prototype = {
    initialize: function(form, addressUrl, saveUrl){
        this.form = form;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        this.addressUrl = addressUrl;
        this.saveUrl = saveUrl;
        this.onAddressLoad = this.fillForm.bindAsEventListener(this);
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    setAddress: function(addressId){
        if (addressId) {
            request = new Ajax.Request(
                this.addressUrl+addressId,
                {method:'get', onSuccess: this.onAddressLoad, onFailure: checkout.ajaxFailure.bind(checkout)}
            );
        }
        else {
            this.fillForm(false);
        }
    },

    newAddress: function(isNew){
        if (isNew) {
            this.resetSelectedAddress();
            Element.show('billing-new-address-form');
        } else {
            Element.hide('billing-new-address-form');
        }
    },

    resetSelectedAddress: function(){
        var selectElement = $('billing-address-select')
        if (selectElement) {
            selectElement.value='';
        }
    },

    fillForm: function(transport){
        var elementValues = {};
        if (transport && transport.responseText){
            try{
                elementValues = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                elementValues = {};
            }
        }
        else{
            this.resetSelectedAddress();
        }
        arrElements = Form.getElements(this.form);
        for (var elemIndex in arrElements) {
            if (arrElements[elemIndex].id) {
                var fieldName = arrElements[elemIndex].id.replace(/^billing:/, '');
                arrElements[elemIndex].value = elementValues[fieldName] ? elementValues[fieldName] : '';
                if (fieldName == 'country_id' && billingForm){
                    billingForm.elementChildLoad(arrElements[elemIndex]);
                }
            }
        }
    },

    setUseForShipping: function(flag) {
        $('shipping:same_as_billing').checked = flag;
    },

    save: function(){
        var loadedAdd;
        var objAddress;
        if (checkout.loadWaiting!=false) return;

        var validator = new Validation(this.form);
        if (validator.validate()) {
            checkout.setLoadWaiting('billing');

            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
            //filling localstorage with new address
                var bill_street=document.getElementById('billing:street1').value;
                var bill_city=document.getElementById('billing:city').value;
                var bill_post_code=document.getElementById('billing:postcode').value;
                var bill_first_name=document.getElementById('billing:firstname').value;
                var bill_last_name=document.getElementById('billing:lastname').value;
                var bill_telephone=document.getElementById('billing:telephone').value;
                var bill_company=document.getElementById('billing:company').value;
                var bill_country_id=document.getElementById('billing:country_id').value;
                var bill_region_id=document.getElementById('billing:region_id').value;
                var bill_region=document.getElementById('billing:region').value;
                var bill_vat=document.getElementById('billing:vat_id').value;
                
                var reg_nonreg=document.getElementById('bill_reg_nonreg').value;
                if(reg_nonreg==1)
                {
                    var bill_address_id=document.getElementById('billing-address-select').value;
                    var bill_database_address_id=document.getElementById('billing:address_id').value;
                    if(bill_address_id!="")
                    {
                        var dataString = 'bill_address_id='+ bill_address_id;
                        $j.ajax({
                            type: "POST",
                            url: "/smgcapi/index/get_register_user_address",
                            data: dataString,
                            cache: false,
                            async: false,
                            success: function(result){
                                loadedAdd = JSON.parse(result);
                                objAddress = {aid: bill_address_id, firstname: loadedAdd.firstname, lastname: loadedAdd.lastname, email: '', tel: loadedAdd.tel, company: loadedAdd.company, line_1: loadedAdd.street[0], line_2: loadedAdd.street[1], town: loadedAdd.region, postcode: loadedAdd.postcode, county: loadedAdd.city, country: loadedAdd.country, vat: bill_vat, region_id: loadedAdd.region_id, database_address_id:bill_database_address_id};
                            }
                        });
                    }
                    else
                    {
                        objAddress = {aid: bill_address_id, firstname: bill_first_name, lastname: bill_last_name, email: bill_email, tel: bill_telephone, company: bill_company, line_1: bill_street, line_2: '', town: bill_region, postcode: bill_post_code, county: bill_city, country: bill_country_id, vat: bill_vat, region_id: bill_region_id, database_address_id:bill_database_address_id};
                    }
                }
                else
                {
                    var bill_address_id='';
                    var bill_database_address_id='';
                    var bill_email=document.getElementById('billing:email').value;
                    objAddress = {aid: bill_address_id, firstname: bill_first_name, lastname: bill_last_name, email: bill_email, tel: bill_telephone, company: bill_company, line_1: bill_street, line_2: '', town: bill_region, postcode: bill_post_code, county: bill_city, country: bill_country_id, vat: bill_vat, region_id: bill_region_id, database_address_id:bill_database_address_id};
                }
                
                
                if (($('billing:use_for_shipping_yes')) && ($('billing:use_for_shipping_yes').checked)) {
                    localStorage.setItem('billing_address', JSON.stringify(objAddress));
                    localStorage.setItem('shipping_address', JSON.stringify(objAddress));
                }
                else
                {
                    localStorage.setItem('billing_address', JSON.stringify(objAddress));
                }
                
            var loadedAddress = JSON.parse(localStorage.getItem("billing_address"));
            var formatAddress = "";
            if(loadedAddress.firstname != "" && loadedAddress.firstname != null && loadedAddress.firstname != undefined) formatAddress = formatAddress + loadedAddress.firstname;
            if(loadedAddress.lastname != "" && loadedAddress.lastname != null && loadedAddress.lastname != undefined) formatAddress = formatAddress + " " + loadedAddress.lastname + ", ";
            if(loadedAddress.company != "" && loadedAddress.company != null && loadedAddress.company != undefined) formatAddress = formatAddress + loadedAddress.company + ", ";
            if(loadedAddress.line_1 != "") formatAddress = formatAddress + loadedAddress.line_1 + ", ";
            if(loadedAddress.line_2 != "" && loadedAddress.line_2 != null && loadedAddress.line_2 != undefined) formatAddress = formatAddress + loadedAddress.line_2 + ", ";
            if(loadedAddress.town != "" && loadedAddress.town != null && loadedAddress.town != undefined) formatAddress = formatAddress + loadedAddress.town + ", ";
            if(loadedAddress.postcode != "") formatAddress = formatAddress + loadedAddress.postcode + ", ";
            formatAddress = formatAddress.substring(0, formatAddress.length - 2);
            document.getElementById('billing-add').innerHTML = '<div>'+formatAddress+'</div>';
            
            if (($('billing:use_for_shipping_no')) && ($('billing:use_for_shipping_no').checked)) {                
                $('co-shipping-form').show();
                document.querySelector('#shipping-new-address-form .wide').style.display = 'block';
                document.getElementById('shipping:firstname').value = document.getElementById('billing:firstname').value;
                document.getElementById('shipping:lastname').value = document.getElementById('billing:lastname').value;
                document.getElementById('shipping:telephone').value = document.getElementById('billing:telephone').value;
                document.getElementById("checkout-step-billing").style.display = 'block !important';
            } else {
                document.getElementById('step-check-billing').className = document.getElementById("step-check-billing").className.replace(/(?:^|\s)active-accord(?!\S)/g, '');
                document.getElementById('step-check-billing').className += " done-accord";
                
                document.getElementById('step-check-shipping_method').className = document.getElementById("step-check-shipping_method").className.replace(/(?:^|\s)disable-accord(?!\S)/g, '');
                document.getElementById('step-check-shipping_method').className += " active-accord";
            }
        }
    },

    resetLoadWaiting: function(transport){
        checkout.setLoadWaiting(false);
        document.body.fire('billing-request:completed', {transport: transport});
    },

    /**
     This method recieves the AJAX response on success.
     There are 3 options: error, redirect or html with shipping options.
     */
    nextStep: function(transport){
        if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }

        if (response.error){
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                if (window.billingRegionUpdater) {
                    billingRegionUpdater.update();
                }

                alert(response.message.join("\n"));
            }

            return false;
        }

        checkout.setStepResponse(response);
        payment.initWhatIsCvvListeners();

    }
}

// shipping
var Shipping = Class.create();
Shipping.prototype = {
    initialize: function(form, addressUrl, saveUrl, methodsUrl){
        this.form = form;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        this.addressUrl = addressUrl;
        this.saveUrl = saveUrl;
        this.methodsUrl = methodsUrl;
        this.onAddressLoad = this.fillForm.bindAsEventListener(this);
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    setAddress: function(addressId){
        if (addressId) {
            request = new Ajax.Request(
                this.addressUrl+addressId,
                {method:'get', onSuccess: this.onAddressLoad, onFailure: checkout.ajaxFailure.bind(checkout)}
            );
        }
        else {
            this.fillForm(false);
        }
    },

    newAddress: function(isNew){
        if (isNew) {
            this.resetSelectedAddress();
            Element.show('shipping-new-address-form');
        } else {
            Element.hide('shipping-new-address-form');
        }
        shipping.setSameAsBilling(false);
    },

    resetSelectedAddress: function(){
        var selectElement = $('shipping-address-select')
        if (selectElement) {
            selectElement.value='';
        }
    },

    fillForm: function(transport){
        var elementValues = {};
        if (transport && transport.responseText){
            try{
                elementValues = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                elementValues = {};
            }
        }
        else{
            this.resetSelectedAddress();
        }
        arrElements = Form.getElements(this.form);
        for (var elemIndex in arrElements) {
            if (arrElements[elemIndex].id) {
                var fieldName = arrElements[elemIndex].id.replace(/^shipping:/, '');
                arrElements[elemIndex].value = elementValues[fieldName] ? elementValues[fieldName] : '';
                if (fieldName == 'country_id' && shippingForm){
                    shippingForm.elementChildLoad(arrElements[elemIndex]);
                }
            }
        }
    },

    setSameAsBilling: function(flag) {
        $('shipping:same_as_billing').checked = flag;
// #5599. Also it hangs up, if the flag is not false
//        $('billing:use_for_shipping_yes').checked = flag;
        if (flag) {
            this.syncWithBilling();
        }
    },

    syncWithBilling: function () {
        $('billing-address-select') && this.newAddress(!$('billing-address-select').value);
        $('shipping:same_as_billing').checked = true;
        if (!$('billing-address-select') || !$('billing-address-select').value) {
            arrElements = Form.getElements(this.form);
            for (var elemIndex in arrElements) {
                if (arrElements[elemIndex].id) {
                    var sourceField = $(arrElements[elemIndex].id.replace(/^shipping:/, 'billing:'));
                    if (sourceField){
                        arrElements[elemIndex].value = sourceField.value;
                    }
                }
            }
            shippingRegionUpdater.update();
            $('shipping:region_id').value = $('billing:region_id').value;
            $('shipping:region').value = $('billing:region').value;
        } else {
            $('shipping-address-select').value = $('billing-address-select').value;
        }
    },

    setRegionValue: function(){
        $('shipping:region').value = $('billing:region').value;
    },

    save: function(){
        var loadedA = JSON.parse(localStorage.getItem("billing_address"));
        
        var address_pass_bill ='shipping_address_id='+loadedA.aid+'&shipping_address_id_radio='+loadedA.aid+'&shipping%5Baddress_id%5D='+loadedA.database_address_id+'&shipping%5Bfirstname%5D='+loadedA.firstname+'&shipping%5Blastname%5D='+loadedA.lastname+'&shipping%5Bcompany%5D='+loadedA.company+'&shipping%5Bstreet%5D%5B%5D='+loadedA.line_1+'&shipping%5Bcity%5D='+loadedA.county+'&shipping%5Bregion_id%5D='+loadedA.region_id+'&shipping%5Bregion%5D='+loadedA.town+'&shipping%5Bpostcode%5D='+loadedA.postcode+'&shipping%5Bcountry_id%5D='+loadedA.country+'&shipping%5Btelephone%5D='+loadedA.tel+'&shipping%5Bfax%5D=&shipping%5Bvat_id%5D='+loadedA.vat+'';
        
        if (checkout.loadWaiting!=false) return;
        var validator = new Validation(this.form);
        if (validator.validate()) {
            checkout.setLoadWaiting('shipping');
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: address_pass_bill
                }
            );
            
                var ship_street=document.getElementById('shipping:street1').value;
                var ship_city=document.getElementById('shipping:city').value;
                var ship_post_code=document.getElementById('shipping:postcode').value;
                var ship_first_name=document.getElementById('shipping:firstname').value;
                var ship_last_name=document.getElementById('shipping:lastname').value;
                var ship_telephone=document.getElementById('shipping:telephone').value;
                var ship_company=document.getElementById('shipping:company').value;
                var ship_country_id=document.getElementById('shipping:country_id').value;
                var ship_region_id=document.getElementById('shipping:region_id').value;
                var ship_region=document.getElementById('shipping:region').value;
                
                var reg_nonreg=document.getElementById('ship_reg_nonreg').value;
                if(reg_nonreg==1)
                {
                    var ship_address_id=document.getElementById('shipping-address-select').value;
                    var ship_database_address_id=document.getElementById('shipping:address_id').value;
                }
                else
                {
                    var ship_address_id='';
                    var ship_database_address_id='';
                }
                var ship_vat=document.getElementById('shipping:vat_id').value;
                
                var objAddress = {aid: ship_address_id, firstname: ship_first_name, lastname: ship_last_name, tel: ship_telephone, company: ship_company, line_1: ship_street, line_2: '', town: ship_region, postcode: ship_post_code, county: ship_city, country: ship_country_id, vat: ship_vat, region_id: ship_region_id, database_address_id:ship_database_address_id};
                localStorage.setItem('shipping_address', JSON.stringify(objAddress));
            
            
            var loadedAddress = JSON.parse(localStorage.getItem("shipping_address"));
            var formatAddress = "";
            if(loadedAddress.firstname != "" && loadedAddress.firstname != null && loadedAddress.firstname != undefined) formatAddress = formatAddress + loadedAddress.firstname;
            if(loadedAddress.lastname != "" && loadedAddress.lastname != null && loadedAddress.lastname != undefined) formatAddress = formatAddress + " " + loadedAddress.lastname + ", ";
            if(loadedAddress.company != "") formatAddress = formatAddress + loadedAddress.company + ", ";
            if(loadedAddress.line_1 != "") formatAddress = formatAddress + loadedAddress.line_1 + ", ";
            if(loadedAddress.line_2 != "") formatAddress = formatAddress + loadedAddress.line_2 + ", ";
            if(loadedAddress.town != "") formatAddress = formatAddress + loadedAddress.town + ", ";
            if(loadedAddress.postcode != "") formatAddress = formatAddress + loadedAddress.postcode + ", ";
            formatAddress = formatAddress.substring(0, formatAddress.length - 2);
            document.getElementById('shipping-add').innerHTML = '<div>'+formatAddress+'</div>';
            
            document.getElementById('step-check-billing').className = document.getElementById("step-check-billing").className.replace(/(?:^|\s)active-accord(?!\S)/g, '');
            document.getElementById('step-check-billing').className += " done-accord";
            
            document.getElementById('step-check-shipping_method').className = document.getElementById("step-check-shipping_method").className.replace(/(?:^|\s)disable-accord(?!\S)/g, '');
            document.getElementById('step-check-shipping_method').className += " active-accord";
                
            document.getElementById("checkout-step-billing").style.display = 'none';
        }
    },

    resetLoadWaiting: function(transport){
        checkout.setLoadWaiting(false);
    },

    nextStep: function(transport){
        if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }
        if (response.error){
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                if (window.shippingRegionUpdater) {
                    shippingRegionUpdater.update();
                }
                alert(response.message.join("\n"));
            }

            return false;
        }

        checkout.setStepResponse(response);
    }
}

// shipping method
var ShippingMethod = Class.create();
ShippingMethod.prototype = {
    initialize: function(form, saveUrl){
        this.form = form;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        this.saveUrl = saveUrl;
        this.validator = new Validation(this.form);
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    validate: function() {
        var methods = document.getElementsByName('shipping_method');
        if (methods.length==0) {
            alert(Translator.translate('Your order cannot be completed at this time as there is no shipping methods available for it. Please make necessary changes in your shipping address.').stripTags());
            return false;
        }

        if(!this.validator.validate()) {
            return false;
        }

        for (var i=0; i<methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert(Translator.translate('Please specify shipping method.').stripTags());
        return false;
    },

    save: function(){
        if (checkout.loadWaiting!=false) return;
        if (this.validate()) {
            checkout.setLoadWaiting('shipping-method');
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
        }
    },

    resetLoadWaiting: function(transport){
        checkout.setLoadWaiting(false);
    },

    nextStep: function(transport){
        if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }

        if (response.error) {
            alert(response.message);
            return false;
        }

        if (response.update_section) {
            $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
        }

        payment.initWhatIsCvvListeners();

        if (response.goto_section) {
            checkout.gotoSection(response.goto_section, true);
            checkout.reloadProgressBlock();
            return;
        }

        if (response.payment_methods_html) {
            $('checkout-payment-method-load').update(response.payment_methods_html);
        }

        checkout.setShippingMethod();
    }
}


// payment
var Payment = Class.create();
Payment.prototype = {
    beforeInitFunc:$H({}),
    afterInitFunc:$H({}),
    beforeValidateFunc:$H({}),
    afterValidateFunc:$H({}),
    initialize: function(form, saveUrl){
        this.form = form;
        this.saveUrl = saveUrl;
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    addBeforeInitFunction : function(code, func) {
        this.beforeInitFunc.set(code, func);
    },

    beforeInit : function() {
        (this.beforeInitFunc).each(function(init){
            (init.value)();;
        });
    },

    init : function () {
        this.beforeInit();
        var elements = Form.getElements(this.form);
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        var method = null;
        for (var i=0; i<elements.length; i++) {
            if (elements[i].name=='payment[method]') {
                if (elements[i].checked) {
                    method = elements[i].value;
                }
            } else {
                elements[i].disabled = true;
            }
            elements[i].setAttribute('autocomplete','off');
        }
        if (method) this.switchMethod(method);
        this.afterInit();
    },

    addAfterInitFunction : function(code, func) {
        this.afterInitFunc.set(code, func);
    },

    afterInit : function() {
        (this.afterInitFunc).each(function(init){
            (init.value)();
        });
    },

    switchMethod: function(method){
        if (this.currentMethod && $('payment_form_'+this.currentMethod)) {
            this.changeVisible(this.currentMethod, true);
            $('payment_form_'+this.currentMethod).fire('payment-method:switched-off', {method_code : this.currentMethod});
        }
        if ($('payment_form_'+method)){
            this.changeVisible(method, false);
            $('payment_form_'+method).fire('payment-method:switched', {method_code : method});
        } else {
            //Event fix for payment methods without form like "Check / Money order"
            document.body.fire('payment-method:switched', {method_code : method});
        }
        if (method == 'free' && quoteBaseGrandTotal > 0.0001
            && !(($('use_reward_points') && $('use_reward_points').checked) || ($('use_customer_balance') && $('use_customer_balance').checked))
        ) {
            if ($('p_method_' + method)) {
                $('p_method_' + method).checked = false;
                if ($('dt_method_' + method)) {
                    $('dt_method_' + method).hide();
                }
                if ($('dd_method_' + method)) {
                    $('dd_method_' + method).hide();
                }
            }
            method == '';
        }
        if (method) {
            this.lastUsedMethod = method;
        }
        this.currentMethod = method;
    },

    changeVisible: function(method, mode) {
        var block = 'payment_form_' + method;
        [block + '_before', block, block + '_after'].each(function(el) {
            element = $(el);
            if (element) {
                element.style.display = (mode) ? 'none' : '';
                element.select('input', 'select', 'textarea', 'button').each(function(field) {
                    field.disabled = mode;
                });
            }
        });
    },

    addBeforeValidateFunction : function(code, func) {
        this.beforeValidateFunc.set(code, func);
    },

    beforeValidate : function() {
        var validateResult = true;
        var hasValidation = false;
        (this.beforeValidateFunc).each(function(validate){
            hasValidation = true;
            if ((validate.value)() == false) {
                validateResult = false;
            }
        }.bind(this));
        if (!hasValidation) {
            validateResult = false;
        }
        return validateResult;
    },

    validate: function() {
        var result = this.beforeValidate();
        if (result) {
            return true;
        }
        var methods = document.getElementsByName('payment[method]');
        if (methods.length==0) {
            alert(Translator.translate('Your order cannot be completed at this time as there is no payment methods available for it.').stripTags());
            return false;
        }
        for (var i=0; i<methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        result = this.afterValidate();
        if (result) {
            return true;
        }
        alert(Translator.translate('Please specify payment method.').stripTags());
        return false;
    },

    addAfterValidateFunction : function(code, func) {
        this.afterValidateFunc.set(code, func);
    },

    afterValidate : function() {
        var validateResult = true;
        var hasValidation = false;
        (this.afterValidateFunc).each(function(validate){
            hasValidation = true;
            if ((validate.value)() == false) {
                validateResult = false;
            }
        }.bind(this));
        if (!hasValidation) {
            validateResult = false;
        }
        return validateResult;
    },

    save: function(){
        if (checkout.loadWaiting!=false) return;
        var validator = new Validation(this.form);
        if (this.validate() && validator.validate()) {
            checkout.setLoadWaiting('payment');
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
            $('checkout-step-shipping_method').hide();
                        
            document.getElementById('step-check-shipping_method').className = document.getElementById("step-check-shipping_method").className.replace(/(?:^|\s)active-accord(?!\S)/g, '');
            document.getElementById('step-check-shipping_method').className += " done-accord";
            
            document.getElementById('step-check-review').className = document.getElementById("step-check-review").className.replace(/(?:^|\s)disable-accord(?!\S)/g, '');
            document.getElementById('step-check-review').className += " active-accord";
            
            if(document.getElementById('p_method_authorizenet').checked) {
                var cr_no = document.getElementById('authorizenet_cc_number').value;
                var lastFour = cr_no.substr(cr_no.length - 4);
                var tempo='XXXX XXXX XXXX '+lastFour;
                document.getElementById('payment_method_info').innerHTML = '<div>'+tempo+'</div>';
            }
            
            var ship_m = document.getElementById('ship_meth_display').value;
            document.getElementById('shipping_method_info').innerHTML = '<div>'+ship_m+'</div>';
            
        }
    },

    resetLoadWaiting: function(){
        checkout.setLoadWaiting(false);
    },

    nextStep: function(transport){
        if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }
        /*
         * if there is an error in payment, need to show error message
         */
        if (response.error) {
            if (response.fields) {
                var fields = response.fields.split(',');
                for (var i=0;i<fields.length;i++) {
                    var field = null;
                    if (field = $(fields[i])) {
                        Validation.ajaxError(field, response.error);
                    }
                }
                return;
            }
            if (typeof(response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.error);
            }
            return;
        }

        checkout.setStepResponse(response);
    },

    initWhatIsCvvListeners: function(){
        $$('.cvv-what-is-this').each(function(element){
            Event.observe(element, 'click', toggleToolTip);
        });
    }
}

var Review = Class.create();
Review.prototype = {
    initialize: function(saveUrl, successUrl, agreementsForm){
        this.saveUrl = saveUrl;
        this.successUrl = successUrl;
        this.agreementsForm = agreementsForm;
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    save: function(){
        if (checkout.loadWaiting!=false) return;
        checkout.setLoadWaiting('review');
        var params = Form.serialize(payment.form);
        if (this.agreementsForm) {
            params += '&'+Form.serialize(this.agreementsForm);
        }
        params.save = true;
        var request = new Ajax.Request(
            this.saveUrl,
            {
                method:'post',
                parameters:params,
                onComplete: this.onComplete,
                onSuccess: this.onSave,
                onFailure: checkout.ajaxFailure.bind(checkout)
            }
        );        
        document.getElementById('step-check-review').className = document.getElementById("step-check-review").className.replace(/(?:^|\s)active-accord(?!\S)/g, '');
        document.getElementById('step-check-review').className += " done-accord";
        
        var reg_nonreg=document.getElementById('bill_reg_nonreg').value;
        if(reg_nonreg==0)
        {
            var email = document.getElementById('billing:email').value;
            if(document.getElementById('newsletter_check').checked)
            {
                localStorage.setItem('newsletter', email);
            }
        }
    },

    resetLoadWaiting: function(transport){
        checkout.setLoadWaiting(false, this.isSuccess);
    },

    nextStep: function(transport){
        if (transport && transport.responseText) {
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
            if (response.redirect) {
                this.isSuccess = true;
                location.href = response.redirect;
                return;
            }
            if (response.success) {
                this.isSuccess = true;
                window.location=this.successUrl;
            }
            else{
                var msg = response.error_messages;
                if (typeof(msg)=='object') {
                    msg = msg.join("\n");
                }
                if (msg) {
                    alert(msg);
                }
            }

            if (response.update_section) {
                $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
            }

            if (response.goto_section) {
                checkout.gotoSection(response.goto_section, true);
            }
        }
    },

    isSuccess: false
}
