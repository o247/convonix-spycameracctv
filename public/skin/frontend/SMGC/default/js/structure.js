$j(window).load(function(){

    $j('footer').prepend($j('.addthis-smartlayers-mobile')).delay(1000);

    $j('.cart-right-content').append($j('#shopping-cart-totals-table, #button-checkout'));

    if ( $j('#checkout-step-billing #show-address').length === 1 ) {
        $j('#show-address').append($j('#billing-new-address-form .wide'));
    }

    $j('#billing_cc_search_input').parent().parent().addClass('moved-search-add');

    $j('.rating').on('click',function(){
        $j('#reviews').trigger('click');
    });



    if ($j('body').width() >= 768) {
        $j('.single-product-slider li div a img').css('max-height','300px');
        $j('.single-product-slider li div a img').css('max-width','300px');
        $j('.single-product-slider li div a, .single-product-slider li div a img').css('height','300px');

        $j('.single-product-slider li div a img').css('max-height','300px');
        $j('.single-product-slider li div a img').css('max-width','300px');
        $j('.single-product-slider li div a, .single-product-slider li div a img').css('height','300px');
    } else if ($j('body').width() <= 767) {
        $j('.single-product-slider li div a img').css('max-height','300px');
        $j('.single-product-slider li div a img').css('max-width','300px');
        $j('.single-product-slider li div a').css('height','270px');
        $j('.single-product-slider li div a img').css('height','auto');
    }

});

$j('#form-validate .fieldset .haveWetToggle input[type="text"]').css('display','none');


$j(document).ready(function(){

  $j('.main-nav .wrapper > ul > li > .col-table > ul > li:not(:first-child)').append($j('.dropdown-overlay'));

  var inputText = $j('.floating-sort span input').siblings('label').text();
  var currentSort = $j('#sort-select-box option:selected').text();

  var sortArr = [];
  sortArr.push('-');
  $j('#sort-select-box option').each(function(){
    sortArr.push($j(this).text().trim());
  });

  $j('.floating-sort span').each(function() {
    if ($j(this).children('label').text() == currentSort) {
      $j(this).children('input').attr('checked','checked');
    }
    if(!($j.inArray($j(this).children('label').text().trim(), sortArr))) {
      $j(this).css('display','none');
    }
  });

  $j('.floating-sort span input').click(function() {
    var inputText = $j(this).siblings('label').text();
    var sortList = $j('#sort-select-box option').text();
    $j('#sort-select-box option').each(function(){
      $j('#sort-select-box option').removeAttr('selected');
      if($j(this).text() == inputText){
        $j(this).attr('selected','selected');
        setLocation($j('#sort-select-box').val());
      }
    });
    $j('.filter-ajax-loader').show();
  });

    $j('.reset-filters').click(function() {
        window.location.href=document.location.origin+location.pathname;
    });

    $j('.haveCouponFld').css('display','none');

    $j('#haveCoupon').change(function(){
        if(this.checked)
            $j('.haveCouponFld').css('display','flex');
        else
            $j('.haveCouponFld').css('display','none');
    });

    $j('#haveWet').change(function(){
        if(this.checked)
            $j('#form-validate .fieldset .haveWetToggle input[type="text"]').css('display','flex');
        else
            $j('#form-validate .fieldset .haveWetToggle input[type="text"]').css('display','none');
    });

    $j('#authorizenet_cc_type_cvv_div div.input-box:first-child').append($j('#payment-tool-tip'));

    $j('.cvv-what-is-this').mouseover(function() {
        $j('#payment-tool-tip').show();
    });
    $j('.cvv-what-is-this').mouseout(function() {
        $j('#payment-tool-tip').hide();
    });

    $j('.rating').mouseover(function() {
        $j('.main-rat-box-holder').show();
    });

    $j('.rating').mouseout(function() {
        $j('.main-rat-box-holder').hide();
    });

    $j('.barChart').barChart({easing: 'easeOutQuart'});

    $j('#edit-address').click(function() {
        $j(this).prev('#sh-address').hide();
        $j(this).hide();
    });

    if(!$j("li#billing-new-address-form li.print-add").is(":visible")) $j("li#billing-new-address-form li.print-add").hide();
    if(!$j("li#shipping-new-address-form li.print-add").is(":visible")) $j("li#shipping-new-address-form li.print-add").hide();

    $j('#checkout-payment-method-load .payment-holder > div').click(function() {
        $j('#checkout-payment-method-load .payment-holder > div').removeClass('selected-pay-method');
        $j(this).addClass('selected-pay-method');
    });

    $j('span.security').click(function() {
    $j('.banner').removeClass('covert');
    $j('.banner').removeClass('wildlife');
    $j('.banner').removeClass('lifestyle');
    $j('.banner').addClass('security');
    $j('span.security').addClass('security-tab-selected');
    $j('span.covert').removeClass('covert-tab-selected');
    $j('span.wildlife').removeClass('wildlife-tab-selected');
    $j('span.lifestyle').removeClass('lifestyle-tab-selected');
    $j('p.security-copy').addClass('show');
    $j('p.covert-copy').removeClass('show');
    $j('p.wildlife-copy').removeClass('show');
    $j('p.lifestyle-copy').removeClass('show');
    $j('.security-button').addClass('show');
    $j('.covert-button').removeClass('show');
    $j('.wildlife-button').removeClass('show');
    $j('.lifestyle-button').removeClass('show');
});

$j('span.covert').click(function() {
    $j('.banner').removeClass('security');
    $j('.banner').removeClass('wildlife');
    $j('.banner').removeClass('lifestyle');
    $j('.banner').addClass('covert');
    $j('span.covert').addClass('covert-tab-selected');
    $j('span.security').removeClass('security-tab-selected');
    $j('span.wildlife').removeClass('wildlife-tab-selected');
    $j('span.lifestyle').removeClass('lifestyle-tab-selected');
    $j('p.covert-copy').addClass('show');
    $j('p.security-copy').removeClass('show');
    $j('p.wildlife-copy').removeClass('show');
    $j('p.lifestyle-copy').removeClass('show');
    $j('.covert-button').addClass('show');
    $j('.security-button').removeClass('show');
    $j('.wildlife-button').removeClass('show');
    $j('.lifestyle-button').removeClass('show');
});

$j('span.wildlife').click(function() {
    $j('.banner').removeClass('security');
    $j('.banner').removeClass('covert');
    $j('.banner').removeClass('lifestyle');
    $j('.banner').addClass('wildlife');
    $j('span.wildlife').addClass('wildlife-tab-selected');
    $j('span.covert').removeClass('covert-tab-selected');
    $j('span.security').removeClass('security-tab-selected');
    $j('span.lifestyle').removeClass('lifestyle-tab-selected');
    $j('p.wildlife-copy').addClass('show');
    $j('p.covert-copy').removeClass('show');
    $j('p.security-copy').removeClass('show');
    $j('p.lifestyle-copy').removeClass('show');
    $j('.wildlife-button').addClass('show');
    $j('.security-button').removeClass('show');
    $j('.covert-button').removeClass('show');
    $j('.lifestyle-button').removeClass('show');
});

$j('span.lifestyle').click(function() {
    $j('.banner').removeClass('security');
    $j('.banner').removeClass('wildlife');
    $j('.banner').removeClass('covert');
    $j('.banner').addClass('lifestyle');
    $j('span.lifestyle').addClass('lifestyle-tab-selected');
    $j('span.covert').removeClass('covert-tab-selected');
    $j('span.wildlife').removeClass('wildlife-tab-selected');
    $j('span.security').removeClass('security-tab-selected');
    $j('p.lifestyle-copy').addClass('show');
    $j('p.covert-copy').removeClass('show');
    $j('p.wildlife-copy').removeClass('show');
    $j('p.security-copy').removeClass('show');
    $j('.lifestyle-button').addClass('show');
    $j('.security-button').removeClass('show');
    $j('.covert-button').removeClass('show');
    $j('.wildlife-button').removeClass('show');
});

if($j(".fancybox")[0]){

    $j('.fancybox').fancybox(
    {
        mouseWheel: false
    }
    );
}
$j('.quick-view-holder .filled-close-btn').click(function() {
    $j('.gray-overlay').removeClass('gray-overlay-opened');
});

$j('.product-page > ul > li:last-child .gray-overlay').click(function() {
    $j(this).removeClass('gray-overlay-opened');
});

$j('.product-page > ul > li:last-child .gray-overlay .quick-view-holder').click(function(event) {
    event.stopPropagation();
});


$j('dl dt').click(function(){
    $j(this).next().toggleClass("show").siblings('dd').removeClass("show");
    $j(this).toggleClass('accord-active').siblings().removeClass('accord-active');
    return false;
});

$j('.currency').click(function() {
    $j('.curr-list').addClass('curr-list-opened');
});

$j('.curr-list > span, .curr-list .filled-close-btn').click(function() {
    $j('.curr-list').removeClass('curr-list-opened');
});

$j('.currency-drop > span, .curr-list > span').click(function(){
    var classer = $j(this).attr('class');
    $j('select.disabled-currency-drop option').removeAttr("selected");
    $j('select.disabled-currency-drop option').each(function(){
        if($j(this).attr('id')==classer){
            $j(this).attr("selected","selected");
            setLocation($j('select.disabled-currency-drop').val());
        }
    });
    $j('.filter-ajax-loader').show();
});
var currentCurrency = $j('select.disabled-currency-drop option:selected').attr('id');
$j('.selected-currency').attr('id', currentCurrency);

var currencyIdName = $j('.selected-currency').attr('id');
$j('.selected-currency').text(currencyIdName);

$j('.currency').attr('id', currentCurrency);

$j('.curr-list > span').removeClass('hide');

$j('.vat-toggle').on('click', function() {
    $j(this).toggleClass('vat-toggle-active');
    $j('.vat-toggle > div > ul > li:last-child').text('Show inc VAT prices');
    if ($j('.vat-toggle').hasClass('vat-toggle-active')) {
    $j('.vat-toggle > div > ul > li:last-child').text('Show inc VAT prices');
    } else
    {
        $j('.vat-toggle > div > ul > li:last-child').text('Show ex VAT prices');
    }
    $j('.vat-icon-show').toggleClass('vat-icon-hide');
    $j('.vat-yellow-icon-hide').toggleClass('vat-yellow-icon-show');
});

var vatstat = localStorage.getItem('vat_pricing');
if(vatstat == "inc"){
    $j('.show-vat').text('Show ex VAT prices');
    $j('.show-vat').removeClass('show-vat-active');
} else {
    $j('.show-vat').text('Show inc VAT prices');
    $j('.show-vat').removeClass('show-vat-active');
    $j('.show-vat').addClass('show-vat-active');
}

$j('.show-vat').click(function() {
    $j(this).toggleClass('show-vat-active');
    if ($j(this).hasClass('show-vat-active')) {
        $j(this).text('Show inc VAT prices');
    } else {
        $j(this).text('Show ex VAT prices');
    }
    $j('.vat-toggle').trigger('click');
});

if ($j('.burger-sub-menu.currency').attr('id') == 'USD') {
  $j('.burger-sub-menu.show-vat').css('display','none');
} else {
  $j('.burger-sub-menu.show-vat').css('display','block');
}

$j('.cus-ser-dropdown').on('hover', function() {
    $j('.ser-icon-show').toggleClass('ser-icon-hide');
    $j('.ser-yellow-icon-hide').toggleClass('ser-yellow-icon-show');
});

$j('.login-register-dropdown').on('hover', function() {
    $j('.log-icon-show').toggleClass('log-icon-hide');
    $j('.log-yellow-icon-hide').toggleClass('log-yellow-icon-show');
});

$j('.checkout-saved-add').click(function() {
    $j('.saved-add .checkout-saved-add').removeClass('saved-add-active');
    $j(this).addClass('saved-add-active');
});

$j('.check-bill-saved-add').click(function() {
    $j('.saved-add .check-bill-saved-add').removeClass('saved-add-active');
    $j(this).addClass('saved-add-active');
});

$j('#bill-new-add').click(function() {
    $j('#billing-new-address-form').removeAttr('style');
});

$j('#ship-new-add').click(function() {
    $j('#shipping-new-address-form').removeAttr('style');
});

$j('#bill-new-add').click(function() {
    $j('.saved-add .checkout-saved-add').removeClass('saved-add-active');
});

$j('#ship-new-add').click(function() {
    $j('.saved-add .check-bill-saved-add').removeClass('saved-add-active');
});

$j(document).ready(function($) {


    $j(document).ready(function($) {

    function serverTime() {
        var time = null;
        $j.ajax({url: '',
            async: false, dataType: 'text',
            success: function(text) {
                time = new Date();
            },
            error: function(http, message, exc) {
                time = new Date();
        }});
        return time;

    }

    function getCountDown() {
        var until = getNowEDT();

        until.setHours(16,0,0,0); // 4PM current day

        if(getNowEDT() >= until){
            until.setHours(40,0,0,0); // 4PM next day
        }



        return until;
    }

    function getNowEDT() {
           var now = new Date();
           now.setMinutes(now.getMinutes() + now.getTimezoneOffset() + 0 * 60); // EDT is UTC-4
        return now;
    }

    $j('#next-dispatch2').countdown({until: getCountDown(), compact: true, serverSync: serverTime, format: 'HMS', timezone: + 0,
        onExpiry: function() {
                $j(this).countdown('option', {until: getCountDown()});
        }});
    });

    // Another quick example of how to handle 24 hour countdown reset (without the timezone sync)


    // http://keith-wood.name/countdownRef.html
    // https://forum.$j.com/topic/countdown-plugin-how-to-restart-countdown-timer-every-24-hours

});

var day ='';
var now = '';
function getNowEDT() {
    now = new Date();
    day = String(now).split(' ')[0]; // defines day;
    now.setMinutes(now.getMinutes() + now.getTimezoneOffset() + 0 * 60); // EDT is UTC-4 ; this gives date in long form;
    return now;
}
function getCountDown() {
    var timeUp = getNowEDT();
    $j('#day').text(day);
    if(day=='Fri') {
        timeUp.setHours(16,0,0,0); // 4PM current day
        if(getNowEDT() >= timeUp){
            timeUp.setHours(88,0,0,0); // 4PM next day
        }
    }
    else if(day=='Sat') {
        timeUp.setHours(16,0,0,0); // 4PM current day
        if(getNowEDT() >= timeUp){
            timeUp.setHours(64,0,0,0); // 4PM next day
        }
    }
    else {
        now.setHours(16,0,0,0); // 4PM current day
        if(getNowEDT() >= timeUp){
            timeUp.setHours(40,0,0,0); // 4PM next day
        }
    }
    return timeUp;
}

$j('#next-dispatch').countdown({until: getCountDown(), compact: true, format: 'HMS', timezone: + 0,
    onExpiry: function() {
        $j(this).countdown('option', {until: getCountDown()});
    }
});


$j(document).ready(function($) {


    $j(document).ready(function($) {

    function serverTime() {
        var time = null;
        $j.ajax({url: '',
            async: false, dataType: 'text',
            success: function(text) {
                time = new Date();
            },
            error: function(http, message, exc) {
                time = new Date();
        }});
        return time;

    }

    function getCountDown() {
        var until = getNowEDT();

        until.setHours(16,0,0,0); // 4PM current day

        if(getNowEDT() >= until){
            until.setHours(40,0,0,0); // 4PM next day
        }



        return until;
    }

    function getNowEDT() {
           var now = new Date();
           now.setMinutes(now.getMinutes() + now.getTimezoneOffset() + 0 * 60); // EDT is UTC-4
        return now;
    }

    $j('#delivery-counter').countdown({until: getCountDown(), compact: true, serverSync: serverTime, format: 'HMS', timezone: + 0,
        onExpiry: function() {
                $j(this).countdown('option', {until: getCountDown()});
        }});
    });

    // Another quick example of how to handle 24 hour countdown reset (without the timezone sync)
    function doCountdown(){

        var todaysThree = new Date(), nextThree = new Date();
        todaysThree.setHours(15,0,0,0);
        if (todaysThree <= nextThree){ nextThree.setDate(nextThree.getDate()+1); }
        nextThree.setHours(15,0,0,0);

        $j('#delivery-counter').countdown({until: nextThree, compact: true,
            description: '',  onExpiry: function(){doCountdown()}});
    }

    // http://keith-wood.name/countdownRef.html
    // https://forum.$j.com/topic/countdown-plugin-how-to-restart-countdown-timer-every-24-hours

});

$j(document).ready(function($) {


    $j(document).ready(function($) {

    function serverTime() {
        var time = null;
        $j.ajax({url: '',
            async: false, dataType: 'text',
            success: function(text) {
                time = new Date();
            },
            error: function(http, message, exc) {
                time = new Date();
        }});
        return time;

    }

    function getCountDown() {
        var until = getNowEDT();

        until.setHours(16,0,0,0); // 4PM current day

        if(getNowEDT() >= until){
            until.setHours(40,0,0,0); // 4PM next day
        }



        return until;
    }

    function getNowEDT() {
           var now = new Date();
           now.setMinutes(now.getMinutes() + now.getTimezoneOffset() + 0 * 60); // EDT is UTC-4
        return now;
    }

    $j('#next-dispatch03').countdown({until: getCountDown(), compact: true, serverSync: serverTime, format: 'HM', timezone: + 0,
        onExpiry: function() {
                $j(this).countdown('option', {until: getCountDown()});
        }});
    });

    // Another quick example of how to handle 24 hour countdown reset (without the timezone sync)
    function doCountdown(){

        var todaysThree = new Date(), nextThree = new Date();
        todaysThree.setHours(15,0,0,0);
        if (todaysThree <= nextThree){ nextThree.setDate(nextThree.getDate()+1); }
        nextThree.setHours(15,0,0,0);

        $j('#next-dispatch03').countdown({until: nextThree, compact: true,
            description: '',  onExpiry: function(){doCountdown()}});
    }

    // http://keith-wood.name/countdownRef.html
    // https://forum.$j.com/topic/countdown-plugin-how-to-restart-countdown-timer-every-24-hours

});

    $j('a.pager-prev').click(function () {
        var current = slider.getCurrentSlide();
        slider.goToPrevSlide(current) - 1;
    });
    $j('a.pager-next').click(function () {
        var current = slider.getCurrentSlide();
        slider.goToNextSlide(current) + 1;
    });



$j('.listing-filters .accordian .filter-list, .open-wireless-cat').slideDown(200);

if ($j('.listing-filters .accordian .filter-list').children('div').hasClass('hAccord')) {
    $j('.hAccord').parent('.filter-list').prev('.round-checkbox').slideUp();
    $j('.hAccord').parent('.filter-list').slideDown();
} else {}



$j('.wireless-cat .round-checkbox label').click(function( event ) {
    event.stopPropagation();
});



$j(window).resize(function() {

});

$j('.back-btn, .close-btn, .checkout-btn, .add-cart-box .filled-close-btn, .add-cart-box .fill-btn').on('click', function() {
    $j('.add-cart-box').removeClass('opened-basket');
});

$j('.update-basket, .checkout-btn, .desktop-basket, .for-tablet .desktop-basket').on('click', function() {
    $j('.basket-number').addClass('basket-number-active');
});

$j('.read-full').on('click', function() {
    $j('html, body').animate({
        scrollTop: $j( $j(this).attr('href') ).offset().top
    }, 500);
    return false;
});



$j('.test-text .filled-close-btn').click(function() {
    $j('.test-website').css('display', 'none');
});

if ($j('body').width() >= 1151) {

        var slider = $j('.bxslider').bxSlider({
            mode: 'fade',
            pause: 6000,
            pagerCustom: '#bx-pager',
            controls: false,
            auto: true,
            infiniteLoop: true,
            onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                $j('.hide-banner-content').removeClass('show');
                $j('.hide-banner-button').removeClass('show');
                $j('.hide-banner-content').eq(currentSlideHtmlObject).addClass('show');
                $j('.hide-banner-button').eq(currentSlideHtmlObject).addClass('show');
            },
            onSliderLoad: function () {
                $j('.hide-banner-content').eq(0).addClass('show')
                $j('.hide-banner-button').eq(0).addClass('show')
            }
        });

        $j(window).resize(function() {
        });

        $j('.brand-slider').bxSlider({
            moveSlides: 6,
            pager: false,
            slideWidth: 195,
            minSlides: 6,
            maxSlides: 6
        });

        setTimeout(function() {
            var slider = $j('.single-product-slider').bxSlider({
                pagerCustom: '#product-pager',
                controls: false,
                auto: false,
                infiniteLoop: false,
                onSlideAfter: function(){
                    product();
                },
                onSliderLoad: function(){
                    $j('a.zoom').easyZoom();
                }
            });

            function product(){
                var total = slider.getCurrentSlide();
                $j('.single-product-slider li a.product').removeClass('zoom');
                $j('.single-product-slider li a.zoom'+total).addClass('zoom');
                setTimeout (function() {
                    $j('a.zoom').easyZoom();
                },100);
            }

            var sliderThumb = $j('#product-pager').bxSlider({
                prevText: '',
                nextText: '',
                controls: true,
                pager: false,
                slideWidth: 69,
                minSlides: 4,
                maxSlides: 4,
                slideMargin: 10,
                infiniteLoop: false,
                hideControlOnEnd: true
            });
        },6000);

        $j('.video-slider').bxSlider({
            slideWidth: 380,
            pager: false,
            minSlides: 1,
            maxSlides: 2,
            video: true
        });

        $j('.shipping-method-slider').bxSlider({
            adaptiveHeight: true
        });

        $j('#crosssell-products-list').bxSlider({
            controls: true,
            pager: false,
            slideWidth: 230,
            minSlides: 4,
            maxSlides: 4
        });

        $j(window).scroll(function() {
            var scroll = $j(window).scrollTop();
            var middleHeight = $j('.single-product').height();
            var midHeight = middleHeight - 221;
            if (scroll > 302 && scroll < midHeight) {
                $j(".add-cart-box").addClass('add-cart-box-scrolled');
            } else {
                $j(".add-cart-box").removeClass('add-cart-box-scrolled');
            }

            if (scroll > midHeight ) {
                $j('.single-product .product-page > ul:last-child > li:last-child').css('vertical-align','bottom');
                $j('.single-product .product-page > ul:last-child > li:last-child')
                .prepend($j('.add-cart-form'));
            } else {
                $j('.single-product .product-page > ul:first-child > li:last-child').css('vertical-align','top');
                $j('.single-product .product-page > ul:first-child > li:last-child')
                .prepend($j('.add-cart-form'));
            }
        });

} else if ($j('body').width() <= 1150 && $j('body').width() >= 768) {

    var slider = $j('.bxslider').bxSlider({
        mode: 'fade',
        pause: 6000,
        pagerCustom: '#bx-pager',
        controls: false,
        auto: true,
        infiniteLoop: true,
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            $j('.hide-banner-content').removeClass('show');
            $j('.hide-banner-button').removeClass('show');
            $j('.hide-banner-content').eq(currentSlideHtmlObject).addClass('show');
            $j('.hide-banner-button').eq(currentSlideHtmlObject).addClass('show');
        },
        onSliderLoad: function () {
            $j('.hide-banner-content').eq(0).addClass('show')
            $j('.hide-banner-button').eq(0).addClass('show')
        }
    });

    $j(window).resize(function() {
        slider.reloadSlider();
    });

    $j('.brand-slider').bxSlider({
        moveSlides: 3,
        pager: false,
        slideWidth: 160,
        minSlides: 4,
        maxSlides: 4
    });

    $j(window).scroll(function() {
        var scroll = $j(window).scrollTop();
        var total = $j('.listing').height();
        if (scroll >= total - 130) {
            $j(".floating-nav").hide();
        } else {
            $j(".floating-nav").show();
        }
    });

    $j('.sort-btn').on('click', function() {
        $j('.floating-sort').toggleClass('show-sort');
        $j('.listing-filters').removeClass('show-listing-filters');
    });

    $j('.filter-btn').on('click', function() {
        $j('.listing-filters').toggleClass('show-listing-filters');
        $j('.floating-sort').removeClass('show-sort');
    });

        setTimeout(function() {
        var slider = $j('.single-product-slider').bxSlider({
            pagerCustom: '#product-pager',
            controls: false,
            auto: false,
            infiniteLoop: false
        });

        $j('.single-product-slider li a.product').each(function() {
            $j(this).removeAttr('href');
        });

        var sliderThumb = $j('#product-pager').bxSlider({
            prevText: '',
            nextText: '',
            controls: true,
            pager: false,
            slideWidth: 69,
            minSlides: 4,
            maxSlides: 4,
            slideMargin: 10,
            infiniteLoop: false,
            hideControlOnEnd: true
        });
        },6000);


        $j('.video-slider').bxSlider({
            slideWidth: 350,
            pager: false,
            minSlides: 1,
            maxSlides: 2
        });

        $j(window).scroll(function() {
            var scroll = $j(window).scrollTop();
            var middleHeight = $j('.single-product').height();
            var midHeight = middleHeight - 221;
            if (scroll > 302 && scroll < midHeight) {

            } else {

            }
        });

} else if ($j('body').width() <= 767 && $j('body').width() >= 600) {

        $j(function() {
            $j('#help-center').on('change', function() {
                var url = $j(this).val();
                if (url) {
                    window.location = url;
                }
                return false;
            });
        });

        var slider = $j('.bxslider').bxSlider({
            mode: 'fade',
            pause: 6000,
            pagerCustom: '#bx-pager',
            controls: false,
            auto: true,
            infiniteLoop: true,
            onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                $j('.hide-banner-content').removeClass('show');
                $j('.hide-banner-button').removeClass('show');
                $j('.hide-banner-content').eq(currentSlideHtmlObject).addClass('show');
                $j('.hide-banner-button').eq(currentSlideHtmlObject).addClass('show');
            },
            onSliderLoad: function () {
                $j('.hide-banner-content').eq(0).addClass('show')
                $j('.hide-banner-button').eq(0).addClass('show')
            }
        });

        $j(window).resize(function() {
            slider.reloadSlider();
        });

        $j('.brand-slider').bxSlider({
            moveSlides: 3,
            pager: false,
            slideWidth: 50,
            minSlides: 7,
            maxSlides: 7
        });

        $j(window).scroll(function() {
            var scroll = $j(window).scrollTop();
            var total = $j('.listing').height();
            if (scroll >= total - 130) {
                $j(".floating-nav").hide();
            } else {
                $j(".floating-nav").show();
            }
        });

        $j('.sort-btn').on('click', function() {
            $j('.floating-sort').toggleClass('show-sort');
            $j('.listing-filters').removeClass('show-listing-filters');
        });

        $j('.filter-btn').on('click', function() {
            $j('.listing-filters').toggleClass('show-listing-filters');
            $j('.floating-sort').removeClass('show-sort');
        });

        $j('footer a, .burger-menu span, .burger-menu a').filter(':nth-child(n+1)').addClass('hide');

        $j('.login a').removeClass('hide');

        $j('footer h5').click(function () {
            $j(this).parent().siblings().children('a').removeClass('show');
            $j(this).siblings('a').toggleClass('show');
            $j(this).parent().siblings().children().removeClass('active-footer-tab');;
            $j(this).toggleClass('active-footer-tab');
        });

        setTimeout(function() {
        var slider = $j('.single-product-slider').bxSlider({
            pagerCustom: '#product-pager',
            controls: false,
            auto: false,
            infiniteLoop: false,
            slideWidth: 445
        });

        $j('.single-product-slider li a.product').each(function() {
            $j(this).removeAttr('href');
        });

        var sliderThumb = $j('#product-pager').bxSlider({
            prevText: '',
            nextText: '',
            controls: true,
            pager: false,
            slideWidth: 68,
            minSlides: 4,
            maxSlides: 4,
            slideMargin: 10,
            infiniteLoop: false,
            hideControlOnEnd: true
        });
        },6000);


        $j('.video-slider').bxSlider({
            slideWidth: 380,
            pager: false,
            minSlides: 1,
            maxSlides: 1
        });

        (function () {
    'use strict';
    var definePinchZoom = function ($) {

        /**
         * Pinch zoom using jQuery
         * @version 0.0.2
         * @author Manuel Stofer <mst@rtp.ch>
         * @param el
         * @param options
         * @constructor
         */
        var PinchZoom = function (el, options) {
                this.el = $(el);
                this.zoomFactor = 1;
                this.lastScale = 1;
                this.offset = {
                    x: 0,
                    y: 0
                };
                this.options = $.extend({}, this.defaults, options);
                this.setupMarkup();
                this.bindEvents();
                this.update();

            },
            sum = function (a, b) {
                return a + b;
            },
            isCloseTo = function (value, expected) {
                return value > expected - 0.01 && value < expected + 0.01;
            };

        PinchZoom.prototype = {

            defaults: {
                tapZoomFactor: 2,
                zoomOutFactor: 1.3,
                animationDuration: 300,
                animationInterval: 5,
                maxZoom: 4,
                minZoom: 0.5,
                use2d: true
            },

            /**
             * Event handler for 'dragstart'
             * @param event
             */
            handleDragStart: function (event) {
                this.stopAnimation();
                this.lastDragPosition = false;
                this.hasInteraction = true;
                this.handleDrag(event);
            },

            /**
             * Event handler for 'drag'
             * @param event
             */
            handleDrag: function (event) {

                if (this.zoomFactor > 1.0) {
                    var touch = this.getTouches(event)[0];
                    this.drag(touch, this.lastDragPosition);
                    this.offset = this.sanitizeOffset(this.offset);
                    this.lastDragPosition = touch;
                }
            },

            handleDragEnd: function () {
                this.end();
            },

            /**
             * Event handler for 'zoomstart'
             * @param event
             */
            handleZoomStart: function (event) {
                this.stopAnimation();
                this.lastScale = 1;
                this.nthZoom = 0;
                this.lastZoomCenter = false;
                this.hasInteraction = true;
            },

            /**
             * Event handler for 'zoom'
             * @param event
             */
            handleZoom: function (event, newScale) {

                // a relative scale factor is used
                var touchCenter = this.getTouchCenter(this.getTouches(event)),
                    scale = newScale / this.lastScale;
                this.lastScale = newScale;

                // the first touch events are thrown away since they are not precise
                this.nthZoom += 1;
                if (this.nthZoom > 3) {

                    this.scale(scale, touchCenter);
                    this.drag(touchCenter, this.lastZoomCenter);
                }
                this.lastZoomCenter = touchCenter;
            },

            handleZoomEnd: function () {
                this.end();
            },

            /**
             * Event handler for 'doubletap'
             * @param event
             */
            handleDoubleTap: function (event) {
                var center = this.getTouches(event)[0],
                    zoomFactor = this.zoomFactor > 1 ? 1 : this.options.tapZoomFactor,
                    startZoomFactor = this.zoomFactor,
                    updateProgress = (function (progress) {
                        this.scaleTo(startZoomFactor + progress * (zoomFactor - startZoomFactor), center);
                    }).bind(this);

                if (this.hasInteraction) {
                    return;
                }
                if (startZoomFactor > zoomFactor) {
                    center = this.getCurrentZoomCenter();
                }

                this.animate(this.options.animationDuration, this.options.animationInterval, updateProgress, this.swing);
            },

            /**
             * Max / min values for the offset
             * @param offset
             * @return {Object} the sanitized offset
             */
            sanitizeOffset: function (offset) {
                var maxX = (this.zoomFactor - 1) * this.getContainerX(),
                    maxY = (this.zoomFactor - 1) * this.getContainerY(),
                    maxOffsetX = Math.max(maxX, 0),
                    maxOffsetY = Math.max(maxY, 0),
                    minOffsetX = Math.min(maxX, 0),
                    minOffsetY = Math.min(maxY, 0);

                return {
                    x: Math.min(Math.max(offset.x, minOffsetX), maxOffsetX),
                    y: Math.min(Math.max(offset.y, minOffsetY), maxOffsetY)
                };
            },

            /**
             * Scale to a specific zoom factor (not relative)
             * @param zoomFactor
             * @param center
             */
            scaleTo: function (zoomFactor, center) {
                this.scale(zoomFactor / this.zoomFactor, center);
            },

            /**
             * Scales the element from specified center
             * @param scale
             * @param center
             */
            scale: function (scale, center) {
                scale = this.scaleZoomFactor(scale);
                this.addOffset({
                    x: (scale - 1) * (center.x + this.offset.x),
                    y: (scale - 1) * (center.y + this.offset.y)
                });
            },

            /**
             * Scales the zoom factor relative to current state
             * @param scale
             * @return the actual scale (can differ because of max min zoom factor)
             */
            scaleZoomFactor: function (scale) {
                var originalZoomFactor = this.zoomFactor;
                this.zoomFactor *= scale;
                this.zoomFactor = Math.min(this.options.maxZoom, Math.max(this.zoomFactor, this.options.minZoom));
                return this.zoomFactor / originalZoomFactor;
            },

            /**
             * Drags the element
             * @param center
             * @param lastCenter
             */
            drag: function (center, lastCenter) {
                if (lastCenter) {
                    this.addOffset({
                        x: -(center.x - lastCenter.x),
                        y: -(center.y - lastCenter.y)
                    });
                }
            },

            /**
             * Calculates the touch center of multiple touches
             * @param touches
             * @return {Object}
             */
            getTouchCenter: function (touches) {
                return this.getVectorAvg(touches);
            },

            /**
             * Calculates the average of multiple vectors (x, y values)
             */
            getVectorAvg: function (vectors) {
                return {
                    x: vectors.map(function (v) { return v.x; }).reduce(sum) / vectors.length,
                    y: vectors.map(function (v) { return v.y; }).reduce(sum) / vectors.length
                };
            },

            /**
             * Adds an offset
             * @param offset the offset to add
             * @return return true when the offset change was accepted
             */
            addOffset: function (offset) {
                this.offset = {
                    x: this.offset.x + offset.x,
                    y: this.offset.y + offset.y
                };
            },

            sanitize: function () {
                if (this.zoomFactor < this.options.zoomOutFactor) {
                    this.zoomOutAnimation();
                } else if (this.isInsaneOffset(this.offset)) {
                    this.sanitizeOffsetAnimation();
                }
            },

            /**
             * Checks if the offset is ok with the current zoom factor
             * @param offset
             * @return {Boolean}
             */
            isInsaneOffset: function (offset) {
                var sanitizedOffset = this.sanitizeOffset(offset);
                return sanitizedOffset.x !== offset.x ||
                    sanitizedOffset.y !== offset.y;
            },

            /**
             * Creates an animation moving to a sane offset
             */
            sanitizeOffsetAnimation: function () {
                var targetOffset = this.sanitizeOffset(this.offset),
                    startOffset = {
                        x: this.offset.x,
                        y: this.offset.y
                    },
                    updateProgress = (function (progress) {
                        this.offset.x = startOffset.x + progress * (targetOffset.x - startOffset.x);
                        this.offset.y = startOffset.y + progress * (targetOffset.y - startOffset.y);
                        this.update();
                    }).bind(this);

                this.animate(
                    this.options.animationDuration,
                    this.options.animationInterval,
                    updateProgress,
                    this.swing
                );
            },

            /**
             * Zooms back to the original position,
             * (no offset and zoom factor 1)
             */
            zoomOutAnimation: function () {
                var startZoomFactor = this.zoomFactor,
                    zoomFactor = 1,
                    center = this.getCurrentZoomCenter(),
                    updateProgress = (function (progress) {
                        this.scaleTo(startZoomFactor + progress * (zoomFactor - startZoomFactor), center);
                    }).bind(this);

                this.animate(
                    this.options.animationDuration,
                    this.options.animationInterval,
                    updateProgress,
                    this.swing
                );
            },

            /**
             * Updates the aspect ratio
             */
            updateAspectRatio: function () {
                this.setContainerY(this.getContainerX() / this.getAspectRatio());
            },

            /**
             * Calculates the initial zoom factor (for the element to fit into the container)
             * @return the initial zoom factor
             */
            getInitialZoomFactor: function () {
                return this.container.width() / this.el.width();
            },

            /**
             * Calculates the aspect ratio of the element
             * @return the aspect ratio
             */
            getAspectRatio: function () {
                return this.el.width() / this.el.height();
            },

            /**
             * Calculates the virtual zoom center for the current offset and zoom factor
             * (used for reverse zoom)
             * @return {Object} the current zoom center
             */
            getCurrentZoomCenter: function () {

                // uses following formula to calculate the zoom center x value
                // offset_left / offset_right = zoomcenter_x / (container_x - zoomcenter_x)
                var length = this.container.width() * this.zoomFactor,
                    offsetLeft  = this.offset.x,
                    offsetRight = length - offsetLeft - this.container.width(),
                    widthOffsetRatio = offsetLeft / offsetRight,
                    centerX = widthOffsetRatio * this.container.width() / (widthOffsetRatio + 1),

                // the same for the zoomcenter y
                    height = this.container.height() * this.zoomFactor,
                    offsetTop  = this.offset.y,
                    offsetBottom = height - offsetTop - this.container.height(),
                    heightOffsetRatio = offsetTop / offsetBottom,
                    centerY = heightOffsetRatio * this.container.height() / (heightOffsetRatio + 1);

                // prevents division by zero
                if (offsetRight === 0) { centerX = this.container.width(); }
                if (offsetBottom === 0) { centerY = this.container.height(); }

                return {
                    x: centerX,
                    y: centerY
                };
            },

            canDrag: function () {
                return !isCloseTo(this.zoomFactor, 1);
            },

            /**
             * Returns the touches of an event relative to the container offset
             * @param event
             * @return array touches
             */
            getTouches: function (event) {
                var position = this.container.offset();
                return Array.prototype.slice.call(event.touches).map(function (touch) {
                    return {
                        x: touch.pageX - position.left,
                        y: touch.pageY - position.top
                    };
                });
            },

            /**
             * Animation loop
             * does not support simultaneous animations
             * @param duration
             * @param interval
             * @param framefn
             * @param timefn
             * @param callback
             */
            animate: function (duration, interval, framefn, timefn, callback) {
                var startTime = new Date().getTime(),
                    renderFrame = (function () {
                        if (!this.inAnimation) { return; }
                        var frameTime = new Date().getTime() - startTime,
                            progress = frameTime / duration;
                        if (frameTime >= duration) {
                            framefn(1);
                            if (callback) {
                                callback();
                            }
                            this.update();
                            this.stopAnimation();
                            this.update();
                        } else {
                            if (timefn) {
                                progress = timefn(progress);
                            }
                            framefn(progress);
                            this.update();
                            setTimeout(renderFrame, interval);
                        }
                    }).bind(this);
                this.inAnimation = true;
                renderFrame();
            },

            /**
             * Stops the animation
             */
            stopAnimation: function () {
                this.inAnimation = false;
            },

            /**
             * Swing timing function for animations
             * @param p
             * @return {Number}
             */
            swing: function (p) {
                return -Math.cos(p * Math.PI) / 2  + 0.5;
            },

            getContainerX: function () {
                return this.container.width();
            },

            getContainerY: function () {
                return this.container.height();
            },

            setContainerY: function (y) {
                return this.container.height(y);
            },

            /**
             * Creates the expected html structure
             */
            setupMarkup: function () {
                this.container = $('<div class="pinch-zoom-container"></div>');
                this.el.before(this.container);
                this.container.append(this.el);

                this.container.css({
                    'overflow': 'hidden',
                    'position': 'relative'
                });

                this.el.css({
                    'webkitTransformOrigin': '0% 0%',
                    'mozTransformOrigin': '0% 0%',
                    'msTransformOrigin': '0% 0%',
                    'oTransformOrigin': '0% 0%',
                    'transformOrigin': '0% 0%',
                    'position': 'absolute'
                });
            },

            end: function () {
                this.hasInteraction = false;
                this.sanitize();
                this.update();
            },

            /**
             * Binds all required event listeners
             */
            bindEvents: function () {
                detectGestures(this.container.get(0), this);
                $(window).bind('resize', this.update.bind(this));
                $(this.el).find('img').bind('load', this.update.bind(this));
            },

            /**
             * Updates the css values according to the current zoom factor and offset
             */
            update: function () {

                if (this.updatePlaned) {
                    return;
                }
                this.updatePlaned = true;

                setTimeout((function () {
                    this.updatePlaned = false;
                    this.updateAspectRatio();

                    var zoomFactor = this.getInitialZoomFactor() * this.zoomFactor,
                        offsetX = -this.offset.x / zoomFactor,
                        offsetY = -this.offset.y / zoomFactor,
                        transform3d =   'scale3d('     + zoomFactor + ', '  + zoomFactor + ',1) ' +
                            'translate3d(' + offsetX    + 'px,' + offsetY    + 'px,0px)',
                        transform2d =   'scale('       + zoomFactor + ', '  + zoomFactor + ') ' +
                            'translate('   + offsetX    + 'px,' + offsetY    + 'px)',
                        removeClone = (function () {
                            if (this.clone) {
                                this.clone.remove();
                                delete this.clone;
                            }
                        }).bind(this);

                    // Scale 3d and translate3d are faster (at least on ios)
                    // but they also reduce the quality.
                    // PinchZoom uses the 3d transformations during interactions
                    // after interactions it falls back to 2d transformations
                    if (!this.options.use2d || this.hasInteraction || this.inAnimation) {
                        this.is3d = true;
                        removeClone();
                        this.el.css({
                            'webkitTransform':  transform3d,
                            'oTransform':       transform2d,
                            'msTransform':      transform2d,
                            'mozTransform':     transform2d,
                            'transform':        transform3d
                        });
                    } else {

                        // When changing from 3d to 2d transform webkit has some glitches.
                        // To avoid this, a copy of the 3d transformed element is displayed in the
                        // foreground while the element is converted from 3d to 2d transform
                        if (this.is3d) {
                            this.clone = this.el.clone();
                            this.clone.css('pointer-events', 'none');
                            this.clone.appendTo(this.container);
                            setTimeout(removeClone, 200);
                        }
                        this.el.css({
                            'webkitTransform':  transform2d,
                            'oTransform':       transform2d,
                            'msTransform':      transform2d,
                            'mozTransform':     transform2d,
                            'transform':        transform2d
                        });
                        this.is3d = false;
                    }
                }).bind(this), 0);
            }
        };

        var detectGestures = function (el, target) {
            var interaction = null,
                fingers = 0,
                lastTouchStart = null,
                startTouches = null,

                setInteraction = function (newInteraction, event) {
                    if (interaction !== newInteraction) {

                        if (interaction && !newInteraction) {
                            switch (interaction) {
                                case "zoom":
                                    target.handleZoomEnd(event);
                                    break;
                                case 'drag':
                                    target.handleDragEnd(event);
                                    break;
                            }
                        }

                        switch (newInteraction) {
                            case 'zoom':
                                target.handleZoomStart(event);
                                break;
                            case 'drag':
                                target.handleDragStart(event);
                                break;
                        }
                    }
                    interaction = newInteraction;
                },

                updateInteraction = function (event) {
                    if (fingers === 2) {
                        setInteraction('zoom');
                    } else if (fingers === 1 && target.canDrag()) {
                        setInteraction('drag', event);
                    } else {
                        setInteraction(null, event);
                    }
                },

                targetTouches = function (touches) {
                    return Array.prototype.slice.call(touches).map(function (touch) {
                        return {
                            x: touch.pageX,
                            y: touch.pageY
                        };
                    });
                },

                getDistance = function (a, b) {
                    var x, y;
                    x = a.x - b.x;
                    y = a.y - b.y;
                    return Math.sqrt(x * x + y * y);
                },

                calculateScale = function (startTouches, endTouches) {
                    var startDistance = getDistance(startTouches[0], startTouches[1]),
                        endDistance = getDistance(endTouches[0], endTouches[1]);
                    return endDistance / startDistance;
                },

                cancelEvent = function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                },

                detectDoubleTap = function (event) {
                    var time = (new Date()).getTime();

                    if (fingers > 1) {
                        lastTouchStart = null;
                    }

                    if (time - lastTouchStart < 300) {
                        cancelEvent(event);

                        target.handleDoubleTap(event);
                        switch (interaction) {
                            case "zoom":
                                target.handleZoomEnd(event);
                                break;
                            case 'drag':
                                target.handleDragEnd(event);
                                break;
                        }
                    }

                    if (fingers === 1) {
                        lastTouchStart = time;
                    }
                },
                firstMove = true;

            el.addEventListener('touchstart', function (event) {
                firstMove = true;
                fingers = event.touches.length;
                detectDoubleTap(event);
            });

            el.addEventListener('touchmove', function (event) {

                if (firstMove) {
                    updateInteraction(event);
                    if (interaction) {
                        cancelEvent(event);
                    }
                    startTouches = targetTouches(event.touches);
                } else {
                    switch (interaction) {
                        case 'zoom':
                            target.handleZoom(event, calculateScale(startTouches, targetTouches(event.touches)));
                            break;
                        case 'drag':
                            target.handleDrag(event);
                            break;
                    }
                    if (interaction) {
                        cancelEvent(event);
                        target.update();
                    }
                }

                firstMove = false;
            });

            el.addEventListener('touchend', function (event) {
                fingers = event.touches.length;
                updateInteraction(event);
            });
        };

        return PinchZoom;
    };

    if (typeof define !== 'undefined' && define.amd) {
        define(['jquery'], function ($) {
            return definePinchZoom($);
        });
    } else {
        window.RTP = window.RTP || {};
        window.RTP.PinchZoom = definePinchZoom(jQuery);
    }
}).call(this);

} else {

    (function () {
    'use strict';
    var definePinchZoom = function ($) {

        /**
         * Pinch zoom using jQuery
         * @version 0.0.2
         * @author Manuel Stofer <mst@rtp.ch>
         * @param el
         * @param options
         * @constructor
         */
        var PinchZoom = function (el, options) {
                this.el = $(el);
                this.zoomFactor = 1;
                this.lastScale = 1;
                this.offset = {
                    x: 0,
                    y: 0
                };
                this.options = $.extend({}, this.defaults, options);
                this.setupMarkup();
                this.bindEvents();
                this.update();

            },
            sum = function (a, b) {
                return a + b;
            },
            isCloseTo = function (value, expected) {
                return value > expected - 0.01 && value < expected + 0.01;
            };

        PinchZoom.prototype = {

            defaults: {
                tapZoomFactor: 2,
                zoomOutFactor: 1.3,
                animationDuration: 300,
                animationInterval: 5,
                maxZoom: 4,
                minZoom: 0.5,
                use2d: true
            },

            /**
             * Event handler for 'dragstart'
             * @param event
             */
            handleDragStart: function (event) {
                this.stopAnimation();
                this.lastDragPosition = false;
                this.hasInteraction = true;
                this.handleDrag(event);
            },

            /**
             * Event handler for 'drag'
             * @param event
             */
            handleDrag: function (event) {

                if (this.zoomFactor > 1.0) {
                    var touch = this.getTouches(event)[0];
                    this.drag(touch, this.lastDragPosition);
                    this.offset = this.sanitizeOffset(this.offset);
                    this.lastDragPosition = touch;
                }
            },

            handleDragEnd: function () {
                this.end();
            },

            /**
             * Event handler for 'zoomstart'
             * @param event
             */
            handleZoomStart: function (event) {
                this.stopAnimation();
                this.lastScale = 1;
                this.nthZoom = 0;
                this.lastZoomCenter = false;
                this.hasInteraction = true;
            },

            /**
             * Event handler for 'zoom'
             * @param event
             */
            handleZoom: function (event, newScale) {

                // a relative scale factor is used
                var touchCenter = this.getTouchCenter(this.getTouches(event)),
                    scale = newScale / this.lastScale;
                this.lastScale = newScale;

                // the first touch events are thrown away since they are not precise
                this.nthZoom += 1;
                if (this.nthZoom > 3) {

                    this.scale(scale, touchCenter);
                    this.drag(touchCenter, this.lastZoomCenter);
                }
                this.lastZoomCenter = touchCenter;
            },

            handleZoomEnd: function () {
                this.end();
            },

            /**
             * Event handler for 'doubletap'
             * @param event
             */
            handleDoubleTap: function (event) {
                var center = this.getTouches(event)[0],
                    zoomFactor = this.zoomFactor > 1 ? 1 : this.options.tapZoomFactor,
                    startZoomFactor = this.zoomFactor,
                    updateProgress = (function (progress) {
                        this.scaleTo(startZoomFactor + progress * (zoomFactor - startZoomFactor), center);
                    }).bind(this);

                if (this.hasInteraction) {
                    return;
                }
                if (startZoomFactor > zoomFactor) {
                    center = this.getCurrentZoomCenter();
                }

                this.animate(this.options.animationDuration, this.options.animationInterval, updateProgress, this.swing);
            },

            /**
             * Max / min values for the offset
             * @param offset
             * @return {Object} the sanitized offset
             */
            sanitizeOffset: function (offset) {
                var maxX = (this.zoomFactor - 1) * this.getContainerX(),
                    maxY = (this.zoomFactor - 1) * this.getContainerY(),
                    maxOffsetX = Math.max(maxX, 0),
                    maxOffsetY = Math.max(maxY, 0),
                    minOffsetX = Math.min(maxX, 0),
                    minOffsetY = Math.min(maxY, 0);

                return {
                    x: Math.min(Math.max(offset.x, minOffsetX), maxOffsetX),
                    y: Math.min(Math.max(offset.y, minOffsetY), maxOffsetY)
                };
            },

            /**
             * Scale to a specific zoom factor (not relative)
             * @param zoomFactor
             * @param center
             */
            scaleTo: function (zoomFactor, center) {
                this.scale(zoomFactor / this.zoomFactor, center);
            },

            /**
             * Scales the element from specified center
             * @param scale
             * @param center
             */
            scale: function (scale, center) {
                scale = this.scaleZoomFactor(scale);
                this.addOffset({
                    x: (scale - 1) * (center.x + this.offset.x),
                    y: (scale - 1) * (center.y + this.offset.y)
                });
            },

            /**
             * Scales the zoom factor relative to current state
             * @param scale
             * @return the actual scale (can differ because of max min zoom factor)
             */
            scaleZoomFactor: function (scale) {
                var originalZoomFactor = this.zoomFactor;
                this.zoomFactor *= scale;
                this.zoomFactor = Math.min(this.options.maxZoom, Math.max(this.zoomFactor, this.options.minZoom));
                return this.zoomFactor / originalZoomFactor;
            },

            /**
             * Drags the element
             * @param center
             * @param lastCenter
             */
            drag: function (center, lastCenter) {
                if (lastCenter) {
                    this.addOffset({
                        x: -(center.x - lastCenter.x),
                        y: -(center.y - lastCenter.y)
                    });
                }
            },

            /**
             * Calculates the touch center of multiple touches
             * @param touches
             * @return {Object}
             */
            getTouchCenter: function (touches) {
                return this.getVectorAvg(touches);
            },

            /**
             * Calculates the average of multiple vectors (x, y values)
             */
            getVectorAvg: function (vectors) {
                return {
                    x: vectors.map(function (v) { return v.x; }).reduce(sum) / vectors.length,
                    y: vectors.map(function (v) { return v.y; }).reduce(sum) / vectors.length
                };
            },

            /**
             * Adds an offset
             * @param offset the offset to add
             * @return return true when the offset change was accepted
             */
            addOffset: function (offset) {
                this.offset = {
                    x: this.offset.x + offset.x,
                    y: this.offset.y + offset.y
                };
            },

            sanitize: function () {
                if (this.zoomFactor < this.options.zoomOutFactor) {
                    this.zoomOutAnimation();
                } else if (this.isInsaneOffset(this.offset)) {
                    this.sanitizeOffsetAnimation();
                }
            },

            /**
             * Checks if the offset is ok with the current zoom factor
             * @param offset
             * @return {Boolean}
             */
            isInsaneOffset: function (offset) {
                var sanitizedOffset = this.sanitizeOffset(offset);
                return sanitizedOffset.x !== offset.x ||
                    sanitizedOffset.y !== offset.y;
            },

            /**
             * Creates an animation moving to a sane offset
             */
            sanitizeOffsetAnimation: function () {
                var targetOffset = this.sanitizeOffset(this.offset),
                    startOffset = {
                        x: this.offset.x,
                        y: this.offset.y
                    },
                    updateProgress = (function (progress) {
                        this.offset.x = startOffset.x + progress * (targetOffset.x - startOffset.x);
                        this.offset.y = startOffset.y + progress * (targetOffset.y - startOffset.y);
                        this.update();
                    }).bind(this);

                this.animate(
                    this.options.animationDuration,
                    this.options.animationInterval,
                    updateProgress,
                    this.swing
                );
            },

            /**
             * Zooms back to the original position,
             * (no offset and zoom factor 1)
             */
            zoomOutAnimation: function () {
                var startZoomFactor = this.zoomFactor,
                    zoomFactor = 1,
                    center = this.getCurrentZoomCenter(),
                    updateProgress = (function (progress) {
                        this.scaleTo(startZoomFactor + progress * (zoomFactor - startZoomFactor), center);
                    }).bind(this);

                this.animate(
                    this.options.animationDuration,
                    this.options.animationInterval,
                    updateProgress,
                    this.swing
                );
            },

            /**
             * Updates the aspect ratio
             */
            updateAspectRatio: function () {
                this.setContainerY(this.getContainerX() / this.getAspectRatio());
            },

            /**
             * Calculates the initial zoom factor (for the element to fit into the container)
             * @return the initial zoom factor
             */
            getInitialZoomFactor: function () {
                return this.container.width() / this.el.width();
            },

            /**
             * Calculates the aspect ratio of the element
             * @return the aspect ratio
             */
            getAspectRatio: function () {
                return this.el.width() / this.el.height();
            },

            /**
             * Calculates the virtual zoom center for the current offset and zoom factor
             * (used for reverse zoom)
             * @return {Object} the current zoom center
             */
            getCurrentZoomCenter: function () {

                // uses following formula to calculate the zoom center x value
                // offset_left / offset_right = zoomcenter_x / (container_x - zoomcenter_x)
                var length = this.container.width() * this.zoomFactor,
                    offsetLeft  = this.offset.x,
                    offsetRight = length - offsetLeft - this.container.width(),
                    widthOffsetRatio = offsetLeft / offsetRight,
                    centerX = widthOffsetRatio * this.container.width() / (widthOffsetRatio + 1),

                // the same for the zoomcenter y
                    height = this.container.height() * this.zoomFactor,
                    offsetTop  = this.offset.y,
                    offsetBottom = height - offsetTop - this.container.height(),
                    heightOffsetRatio = offsetTop / offsetBottom,
                    centerY = heightOffsetRatio * this.container.height() / (heightOffsetRatio + 1);

                // prevents division by zero
                if (offsetRight === 0) { centerX = this.container.width(); }
                if (offsetBottom === 0) { centerY = this.container.height(); }

                return {
                    x: centerX,
                    y: centerY
                };
            },

            canDrag: function () {
                return !isCloseTo(this.zoomFactor, 1);
            },

            /**
             * Returns the touches of an event relative to the container offset
             * @param event
             * @return array touches
             */
            getTouches: function (event) {
                var position = this.container.offset();
                return Array.prototype.slice.call(event.touches).map(function (touch) {
                    return {
                        x: touch.pageX - position.left,
                        y: touch.pageY - position.top
                    };
                });
            },

            /**
             * Animation loop
             * does not support simultaneous animations
             * @param duration
             * @param interval
             * @param framefn
             * @param timefn
             * @param callback
             */
            animate: function (duration, interval, framefn, timefn, callback) {
                var startTime = new Date().getTime(),
                    renderFrame = (function () {
                        if (!this.inAnimation) { return; }
                        var frameTime = new Date().getTime() - startTime,
                            progress = frameTime / duration;
                        if (frameTime >= duration) {
                            framefn(1);
                            if (callback) {
                                callback();
                            }
                            this.update();
                            this.stopAnimation();
                            this.update();
                        } else {
                            if (timefn) {
                                progress = timefn(progress);
                            }
                            framefn(progress);
                            this.update();
                            setTimeout(renderFrame, interval);
                        }
                    }).bind(this);
                this.inAnimation = true;
                renderFrame();
            },

            /**
             * Stops the animation
             */
            stopAnimation: function () {
                this.inAnimation = false;
            },

            /**
             * Swing timing function for animations
             * @param p
             * @return {Number}
             */
            swing: function (p) {
                return -Math.cos(p * Math.PI) / 2  + 0.5;
            },

            getContainerX: function () {
                return this.container.width();
            },

            getContainerY: function () {
                return this.container.height();
            },

            setContainerY: function (y) {
                return this.container.height(y);
            },

            /**
             * Creates the expected html structure
             */
            setupMarkup: function () {
                this.container = $('<div class="pinch-zoom-container"></div>');
                this.el.before(this.container);
                this.container.append(this.el);

                this.container.css({
                    'overflow': 'hidden',
                    'position': 'relative'
                });

                this.el.css({
                    'webkitTransformOrigin': '0% 0%',
                    'mozTransformOrigin': '0% 0%',
                    'msTransformOrigin': '0% 0%',
                    'oTransformOrigin': '0% 0%',
                    'transformOrigin': '0% 0%',
                    'position': 'absolute'
                });
            },

            end: function () {
                this.hasInteraction = false;
                this.sanitize();
                this.update();
            },

            /**
             * Binds all required event listeners
             */
            bindEvents: function () {
                detectGestures(this.container.get(0), this);
                $(window).bind('resize', this.update.bind(this));
                $(this.el).find('img').bind('load', this.update.bind(this));
            },

            /**
             * Updates the css values according to the current zoom factor and offset
             */
            update: function () {

                if (this.updatePlaned) {
                    return;
                }
                this.updatePlaned = true;

                setTimeout((function () {
                    this.updatePlaned = false;
                    this.updateAspectRatio();

                    var zoomFactor = this.getInitialZoomFactor() * this.zoomFactor,
                        offsetX = -this.offset.x / zoomFactor,
                        offsetY = -this.offset.y / zoomFactor,
                        transform3d =   'scale3d('     + zoomFactor + ', '  + zoomFactor + ',1) ' +
                            'translate3d(' + offsetX    + 'px,' + offsetY    + 'px,0px)',
                        transform2d =   'scale('       + zoomFactor + ', '  + zoomFactor + ') ' +
                            'translate('   + offsetX    + 'px,' + offsetY    + 'px)',
                        removeClone = (function () {
                            if (this.clone) {
                                this.clone.remove();
                                delete this.clone;
                            }
                        }).bind(this);

                    // Scale 3d and translate3d are faster (at least on ios)
                    // but they also reduce the quality.
                    // PinchZoom uses the 3d transformations during interactions
                    // after interactions it falls back to 2d transformations
                    if (!this.options.use2d || this.hasInteraction || this.inAnimation) {
                        this.is3d = true;
                        removeClone();
                        this.el.css({
                            'webkitTransform':  transform3d,
                            'oTransform':       transform2d,
                            'msTransform':      transform2d,
                            'mozTransform':     transform2d,
                            'transform':        transform3d
                        });
                    } else {

                        // When changing from 3d to 2d transform webkit has some glitches.
                        // To avoid this, a copy of the 3d transformed element is displayed in the
                        // foreground while the element is converted from 3d to 2d transform
                        if (this.is3d) {
                            this.clone = this.el.clone();
                            this.clone.css('pointer-events', 'none');
                            this.clone.appendTo(this.container);
                            setTimeout(removeClone, 200);
                        }
                        this.el.css({
                            'webkitTransform':  transform2d,
                            'oTransform':       transform2d,
                            'msTransform':      transform2d,
                            'mozTransform':     transform2d,
                            'transform':        transform2d
                        });
                        this.is3d = false;
                    }
                }).bind(this), 0);
            }
        };

        var detectGestures = function (el, target) {
            var interaction = null,
                fingers = 0,
                lastTouchStart = null,
                startTouches = null,

                setInteraction = function (newInteraction, event) {
                    if (interaction !== newInteraction) {

                        if (interaction && !newInteraction) {
                            switch (interaction) {
                                case "zoom":
                                    target.handleZoomEnd(event);
                                    break;
                                case 'drag':
                                    target.handleDragEnd(event);
                                    break;
                            }
                        }

                        switch (newInteraction) {
                            case 'zoom':
                                target.handleZoomStart(event);
                                break;
                            case 'drag':
                                target.handleDragStart(event);
                                break;
                        }
                    }
                    interaction = newInteraction;
                },

                updateInteraction = function (event) {
                    if (fingers === 2) {
                        setInteraction('zoom');
                    } else if (fingers === 1 && target.canDrag()) {
                        setInteraction('drag', event);
                    } else {
                        setInteraction(null, event);
                    }
                },

                targetTouches = function (touches) {
                    return Array.prototype.slice.call(touches).map(function (touch) {
                        return {
                            x: touch.pageX,
                            y: touch.pageY
                        };
                    });
                },

                getDistance = function (a, b) {
                    var x, y;
                    x = a.x - b.x;
                    y = a.y - b.y;
                    return Math.sqrt(x * x + y * y);
                },

                calculateScale = function (startTouches, endTouches) {
                    var startDistance = getDistance(startTouches[0], startTouches[1]),
                        endDistance = getDistance(endTouches[0], endTouches[1]);
                    return endDistance / startDistance;
                },

                cancelEvent = function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                },

                detectDoubleTap = function (event) {
                    var time = (new Date()).getTime();

                    if (fingers > 1) {
                        lastTouchStart = null;
                    }

                    if (time - lastTouchStart < 300) {
                        cancelEvent(event);

                        target.handleDoubleTap(event);
                        switch (interaction) {
                            case "zoom":
                                target.handleZoomEnd(event);
                                break;
                            case 'drag':
                                target.handleDragEnd(event);
                                break;
                        }
                    }

                    if (fingers === 1) {
                        lastTouchStart = time;
                    }
                },
                firstMove = true;

            el.addEventListener('touchstart', function (event) {
                firstMove = true;
                fingers = event.touches.length;
                detectDoubleTap(event);
            });

            el.addEventListener('touchmove', function (event) {

                if (firstMove) {
                    updateInteraction(event);
                    if (interaction) {
                        cancelEvent(event);
                    }
                    startTouches = targetTouches(event.touches);
                } else {
                    switch (interaction) {
                        case 'zoom':
                            target.handleZoom(event, calculateScale(startTouches, targetTouches(event.touches)));
                            break;
                        case 'drag':
                            target.handleDrag(event);
                            break;
                    }
                    if (interaction) {
                        cancelEvent(event);
                        target.update();
                    }
                }

                firstMove = false;
            });

            el.addEventListener('touchend', function (event) {
                fingers = event.touches.length;
                updateInteraction(event);
            });
        };

        return PinchZoom;
    };

    if (typeof define !== 'undefined' && define.amd) {
        define(['jquery'], function ($) {
            return definePinchZoom($);
        });
    } else {
        window.RTP = window.RTP || {};
        window.RTP.PinchZoom = definePinchZoom(jQuery);
    }
}).call(this);


        $j(function() {
            $j('#help-center').on('change', function() {
                var url = $j(this).val();
                if (url) {
                    window.location = url;
                }
                return false;
            });
        });

        var slider = $j('.bxslider').bxSlider({
            mode: 'fade',
            pause: 6000,
            pagerCustom: '#bx-pager',
            controls: false,
            auto: true,
            infiniteLoop: true,
            onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                $j('.hide-banner-content').removeClass('show');
                $j('.hide-banner-button').removeClass('show');
                $j('.hide-banner-content').eq(currentSlideHtmlObject).addClass('show');
                $j('.hide-banner-button').eq(currentSlideHtmlObject).addClass('show');
            },
            onSliderLoad: function () {
                $j('.hide-banner-content').eq(0).addClass('show')
                $j('.hide-banner-button').eq(0).addClass('show')
            }
        });

        setTimeout (function () {
            $j('a.pinch-zoom').each(function () {
                new RTP.PinchZoom($j(this), {});
            });
        },500);

        $j(window).resize(function() {
        });

        $j('.brand-slider').bxSlider({
            auto: true,
            moveSlides: 2,
            pager: false,
            slideWidth: 300,
            minSlides: 2,
            maxSlides: 2
        });

        $j('p.lifestyle-copy').removeClass('show');
        $j('p.covert-copy').removeClass('show');
        $j('p.wildlife-copy').removeClass('show');
        $j('p.security-copy').removeClass('show');

        $j('#categories').on('change', function() {
            $j('.shop-use').hide();
            var val = $j(this).val();
            $j('#'+val).show();
        });

        $j('.sort-btn').on('click', function() {
            $j('.floating-sort').toggleClass('show-sort');
            $j('.listing-filters').removeClass('show-listing-filters');
        });

        $j('.filter-btn').on('click', function() {
            $j('.listing-filters').toggleClass('show-listing-filters');
            $j('.floating-sort').removeClass('show-sort');
        });

        (function () {

            $j('.burger').click(function() {
                $j('.burger-menu').toggleClass('burger-menu-show');
            });

            $j('footer a, .burger-menu span, .burger-menu a').filter(':nth-child(n+1)').addClass('hide');

            $j('.login a').removeClass('hide');

            $j('footer h5').click(function () {
                $j(this).parent().siblings().children('a').removeClass('show');
                $j(this).siblings('a').toggleClass('show');
                $j(this).parent().siblings().children().removeClass('active-footer-tab');;
                $j(this).toggleClass('active-footer-tab');
            });

            $j('.burger-menu .burger-accordian div.burger-sub-menu').click(function () {
                $j(this).siblings('div').children('a').slideUp(200);
                $j(this).parents().siblings().children('div.toggle-main-menu').children('a').slideUp(200);
                $j(this).parents().children('.burger-menu span').removeClass('active-burger-sub-menu');
                $j('.customer-services').removeClass('active-burger-menu');
                $j('.customer-services').siblings('a').removeClass('show');
                $j('.deals').removeClass('active-burger-menu');
                $j('.deals').siblings('a').removeClass('show');
                $j('.burger-menu span').slideUp(200);
                $j(this).siblings('span').css('display','block');
                $j(this).parents().siblings().children('.burger-menu div.toggle-main-menu').removeClass('active-burger-menu');
                $j(this).toggleClass('active-burger-menu');
            });

            $j('.burger-menu .burger-accordian span').click(function () {
                $j(this).siblings('div').children('a').slideUp(200);
                $j(this).next('div').children('a').css('display','block');
                $j(this).siblings('.burger-menu span.hide').removeClass('active-burger-sub-menu');
                $j(this).parents().siblings().children('a.hide').removeClass('show');
                $j(this).toggleClass('active-burger-sub-menu');
            });

            $j('.burger-menu .burger-accordian .customer-services').click(function() {
                $j('.burger-sub-menu').removeClass('active-burger-menu');
                $j('.burger-sub-menu').siblings('span').css('display', 'none');
                $j('.burger-sub-menu').siblings('div').children('a').css('display', 'none');
                $j('.deals').removeClass('active-burger-menu');
                $j('.deals').siblings('a').removeClass('show');
                $j(this).siblings('a').toggleClass('show');
                $j(this).toggleClass('active-burger-menu');
            });

            $j('.burger-menu .burger-accordian .deals').click(function() {
                $j('.burger-sub-menu').removeClass('active-burger-menu');
                $j('.burger-sub-menu').siblings('span').css('display', 'none');
                $j('.burger-sub-menu').siblings('div').children('a').css('display', 'none');
                $j('.customer-services').removeClass('active-burger-menu');
                $j('.customer-services').siblings('a').removeClass('show');
                $j(this).siblings('a').toggleClass('show');
                $j(this).toggleClass('active-burger-menu');
            });

            $j(window).scroll(function() {
                var scroll = $j(window).scrollTop();
                var total = $j('.listing').height();
                if (scroll >= total - 130) {
                    $j(".floating-nav").hide();
                } else {
                    $j(".floating-nav").show();
                }
            });

        setTimeout(function() {
            var slider = $j('.single-product-slider').bxSlider({
                pagerCustom: '#product-pager',
                controls: false,
                auto: false,
                infiniteLoop: false
            });

            $j('.single-product-slider li a.product').each(function() {
                $j(this).removeAttr('href');
            });

            var sliderThumb = $j('#product-pager').bxSlider({
                prevText: '',
                nextText: '',
                controls: true,
                pager: false,
                slideWidth: 69,
                minSlides: 3,
                maxSlides: 3,
                slideMargin: 10,
                infiniteLoop: false,
                hideControlOnEnd: true
            });
        },6000);



        $j('.video-slider').bxSlider({
            slideWidth: 310,
            pager: false,
            minSlides: 1,
            maxSlides: 1
        });

        })();

    $j('.burger-search div.fill-btn').click(function() {
        $j(this).siblings().toggleClass('show');
        $j('.burger-search input').focus();
    });
}
if ($j('body').width() < 768 && $j('body').height() < 415 && $j('body').width() > 480 && $j('body').height() > 320) {
} else {}
// for checkout billing accord
if($j('#opc-billing').hasClass('active')) $j('#opc-billing').prev().removeClass('disable-accord').addClass('active-accord');
$j("button[name='update_cart_action_replica']").click(function(){$j("button[value='update_qty']").click();});


$j('#vat-show-hide').click(function() {
    $j("#vat-show-hide-input").toggle(this.checked);
});
$j('#password-show-hide').click(function() {
    $j("#register-customer-password").toggle(this.checked);
});
$j('#vat-shipping-show-hide').click(function() {
    $j("#vat-shipping-show-hide-input").toggle(this.checked);
});

});
