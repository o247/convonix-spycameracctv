$j(window).load(function(){
    var url=window.location.href;
    $j("#help-center option").each(function(){
      if ($j(this).val() == url)
        $j(this).attr("selected","selected");
    });
});

$j(document).ready(function(){
    $j(function() {
        $j('#help-center').on('change', function() {
            var url = $j(this).val();
            if (url) {
                window.location = url;
            }
            return false;
        });
    });

    // Kill Switch for Local Storage
    var now = new Date();
    if(localStorage.getItem('ls_killswitch') === null) {
        localStorage.clear();
        now.setHours(now.getHours() + 10);
        localStorage.setItem('ls_killswitch', now.getTime());
    } else if (now.getTime() > localStorage.getItem('ls_killswitch')) {
        localStorage.clear();
        now.setHours(now.getHours() + 10);
        localStorage.setItem('ls_killswitch', now.getTime());
    }

    // Set Country Code (2 character)
    if(localStorage.getItem('client_geolocation') === null) {
        $j.get("https://ipinfo.io", function (response) {
            localStorage.setItem('client_geolocation', response.country);
        }, "jsonp");
    }

    // Set Phone No. and next dispatch on all pages
    if(localStorage.getItem('store_contact_no') === null) {
        localStorage.setItem('store_contact_no', '(0)117 325 2470');
    }
    if(localStorage.getItem('client_geolocation') == 'GB') {
        $j("span#geolocation_phone").html(localStorage.getItem('store_contact_no').replace("(0)", "0"));
        $j("li.geolocation_delivery_world").hide();
    }
    else {
        $j("span#geolocation_phone").html(localStorage.getItem('store_contact_no').replace("(0)", "+44 (0)"));
        $j(".next-dispatch span").text('Time until next dispatch');
        $j("li.geolocation_delivery_uk").hide();
    }

    // Inc / Ex VAT Procesing
    $j('.bundle-option-select option').each(function(){
        if($j(this).val()!=""){
            var vatstat = localStorage.getItem('vat_pricing');
            if(vatstat == "inc"){
                $j(this).html($j(this).attr('data-inc'));
            } else {
                $j(this).html($j(this).attr('data-exc'));
            }
        }
    });

    vat_processing_md();
    $j(".vat-toggle").click(function(){
        $j('.filter-ajax-loader').show();
        vat_processing_md(true);
    });

    recalc_price();
    $j('.cart-box select').change(function(){ recalc_price(); });

    $j(".help-center-menu a").each(function(){
        $j(this).removeClass('active');
        if(document.location.href == $j(this).attr('href')) {
            $j(this).addClass('active');
        }
    });

    // Login Form
    var dataForm = new VarienForm('login-form', true);

    // Product page tabs
    $j(".cart-box > dd, .cart-box > dt").hide();
    $j('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default',
        width: 'auto',
        fit: true,
        tabidentify: 'hor_1',
        activate: function(event) {
            var $tab = $j(this);
            var $info = $j('#nested-tabInfo');
            var $name = $j('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

    // Show Loader on sort change
    $j('#sort-select-box').on('change', function() {
        $j('.filter-ajax-loader').show();
    });

    // Rating selection on product page
    $j('.rate_row').starwarsjs({
        stars : 5,
        count : 1,
        default_stars: 5
    });
    $j('.rate_row span').click(function() {
        var rateStar = $j('.rate_row .checked').last().data('value');
        $j('input[name=ratings]').each(function(){
            if($j(this).val() == rateStar) {
                $j(this).prop('checked', 'checked');
            }
        });
    });

    // Reviews submission
    $j('#reviewform').on('submit', function(e) {
        var name=$j('#review_name').val();
        var title=$j('#review_title').val();
        var comment=$j('#review_comment').val();
        var pro_id=$j('#product_id').val();
        var cust_id=$j('#cust_id').val();
        if(name=='') $j('#review_name').addClass('required-entry validation-failed');
        else $j('#review_name').removeClass('required-entry validation-failed');
        if(title=='') $j('#review_title').addClass('required-entry validation-failed');
        else $j('#review_title').removeClass('required-entry validation-failed');
        if(comment=='') $j('#review_comment').addClass('required-entry validation-failed');
        else $j('#review_comment').removeClass('required-entry validation-failed');
        if(name!='' && title!='' && comment!='') {
            $j('review_name').removeClass('required-entry validation-failed');
            $j('#review_title').removeClass('required-entry validation-failed');
            $j('#review_comment').removeClass('required-entry validation-failed');
            $j.ajax({
                type: "POST",
                url: "/smgcapi/index/reviewsubmit",
                data: $j('#reviewform').serialize(),
                cache: false,
                success: function(result){
                    $j('.gray-overlay-review').addClass('gray-overlay-review-opened');
                    $j('#review_name').val("");
                    $j('#review_title').val("");
                    $j('#review_comment').val("");
                    $j("input[name=ratings]:checked").removeAttr("checked");
                }
            });
        }
        return false;
    });

    $j('.gray-overlay-review').click(function() {
        $j(this).removeClass('gray-overlay-review-opened');
    });

    $j('.gray-overlay-wrng-cred').click(function() {
        $j(this).removeClass('gray-overlay-wrng-cred-opened');
    });




    // /public_html/app/design/frontend/SMGC/default/template/page/html/header.phtml
    $j('#cust-log-out').click(function () {
        localStorage.removeItem('billing_address');
        localStorage.removeItem('shipping_address');
    });

    $j('#login-form').on('submit', function(e) {
        var form_key = $j("input[name=form_key]").val();
        var mail_id=$j('#email').val();
        var password=$j('#pass').val();
        var send='';
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var flag=1;

        var dataString = '&username='+ mail_id + '&password='+ password;

        var send_mail = 'mail='+ mail_id;
        if(mail_id=='') $j('#email').addClass('validation-failed');
        else $j('#email').removeClass('validation-failed');
        if(password=='') $j('#pass').addClass('validation-failed');
        else $j('#pass').removeClass('validation-failed');
        if (reg.test(mail_id) == false) {
            $j('#email').addClass('validation-failed');
            $j('#email-not-exist').show();
            $j('#login-failure-msg').hide();
            flag=0;
        } else $j('#email').removeClass('validation-failed');
        if(mail_id!='' && password!='' && flag==1) {
            $j('#email').removeClass('validation-failed');
            $j('#pass').removeClass('validation-failed');

            $j.ajax({
                type: "POST",
                url: "/smgcapi/index/login",
                data: $j('#login-form').serialize(),
                cache: false,
                success: function(result){
                    if(result==1) {
                        localStorage.removeItem('billing_address');
                        localStorage.removeItem('shipping_address');
                        window.location = window.location.href;
                    } else if(result=="failed") {
                        $j('#login-failure-msg').show();
                        $j('#email-not-exist').hide();
                    } else if(result=="notexist") {
                        $j('#email-not-exist').show();
                        $j('#login-failure-msg').hide();
                    }
                }
            });
         }
        return false;
    });

    // public_html/app/design/frontend/SMGC/default/template/checkout/cart/shipping.phtml
    $j('#ship-login-form').on('submit', function(e) {
        var form_key = $j("input[name=form_key]").val();
        var mail_id=$j('#log-email').val();
        var password=$j('#log-password').val();
        var send='';
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var flag=1;

        var dataString = '&username='+ mail_id + '&password='+ password;

        var send_mail = 'mail='+ mail_id;
        if(mail_id=='') $j('#log-email').addClass('validation-failed');
        else $j('#log-email').removeClass('validation-failed');
        if(password=='') $j('#log-password').addClass('validation-failed');
        else $j('#log-password').removeClass('validation-failed');
        if (reg.test(mail_id) == false) {
            $j('#log-email').addClass('validation-failed');
            $j('#email-not-exist3').show();
            $j('#login-failure-msg3').hide();
            flag=0;
        } else $j('#log-email').removeClass('validation-failed');
        if(mail_id!='' && password!='' && flag==1) {
            $j('#log-email').removeClass('validation-failed');
            $j('#log-password').removeClass('validation-failed');

            $j.ajax({
                type: "POST",
                url: "/smgcapi/index/login",
                data: $j('#ship-login-form').serialize(),
                cache: false,
                success: function(result){
                    if(result==1) {
                        localStorage.removeItem('billing_address');
                        localStorage.removeItem('shipping_address');
                        window.location = window.location.href;
                    } else if(result=="failed") {
                        $j('#login-failure-msg3').show();
                        $j('#email-not-exist3').hide();
                    } else if(result=="exist") {
                        $j('#email-not-exist3').show();
                        $j('#login-failure-msg3').hide();
                    }
                }
            });
         }
        return false;
    });

    // /public_html/app/design/frontend/SMGC/default/template/mana/filters/items/list.phtml
    if(localStorage.getItem('vat_pricing') == 'inc') {
        var minprice=$j('#price-range-baseline').attr('data-rangemin-inc');
        var maxprice=$j('#price-range-baseline').attr('data-rangemax-inc');
        var fromprice=$j('#price-range').attr('data-rangemin-inc');
        var toprice=$j('#price-range').attr('data-rangemax-inc');

        $j("#price-range").ionRangeSlider({
            type: "double",
            min: minprice,
            max: maxprice,
            from: fromprice,
            to: toprice,
            step: 5,
            grid: true,
            prefix: $j('#price-range').attr('symbol'),
            onFinish: function (data) {
                var start=data.from;
                var end=data.to;
                var stru = $j('#price-range').attr('data-value');
                var arr = stru.split(",");
                var j,r_start;var arraio = [];var arraiop = [];
                for(j=0;j<arr.length;j++){
                    var ft = parseFloat(arr[j])+(parseFloat(arr[j])*0.2);
                    if(parseInt(ft) >= data.from){
                        arraio.push(parseFloat(arr[j]));
                    }
                    if(parseInt(ft) <= data.to){
                        arraiop.push(parseFloat(arr[j]));
                    }
                }
                var hj_min = Math.min.apply(Math,arraio);
                hj_min = parseInt(hj_min);
                if(hj_min%5!=0){
                    r_start=hj_min-(hj_min%5);
                } else {
                    r_start=hj_min;
                }
                var hl = Math.max.apply(Math,arraiop);
                var hl = parseInt(hl);
                if(hl%5!=0){
                    var temp=5-(hl%5);
                    end=hl+temp;
                }
                var str="2,"+r_start;
                var temp=2*r_start;
                var start_lim,i,end_lim;
                if(temp > end){
                    var gh=(end-r_start)/5;
                    start_lim=(end/5)-gh;
                } else {
                    start_lim=(temp/5)+1;
                }
                end_lim=end/5;
                for(i=start_lim;i<=end_lim;i++){
                    str=str+"_"+i+",5";
                }

                var qs_split_imp="";
                x=window.location.href;
                if(x.indexOf("?")>=0) {
                    var qs = x.split("?");
                    if(qs[1].indexOf("&")>=0) {
                        var qs_split = qs[1].split("&");
                        for(str of qs_split) {
                            if(str.indexOf("price")>=0) {}
                            else{
                                qs_split_imp = "&"+str+qs_split_imp;
                            }
                        }
                    } else {
                        qs_split_imp="&"+qs[1];
                    }
                }
                filterReplace(location.pathname+'?price='+str+qs_split_imp);
                $j('.reset-filters').css('display', 'table');
            }
        });
    } else {
        var minprice=$j('#price-range-baseline').attr('data-rangemin');
        var maxprice=$j('#price-range-baseline').attr('data-rangemax');
        var fromprice=$j('#price-range').attr('data-rangemin');
        var toprice=$j('#price-range').attr('data-rangemax');
        $j("#price-range").ionRangeSlider({
            type: "double",
            min: minprice,
            max: maxprice,
            from: fromprice,
            to: toprice,
            step: 5,
            grid: true,
            prefix: $j('#price-range').attr('symbol'),
            onFinish: function (data) {
                var start=data.from;
                var end=data.to;
                var str="2,"+data.from;
                var temp=2*start;
                var start_lim,i,end_lim;
                if(temp > end){
                    var gh=(end-start)/5;
                    start_lim=(end/5)-gh;
                } else {
                    start_lim=(temp/5)+1;
                }
                end_lim=end/5;
                for(i=start_lim;i<=end_lim;i++){
                    str=str+"_"+i+",5";
                }

                var qs_split_imp="";
                x=window.location.href;
                if(x.indexOf("?")>=0) {
                    var qs = x.split("?");
                    if(qs[1].indexOf("&")>=0) {
                        var qs_split = qs[1].split("&");
                        for(str of qs_split) {
                            if(str.indexOf("price")>=0) {}
                            else{
                                qs_split_imp = "&"+str+qs_split_imp;
                            }
                        }
                    } else{
                        qs_split_imp="&"+qs[1];
                    }
                }
                filterReplace(location.pathname+'?price='+str+qs_split_imp);
                $j('.reset-filters').css('display', 'table');
            }
        });
    }

    $j("#night-vision-range").ionRangeSlider({
    type: "double",
    min: $j('#night-vision-range-baseline').attr('data-rangemin'),
    max: $j('#night-vision-range-baseline').attr('data-rangemax'),
    from: $j('#night-vision-range').attr('data-rangemin'),
    to: $j('#night-vision-range').attr('data-rangemax'),
    step: 1,
    grid: true,
    postfix: "m",
    onFinish: function (data) {
            var nvr_data=$j('#night-vision-range-baseline').attr('data-value');
            var i,q,u="";
            var p = nvr_data.split(",");
            for(i=0;i<p.length;i++){
                q = p[i].split("_");
                if(q[1] >= data.from && q[1] <= data.to){
                    u=u+"_"+q[0];
                }
            }
            var final_url = u.substr(1);
            var qs_split_imp="";
            x=window.location.href;
            if(x.indexOf("?")>=0) {
                var qs = x.split("?");
                if(qs[1].indexOf("&")>=0) {
                    var qs_split = qs[1].split("&");
                    for(str of qs_split) {
                        if(str.indexOf("night_vision_range")>=0) {}
                        else{
                            qs_split_imp = "&"+str+qs_split_imp;
                        }
                    }
                } else {
                    qs_split_imp="&"+qs[1];
                }
            }
            filterReplace(location.pathname+'?night_vision_range='+final_url+qs_split_imp);
            $j('.reset-filters').css('display', 'table');
        }
    });
    $j("#field-of-view-range").ionRangeSlider({
    type: "double",
    min: $j('#field-of-view-range-baseline').attr('data-rangemin'),
    max: $j('#field-of-view-range-baseline').attr('data-rangemax'),
    from: $j('#field-of-view-range').attr('data-rangemin'),
    to: $j('#field-of-view-range').attr('data-rangemax'),
    step: 10,
    grid: true,
    postfix: "°",
    onFinish: function (data) {
            var nvr_data=$j('#field-of-view-range-baseline').attr('data-value');
            var i,q,u="";
            var p = nvr_data.split(",");
            for(i=0;i<p.length;i++){
                q = p[i].split("_");
                if(q[1] >= data.from && q[1] <= data.to){
                    u=u+"_"+q[0];
                }
            }
            var final_url = u.substr(1);
            var qs_split_imp="";
            x=window.location.href;
            if(x.indexOf("?")>=0) {
                var qs = x.split("?");
                if(qs[1].indexOf("&")>=0) {
                    var qs_split = qs[1].split("&");
                    for(str of qs_split) {
                        if(str.indexOf("field_of_view")>=0) {}
                        else{
                            qs_split_imp = "&"+str+qs_split_imp;
                        }
                    }
                } else {
                    qs_split_imp="&"+qs[1];
                }
            }
            filterReplace(location.pathname+'?field_of_view='+final_url+qs_split_imp);
            $j('.reset-filters').css('display', 'table');
        }
    });

    $j("#discount-coupon-form button:eq(1)").click(function(e){
        e.preventDefault();
        $j("input#coupon_code").val('');
        $j("#discount-coupon-form").submit();
    });

    if(localStorage.getItem('shipping_address') != null) {
        var loadedAddress = JSON.parse(localStorage.getItem('shipping_address'));
        var formatAddress = "";
        if(loadedAddress.firstname != "" && loadedAddress.firstname != null && loadedAddress.firstname != undefined) formatAddress = formatAddress + loadedAddress.firstname;
        if(loadedAddress.lastname != "" && loadedAddress.lastname != null && loadedAddress.lastname != undefined) formatAddress = formatAddress + " " + loadedAddress.lastname + ",<br>";
        if(loadedAddress.company != "") formatAddress = formatAddress + loadedAddress.company + ",<br>";
        if(loadedAddress.line_1 != "") formatAddress = formatAddress + loadedAddress.line_1 + ",<br>";
        if(loadedAddress.line_2 != "") formatAddress = formatAddress + loadedAddress.line_2 + ",<br>";
        if(loadedAddress.county != "") formatAddress = formatAddress + loadedAddress.county + ",<br>";
        if(loadedAddress.town != "") formatAddress = formatAddress + loadedAddress.town + ",<br>";
        if(loadedAddress.postcode != "") formatAddress = formatAddress + loadedAddress.postcode + ",<br>";
        formatAddress = formatAddress.substring(0, formatAddress.length - 5);
        if(loadedAddress.aid == "") {
            $j("#show-add-prime").hide();
            $j("#static_add").html(formatAddress);
        } else {
            $j("#static_add").hide();
            $j("#show-add-prime").html(formatAddress);
        }
    }
    if($j('input[name=estimate_method]:checked').val() != "" && typeof $j('input[name=estimate_method]:checked').val() != "undefined") $j('#checkout-button').removeClass("disabled-btn");

    if($j('#rat').val()==1) {
        $j('input[name=estimate_method]').prop("checked",true);
        var checkShipping = false;
        $j("#shopping-cart-totals-table div div div").each(function(){
            var str = $j(this).text().trim();
            var patt = /shipping/gi;
            var res = patt.test(str);
            if(res == true) checkShipping = true;
        });
        if(checkShipping == false) {
            $j('.filter-ajax-loader').show();
            $j("button[value='update_total']").click();
        }
    }
    if(localStorage.getItem('shipping_address') != null) {
        if($j('input[type="radio"]').length==0){
            $j('#no-shipping-option').show();
        }
    }

    $j('#co-ship-method-form input').on('change', function() {
        $j('.filter-ajax-loader').show();
        $j("button[value='update_total']").click();
        $j('#checkout-button').removeClass("disabled-btn");
    });

    // /public_html/app/design/frontend/SMGC/default/template/persistent/checkout/billing.phtml
    $j('#edit-address').click(function(e) {
        e.preventDefault();
        $j('#billing-new-address-form .wide').show();
        $j('#billing-address-select').val("");
    });
    $j('#bill-new-add').click(function(e) {
        // make sure submission resets ls.shipping_address
        $j("#billing-address-select option:last").prop('selected','selected');
        $j('#billing-new-address-form .wide').show();
    });

    $j('input[name=billing_address_id_radio]').change(function(){
        var value = $j( 'input[name=billing_address_id_radio]:checked' ).val();
        $j('#billing-new-address-form').css('display','none');
        // update ls.shipping_address
        $j('#billing-address-select').val(value);
    });

    // /public_html/app/design/frontend/SMGC/default/template/checkout/onepage/shipping.phtml
    $j('#ship-new-add').click(function(e) {
        localStorage.setItem('address_id',"");
        $j("#shipping-address-select option:last").prop('selected','selected');
        $j('#shipping-new-address-form .wide').show();
    })

    $j('#shipping-new-address-form .wide').show();
    $j(".step_title_1 a").click(function(){$j("#checkout-step-shipping_method").hide()})

    $j('#checkout-step-billing').show();
    $j('input[name=shipping_address_id_radio]').change(function(){
        var value = $j( 'input[name=shipping_address_id_radio]:checked' ).val();
        $j('#shipping-new-address-form').css('display','none');
        localStorage.setItem('address_id',value);
        $j('#shipping-address-select').val(value);
    });

    // /public_html/app/design/frontend/SMGC/default/template/checkout/onepage/shipping_method.phtml
    $j(".step_title_2 a").click(function(){$j("#checkout-step-payment").show()})



    // /public_html/app/design/frontend/SMGC/default/template/catalog/product/view/type/add_popup.phtml
    // /public_html/app/design/frontend/SMGC/default/template/catalog/product/view/type/update_popup.phtml
    $j('#cont_shop').click(function () {
        $j('.filter-ajax-loader').show();
    });


    // /public_html/app/design/frontend/SMGC/default/template/persistent/customer/form/register.phtml
    $j('#firstname').attr("tabindex", 1);
    $j('#lastname').attr("tabindex", 2);
    $j('#email_address').attr("tabindex", 3);
    $j('#telephone').attr("tabindex", 4);
    $j('#is_subscribed').attr("tabindex", 5);
    $j('#_cc_search_input').attr("tabindex", 6);
    $j('#company').attr("tabindex", 7);
    $j('#street_1').attr("tabindex", 8);
    $j('#city').attr("tabindex", 9);
    $j('#region').attr("tabindex", 10);
    $j('#zip').attr("tabindex", 11);
    $j('#country').attr("tabindex", 12);
    $j('#vat-show-hide').attr("tabindex", 13);
    $j('#password').attr("tabindex", 14);
    $j('#confirmation').attr("tabindex", 15);
    $j('#regis-new-cust').attr("tabindex", 16);
});

//function for filter ajax
if (typeof window.filterReplace === 'undefined') {
    window.filterReplace = function filterReplace(x) {
        $j('.filter-ajax-loader').show();
        $j.ajax({
            url: x,
            datatType: 'html',
            success: function(data) {
                var filterdiv = $j(data).find('#filterdiv').html();
                $j('#filterdiv').html(filterdiv);
                var resultdiv = $j(data).find('#filterresultsdiv').html();
                $j('#filterresultsdiv').html(resultdiv);

                $j('.quick-view-holder .filled-close-btn').click(function() {
                    $j('.gray-overlay').removeClass('gray-overlay-opened');
                });

                $j('.reset-filters').click(function() {
                    window.location.href=document.location.origin+location.pathname;
                });

                $j('button.quick-view').click(function() {
                    var quickViewId = $j(this).attr('id');
                        quickViewId = '.' + quickViewId;
                    $j('body').find(quickViewId).addClass('gray-overlay-opened');
                });

                $j('.open-wireless-cat').slideUp(200);

                if ($j('.listing-filters .accordian .filter-list').children('div').hasClass('hAccord')) {
                    $j('.hAccord').parent('.filter-list').prev('.round-checkbox').slideUp();
                    $j('.hAccord').parent('.filter-list').slideDown();
                } else {}

                $j('.listing-filters .accordian .round-checkbox').click(function () {
                    $j(this).next('.listing-filters .accordian div.filter-list').slideToggle(200);
                    $j(this).toggleClass('checkbox-active');
                });

                $j(".show-more").click(function(){
                    $j(".filter-list:gt(9)").css("display","block");
                    $j(".round-checkbox").css("display","block");
                    $j('.hAccord').parent('.filter-list').prev('.round-checkbox').slideUp();
                    $j('.hAccord').parent('.filter-list').slideDown();
                    $j(this).css("display","none");
                    $j(".show-less").css("display","flex");
                });

                $j(".show-less").click(function(){
                    $j(".filter-list:gt(9)").css("display","none");
                    $j(".round-checkbox:gt(9)").css("display","none");
                    $j(this).css("display","none");
                    $j(".show-more").css("display","flex");
                });
                $j('.wireless-cat .round-checkbox label').click(function( event ) {
                    event.stopPropagation();
                });

                $j(".filter-list").each(function(){
                    if ($j(this).children().length == 0 ) {
                        $j(this).prev('.round-checkbox').hide();
                    }
                });
                if(x.indexOf("?")>=0) {
                    $j('.reset-filters').css('display', 'table');
                    var qs = x.split("?");
                    var qs_split_imp="";
                    var qs_split_imp1="";
                    var qs_split_imp2="";
                    if(qs[1].indexOf("night_vision_range")>=0) {
                        if(qs[1].indexOf("&")>=0) {
                            var qs_split = qs[1].split("&");
                            for(str of qs_split) {
                                if(str.indexOf("night_vision_range")>=0) {}
                                else{
                                    qs_split_imp = "&"+str+qs_split_imp;
                                }
                            }
                        }
                    } else {
                        qs_split_imp = "&"+qs[1];
                    }

                    if(qs[1].indexOf("field_of_view")>=0) {
                        if(qs[1].indexOf("&")>=0) {
                            var qs_split1 = qs[1].split("&");
                            for(str1 of qs_split1) {
                                if(str1.indexOf("field_of_view")>=0) {}
                                else{
                                    qs_split_imp1 = "&"+str1+qs_split_imp1;
                                }
                            }
                        }
                    } else {
                        qs_split_imp1 = "&"+qs[1];
                    }

                    if(qs[1].indexOf("price")>=0) {
                        if(qs[1].indexOf("&")>=0) {
                            var qs_split2 = qs[1].split("&");
                            for(str2 of qs_split2) {
                                if(str2.indexOf("price")>=0) {}
                                else{
                                    qs_split_imp2 = "&"+str2+qs_split_imp2;
                                }
                            }
                        }
                    } else {
                        qs_split_imp2 = "&"+qs[1];
                    }
                }

                if(localStorage.getItem('vat_pricing') == 'inc') {
                    var minprice=$j('#price-range-baseline').attr('data-rangemin-inc');
                    var maxprice=$j('#price-range-baseline').attr('data-rangemax-inc');
                    var fromprice=$j('#price-range').attr('data-rangemin-inc');
                    var toprice=$j('#price-range').attr('data-rangemax-inc');
                    $j("#price-range").ionRangeSlider({
                        type: "double",
                        min: minprice,
                        max: maxprice,
                        from: fromprice,
                        to: toprice,
                        step: 5,
                        grid: true,
                        prefix: $j('#price-range').attr('symbol'),
                        onFinish: function (data) {
                            var start=data.from;
                            var end=data.to;
                            var stru = $j('#price-range-baseline').attr('data-value');
                            var arr = stru.split(",");
                            var j,r_start;var arraio = [];var arraiop = [];
                            for(j=0;j<arr.length;j++){
                                var ft = parseFloat(arr[j])+(parseFloat(arr[j])*0.2);
                                if(parseInt(ft) >= data.from){
                                    arraio.push(parseFloat(arr[j]));
                                }
                                if(parseInt(ft) <= data.to){
                                    arraiop.push(parseFloat(arr[j]));
                                }
                            }
                            var hj_min = Math.min.apply(Math,arraio);
                            hj_min = parseInt(hj_min);
                            if(hj_min%5!=0){
                                r_start=hj_min-(hj_min%5);
                            }else{
                                r_start=hj_min;
                            }
                            var hl = Math.max.apply(Math,arraiop);
                            var hl = parseInt(hl);
                            if(hl%5!=0){
                                var temp=5-(hl%5);
                                end=hl+temp;
                            }
                            var str="2,"+r_start;
                            var temp=2*r_start;
                            var start_lim,i,end_lim,count=0,count1=0;
                            if(temp > end){
                                str = "";
                                for(i=0;i<=r_start;i=i+5){
                                    count++;
                                }
                                for(i=0;i<=end;i=i+5){
                                    count1++;
                                }
                                for(i=count;i<=count1;i++){
                                    str=str+"_"+i+",5";
                                }
                            } else {
                                start_lim=(temp/5)+1;
                                end_lim=end/5;
                                for(i=start_lim;i<=end_lim;i++){
                                    str=str+"_"+i+",5";
                                }
                            }
                            filterReplace(document.location.origin+location.pathname+'?price='+str+qs_split_imp2);
                        }
                    });
                } else {
                    var minprice=$j('#price-range-baseline').attr('data-rangemin');
                    var maxprice=$j('#price-range-baseline').attr('data-rangemax');
                    var fromprice=$j('#price-range').attr('data-rangemin');
                    var toprice=$j('#price-range').attr('data-rangemax');
                    $j("#price-range").ionRangeSlider({
                        type: "double",
                        min: minprice,
                        max: maxprice,
                        from: fromprice,
                        to: toprice,
                        step: 5,
                        grid: true,
                        prefix: $j('#price-range').attr('symbol'),
                        onFinish: function (data) {
                            var start=data.from;
                            var end=data.to;
                            var str="2,"+data.from;
                            var temp=2*start;
                            var start_lim,i,end_lim;
                            if(temp > end){
                                str = "";
                                for(i=0;i<=r_start;i=i+5){
                                    count++;
                                }
                                for(i=0;i<=end;i=i+5){
                                    count1++;
                                }
                                for(i=count;i<=count1;i++){
                                    str=str+"_"+i+",5";
                                }
                            } else {
                                end_lim=end/5;
                                for(i=start_lim;i<=end_lim;i++){
                                    str=str+"_"+i+",5";
                                }
                            }
                            filterReplace(document.location.origin+location.pathname+'?price='+str+qs_split_imp2);
                        }
                    });
                }


                $j("#night-vision-range").ionRangeSlider({
                    type: "double",
                    min: $j('#night-vision-range-baseline').attr('data-rangemin'),
                    max: $j('#night-vision-range-baseline').attr('data-rangemax'),
                    from: $j('#night-vision-range').attr('data-rangemin'),
                    to: $j('#night-vision-range').attr('data-rangemax'),
                    step: 1,
                    grid: true,
                    postfix: "m",
                    onFinish: function (data) {
                            var nvr_data=$j('#night-vision-range-baseline').attr('data-value');
                            var i,q,u="";
                            var p = nvr_data.split(",");
                            for(i=0;i<p.length;i++){
                                q = p[i].split("_");
                                if(q[1] >= data.from && q[1] <= data.to){
                                    u=u+"_"+q[0];
                                }
                            }
                            var final_url = u.substr(1);
                            filterReplace(document.location.origin+location.pathname+'?night_vision_range='+final_url+qs_split_imp);
                        }
                    });

                $j("#field-of-view-range").ionRangeSlider({
                    type: "double",
                    min: $j('#field-of-view-range-baseline').attr('data-rangemin'),
                    max: $j('#field-of-view-range-baseline').attr('data-rangemax'),
                    from: $j('#field-of-view-range').attr('data-rangemin'),
                    to: $j('#field-of-view-range').attr('data-rangemax'),
                    step: 10,
                    grid: true,
                    postfix: "°",
                    onFinish: function (data) {
                            var nvr_data=$j('#field-of-view-range-baseline').attr('data-value');
                            var i,q,u="";
                            var p = nvr_data.split(",");
                            for(i=0;i<p.length;i++){
                                q = p[i].split("_");
                                if(q[1] >= data.from && q[1] <= data.to){
                                    u=u+"_"+q[0];
                                }
                            }
                            var final_url = u.substr(1);
                            filterReplace(document.location.origin+location.pathname+'?field_of_view='+final_url+qs_split_imp1);
                        }
                    });
                $j('.filter-ajax-loader').hide();
            }
        });
    }
}

// Function for VAT processing
function vat_processing_md(set) {
    if (typeof(set)==='undefined') set = false;
    // Set VAT
    if(localStorage.getItem('vat_pricing') === null) {
        // inc = Price has tax included
        // ex = Price has no tax included
        localStorage.setItem('vat_pricing', 'inc');
    }

    if($j("#currency-drop-down-box option:selected").attr('id') == 'GBP' || $j("#currency-drop-down-box option:selected").attr('id') == 'EUR') {
        if(localStorage.getItem('vat_pricing') == 'inc') {
            if(set == true) {
                $j(".vat-icon-show").addClass('vat-icon-hide');
                $j('.vat-yellow-icon-hide').addClass('vat-yellow-icon-show');
                $j('.vat-toggle > div > ul > li:last-child').text('Show inc VAT prices');
                localStorage.setItem('vat_pricing', 'ex');
                location.reload();
            } else {
                $j(".vat-icon-show").removeClass('vat-icon-hide');
                $j('.vat-yellow-icon-hide').removeClass('vat-yellow-icon-show');
                $j('.vat-toggle > div > ul > li:last-child').text('Show ex VAT prices');
            }
        } else {
            if(set == true) {
                $j(".vat-icon-show").removeClass('vat-icon-hide');
                $j('.vat-yellow-icon-hide').removeClass('vat-yellow-icon-show');
                $j('.vat-toggle > div > ul > li:last-child').text('Show ex VAT prices');
                localStorage.setItem('vat_pricing', 'inc');
                location.reload();
            } else {
                $j(".vat-icon-show").addClass('vat-icon-hide');
                $j('.vat-yellow-icon-hide').addClass('vat-yellow-icon-show');
                $j('.vat-toggle > div > ul > li:last-child').text('Show inc VAT prices');
            }
        }
        $j('.top-nav li a.vat-toggle').parent('li').css('display','table-cell');
    } else {
        $j(".vat-icon-show").removeClass('vat-icon-hide');
        $j('.vat-yellow-icon-hide').removeClass('vat-yellow-icon-show');
        $j('.vat-toggle > div > ul > li:last-child').text('inc VAT prices');
        localStorage.setItem('vat_pricing', 'inc');
        $j('.top-nav li a.vat-toggle').parent('li').css('display','none');
    }
}

// function for mobile number validation
function isNumberForMobile(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;

  // ASCII 48-57 is '0'-'9' characters
  // 40,41 open '(' and close ')' characters
  // 43,45 plus '+' and hypehn '-' characters
  if (
    ((charCode >= 48) && (charCode <= 57))
    || (charCode === 40)
    || (charCode === 41)
    || (charCode === 43)
    || (charCode === 45)
    || (charCode === 0)
    || (charCode === 8)
  ) {
      return true;
  }

  return false;
}

function updateaddress(order_id)
{
    var loadedAddress = JSON.parse(localStorage.getItem('shipping_address'));
    var password = localStorage.getItem('password');
    var dataString = {};
    dataString['order_id'] = decodeURIComponent(order_id || '');
    dataString['customer_firstname'] = decodeURIComponent(loadedAddress.firstname || '');
    dataString['customer_lastname'] = decodeURIComponent(loadedAddress.lastname || '');
    dataString['customer_tel'] = decodeURIComponent(loadedAddress.tel || '');
    dataString['country'] = decodeURIComponent(loadedAddress.country || '');
    dataString['postcode'] = decodeURIComponent(loadedAddress.postcode || '');
    dataString['county'] = decodeURIComponent(loadedAddress.county || '');
    dataString['company'] = decodeURIComponent(loadedAddress.company || '');
    dataString['street'] = decodeURIComponent(loadedAddress.line_1+" "+loadedAddress.line_2 || '');
    dataString['region_id'] = decodeURIComponent(loadedAddress.region_id || '');
    dataString['region'] = decodeURIComponent(loadedAddress.town || '');
    dataString['aid'] = decodeURIComponent(loadedAddress.aid || '');
    dataString['password'] = decodeURIComponent(password || '');
    var data_update = JSON.stringify(dataString);

    var newsletter = localStorage.getItem('newsletter');

    if(newsletter!=""){
        $j.ajax({
            type: "POST",
            url: "/newsletter/subscriber/new",
            data: "email="+decodeURIComponent(newsletter || ''),
            cache: false,
            success: function(result){
            }
        });
    }
    $j.ajax({
        type: "POST",
        url: "/smgcapi/index/updateaddress",
        data: {'data' : data_update},
        cache: false,
        dataType: "JSON",
        success: function(result){
        }
    });
    localStorage.clear();
}

// /public_html/app/design/frontend/SMGC/default/template/checkout/onepage/shipping_method/available.phtml
function shipping_method_checkout()
{
    if($j('input[type="radio"][name="shipping_method"]').is(":checked")) {
        $j('input[type="radio"][name="shipping_method"]:checked').parent('span').parent('li').addClass('selected-ship-opt');
        $j("button[value='trigger_shipping_method']").click();
        var idVal = $j('input[type="radio"][name="shipping_method"]:checked').attr("id");
        var ship_meth = $j("label[for='"+idVal+"']").html();
        var arr = ship_meth.split('</span>');
        var as=arr[0].split('>');
        $j("#ship_meth_display").val(as[1]+' - '+arr[1]);
        $j('#shipping_method_info div').text('');
    }

    $j('#co-shipping-method-form input').on('change', function() {
        $j('.selected-ship-opt').removeClass('selected-ship-opt');
        $j('input[type="radio"][name="shipping_method"]:checked').parent('span').parent('li').addClass('selected-ship-opt');
        $j("button[value='trigger_shipping_method']").click();
        var idVal = $j('input[type="radio"][name="shipping_method"]:checked').attr("id");
        var ship_meth = $j("label[for='"+idVal+"']").html();
        var arr = ship_meth.split('</span>');
        var as=arr[0].split('>');
        $j("#ship_meth_display").val(as[1]+' - '+arr[1]);
        $j('#shipping_method_info div').text('');
    });
}

function recalc_price() {
    if($j("#currency-drop-down-box option:selected").attr('id') != 'USD') {
        $j("span.price,span.save b,span.discount b").each(function(){
            var vatstat = localStorage.getItem('vat_pricing');
            if(vatstat == "inc"){
                $j(this).html($j(this).attr('data-currency')+$j(this).attr('data-incprice'));
            } else {
                $j(this).html($j(this).attr('data-currency')+$j(this).attr('data-exprice'));
            }
        });
        $j("span.state-filter").each(function(){
            var vatstat = localStorage.getItem('vat_pricing');
            if(vatstat == "inc"){
                $j(this).html($j(this).attr('data-incprice'));
            } else {
                $j(this).html($j(this).attr('data-exprice'));
            }
        });
    } else {
        $j("span.price,span.save b,span.discount b").each(function(){
            $j(this).html($j(this).attr('data-currency')+$j(this).attr('data-exprice'));
        });
        $j("span.state-filter").each(function(){
            $j(this).html($j(this).attr('data-exprice'));
        });
    }

    $j('#checkout-payment-method-load .payment-holder > div').click(function() {
        $j('#checkout-payment-method-load .payment-holder > div').removeClass('selected-pay-method');
        $j(this).addClass('selected-pay-method');
    });
}
