Product.Config.prototype.fillSelect  = Product.Config.prototype.fillSelect.wrap(function(parentMethod, element){
     var attributeId = element.id.replace(/[a-z]*/, '');
    var options = this.getAttributeOptions(attributeId);
    this.clearSelect(element);
    element.options[0] = new Option('', '');
    element.options[0].innerHTML = this.config.chooseText;

    var prevConfig = false;
    if(element.prevSetting){
        prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
    }

    if(options) {
        var index = 1;
        for(var i=0;i<options.length;i++){
            var allowedProducts = [];
            if(prevConfig) {
                for(var j=0;j<options[i].products.length;j++){
                    if(prevConfig.config.allowedProducts
                        && prevConfig.config.allowedProducts.indexOf(options[i].products[j])>-1){
                        allowedProducts.push(options[i].products[j]);
                    }
                }
            } else {
                allowedProducts = options[i].products.clone();
            }

            if(allowedProducts.size()>0){
                options[i].allowedProducts = allowedProducts;
                var vatstat = localStorage.getItem('vat_pricing');
                if(vatstat == "inc"){
                    var pri=parseFloat(options[i].price);
                    var pric=pri+(pri*0.2);
                    element.options[index] = new Option(this.getOptionLabel(options[i], pric), options[i].id);
                    if (typeof options[i].price != 'undefined') {
                        element.options[index].setAttribute('price', pric);
                    }
                }
                else
                {
                    element.options[index] = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                    if (typeof options[i].price != 'undefined') {
                        element.options[index].setAttribute('price', options[i].price);
                    }
                }
                 element.options[index].config = options[i];
                index++;
            }
        }
    }
});