# CHANGELOG
## 0.10.2 (29 Nov 2017)
- Fix Issue #27 Search Box on Desktop layout: No default text

## 0.10.1 (29 Nov 2017)
- Fix Issue #26 Register page: Can't delete characters in telephone field

## 0.10.0 (24 Nov 2017)
- Update conflicting HTML id for Sooqr search box

## 0.1.9 (30 Oct 2017)
- Adapted Andy's recommendation to remove commented CSS codes
- Fixed Issue #22 - Product page: Contents Box has an odd margin
- Fixed Issue #28 - Darken the rest of the page when navigation menu is open
- Fixed Issue #31 - Product page: photo overlay pop-up too narrow
- Fixed Issue #34 - Product page: In Stock element required
- Fixed Issue #38 - Mobile: Product pages - no price or upsells appear
- Fixed Issue #39 - Mobile: Search page layout completely wrong
- Fixed Issue #40 - Mobile: Category page 'View Product' button not tall enough
- Fixed Issue #41 - Tablet: Sub-category text on navigation menu too small
- Fixed Issue #42 - Tablet: Thank you page - tea mug image far too large
- Fixed Issue #43 - Mobile: product pages - change < & > icons to + & –

## 0.1.8 (10 Oct 2017)
- Fixed Issue #17 - Basket promotion codes are not working
- Fixed Issue #19 - Blog section on homepage not pulling through posts
- Fixed Issue #25 - Product prices glitching when translated into other languages
- Fixed Issue #26: Register page: Can't delete characters in telephone field

## 0.1.7 (23 Aug 2017)
- Fixed mobile navigation

## 0.1.6 (23 Aug 2017)
- Removed css file for starwars.js from themes local.xml file

## 0.1.5 (23 Aug 2017)
- Fixed CSS and JS
- Included missing images
- Code hygiene

## 0.1.4 (23 Aug 2017)
- Modified files (public/app/design/frontend/SMGC/default/template/catalog/product/view.phtml and public/app/design/frontend/SMGC/default/template/checkout/cart/item/configure/updatecart.phtml) to fix add/edit product functionality
- Replaced hard-coded domain name with Magento getBaseUrl()

## 0.1.3 (22 Aug 2017)
- Fixed category layout in catalog.xml file

## 0.1.2 (10 Aug 2017)
- Removed CSS from local.xml layout file

## 0.1.1 (26 June 2017)
- Fix untest broken CSS in local.xml layout file

## 0.1.0 (26 June 2017)
- jQuery upgrade 1.7.2 to 1.10.2
- Modifications to theme layout XML - Third party CSS and JS
- Removal of commented code
- Convonix work 20170530 Zip file sha1sum: 1e7ab59bbdf3839198091fac9a47260653fd7b45 code-20170530.zip
- Convonix work 20170523 Zip file
sha1sum:
5e8bc731899e02c9bed4d2cf913a8fef0b407ce8  code-20170523.zip
- Convonix work 20170719 Zip file
9ca7b32a0a1f8373dfe501b88dbaef83aaf01981  code-20170519.zip
